# -*- coding: utf-8 -*-
from pscan2 import *
from pscan2 import psglobals
from pscan2.Simulation import TimeStep, InitSimulation
import sys, numpy, time
import matplotlib.pyplot as plt

initialize(sys.argv)

#print("Global Parameters:")
#for param in psglobals.Parameters.values():
#	print(param)

#print("Global Internals:")
#for intern in psglobals.InternalsList:
#	print(intern)

#print("Nodes:\n", psglobals.NodeMap, "\n\n")

#print("Elements:", len(psglobals.ElementList))
#print("Nodes:", len(psglobals.NodeMap))

#for elem in psglobals.ElementList:
#	print(elem)

#print("\nCircuit scripts:\n")	
#for circuit in psglobals.CircuitScriptMap.values():
#	print(circuit)

#print("Total Parameters: " + str(psglobals.TotalParameters))
#print("Total Externals: " + str(psglobals.TotalExternals))

tarr = []
varr1 = []
varr2 = []
varr3 = []
varr4 = []
varr5 = []
varr6 = []


v1 = find('.l1', otype='e')
v2 = find('.l2', otype='e')
v3 = find('.l3', otype='e')
#v4 = find('.n4', otype='n')

InitSimulation()

while psglobals.CurrentTime <= 20:
	TimeStep()
	tarr.append(psglobals.CurrentTime)
	q1 = v1.I()
	varr1.append(q1)
	q2 = v2.I()
	varr2.append(q2)
	varr3.append(v3.I())
#	varr4.append(v4.P())


plt.plot(tarr,varr1, linestyle='-', marker='D', color = 'red')
plt.plot(tarr,varr2, linestyle='-', marker='D', color = 'blue')
plt.plot(tarr,varr3, linestyle='-', marker='*', color = 'green')
#plt.plot(tarr,varr4, linestyle='-', marker='*', color = 'yellow')
#plt.plot(tarr,varr5, linestyle='-', marker='*', color = 'gray')
#plt.plot(tarr,varr6, linestyle='-', marker='*', color = 'brown')



plt.xlabel('T')
plt.ylabel('V')
plt.grid(True)
plt.show()
