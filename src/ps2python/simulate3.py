# -*- coding: utf-8 -*-
from pscan2 import *
from pscan2 import psglobals
from pscan2.Simulation import TimeStep, InitSimulation
import sys, numpy, time
import matplotlib.pyplot as plt

initialize(sys.argv)

tarr = []
varr1 = []
varr2 = []

v1 = find('.l1', otype='e')

InitSimulation()

while psglobals.CurrentTime <= 50:
	TimeStep()
	tarr.append(psglobals.CurrentTime)
	q1 = v1.V()
	varr1.append(q1)
	varr2.append(v1.I())

plt.plot(tarr,varr1, linestyle='-', marker='D', color = 'red')
plt.plot(tarr,varr2, linestyle='-', marker='D', color = 'blue')


plt.xlabel('T')
plt.ylabel('P')
plt.grid(True)
plt.show()
