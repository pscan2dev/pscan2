# -*- coding: utf-8 -*-
from pscan2 import *
import sys, time

# Initilaize PSCAN2 and load circuit
initialize(['', 'testnot'])

xi = find('XI')
print(xi)

p1 = find('.p1', 'e')
print(p1)

l1_2 = param_find('not1', 'l1')
print(l1_2)

j1 = find('.mi.j1')
print(j1)

xj1 = find('.mi.xj1')
print(xj1)


llst = find_re('.l*', 'e')
print(".l* -> ", str(llst))

jlst = find_re('.mi.j*')
print(".mi.j* -> ", str(jlst))

jlst = param_re('not1', 'j*')
print("not1.j* -> ", str(jlst))


xjlst = find_re('.mi.xj*')
print(".mi.xj* -> ", str(xjlst))


