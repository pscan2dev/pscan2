import math
import matplotlib.pyplot as plt

def smooth_step(x):
	if x <= 0.0:
		return(0.0)
	elif x >= 1.0:
		return(1.0)
	else:
		return(-2.0*x*x*x+3.0*x*x)
		
def psfq(p,d,s,t):
	dt = t - s
	if dt < 0:
		return(0.0)
	else:
		return(2.0 * math.pi * (math.floor(dt / p) + smooth_step((dt % p)/d)))
		
		

tarr = []
parr = []

for i in range(0,1000):
	t = 0.1* i
	x = psfq(40, 5, 10, t)
	tarr.append(t)
	parr.append(x)
	
plt.plot(tarr,parr, linestyle='-', marker='o')
plt.grid(True)
plt.show()