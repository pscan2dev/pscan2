# -*- coding: utf-8 -*-
from pscan2 import *
import sys, time

# Initilaize PSCAN2 and load circuit
initialize(sys.argv)

xi = find('XI')
print(xi)

l1 = find('.l1', 'e')
print(l1)

l1_2 = param_find('adder01', 'l1')
print(l1_2)

j1 = find('.mha.j1')
print(j1)

xj1 = find('.mha.xj1')
print(xj1)


llst = find_re('.l*', 'e')
print(".l* -> ", str(llst))

jlst = find_re('.mha.j*')
print(".mha.j* -> ", str(jlst))

jlst = param_re('ha', 'j*')
print("ha.j* -> ", str(jlst))


xjlst = find_re('.mha.xj*')
print(".mha.xj* -> ", str(xjlst))

save_parameters(['cb', 'ha'])

