# -*- coding: utf-8 -*-
from pscan2 import *
from pscan2 import psglobals
from pscan2.Simulation import TimeStep, InitSimulation
import sys, numpy, time
import matplotlib.pyplot as plt

initialize(sys.argv)

#print("Global Parameters:")
#for param in psglobals.Parameters.values():
#	print(param)

#print("Global Internals:")
#for intern in psglobals.InternalsList:
#	print(intern)

#print("Nodes:\n", psglobals.NodeMap, "\n\n")

#print("Elements:", len(psglobals.ElementList))
#print("Nodes:", len(psglobals.NodeMap))

#for elem in psglobals.ElementList:
#	print(elem)

#print("\nCircuit scripts:\n")	
#for circuit in psglobals.CircuitScriptMap.values():
#	print(circuit)

#print("Total Parameters: " + str(psglobals.TotalParameters))
#print("Total Externals: " + str(psglobals.TotalExternals))

tarr = []
varr1 = []
varr2 = []
varr3 = []
varr4 = []
varr5 = []
varr6 = []

#p = find('tff1.J2')
#plist = ['tff1.XJ2', 'tff1.XJ3']
#for param in plist:
#	[pmin, pmax] = margin(param, 500)
#	print("Margins for {}, value {:.3f} [{:.2f},{:.2f}]".format(param, find(param).Value(), pmin, pmax))

#opt = Optimizer([['tff1.XJ2',0.4], ['tff1.XJ3',0.4]], ['tff1.J2', 'tff1.J3', 'tff1.I1'], 500)
#opt.optimize()

#tbegin = time.time()
#simulate(200, print_messages = True)
#telapsed = time.time() - tbegin
#print("Time {:.2f}".format(telapsed))
#sys.exit(0)
	

v1 = find('.pas', otype='e')
v2 = find('.loadaa.r1', otype='e')
#v3 = find('.tl2jaA.j1', otype='e')
#v4 = find('.tl2jaA.j2', otype='e')


InitSimulation()
#psglobals.RootCircuit.InitilizeRules()

while psglobals.CurrentTime <= 20.0:
	TimeStep()
	tarr.append(psglobals.CurrentTime)
	varr1.append(v1.I())
	varr2.append(v2.I())
#	varr3.append(v3.I())
#	varr4.append(v4.I())
#	varr5.append(v4.I())
#	varr6.append(v5.I())
#	varr6.append(v6.V())
#	(status, messages) = psglobals.RootCircuit.ProcessRules()
#	if len(messages) > 0:
#		print("----------------------------")
#		for msg in messages:
#			print(msg)




plt.plot(tarr,varr1, linestyle='-', marker='D', color = 'red')
plt.plot(tarr,varr2, linestyle='-', marker='D', color = 'blue')
#plt.plot(tarr,varr3, linestyle='-', marker='D', color = 'green')
#plt.plot(tarr,varr4, linestyle='-', marker='D', color = 'yellow')
#plt.plot(tarr,varr5, linestyle='-', marker='*', color = 'yellow')
#plt.plot(tarr,varr6, linestyle='-', marker='*', color = 'red')

plt.xlabel('T')
plt.ylabel('V')
plt.grid(True)
plt.show()
