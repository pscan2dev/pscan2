# -*- coding: utf-8 -*-
from pscan2 import *
from pscan2 import psglobals
from pscan2.Simulation import TimeStep, InitSimulation
import sys, numpy, time
import matplotlib.pyplot as plt

initialize(sys.argv)
print('sys.args', sys.argv)

tarr = []
varr1 = []
varr2 = []
varr3 = []
varr4 = []
varr5 = []

v1 = find('.j1', otype='e')
v2 = find('.i1', otype='e')
v3 = find('.n0', otype='n')
#v4 = find('.n1', otype='n')
tmax = find('tseq', otype='p').Value()
curvolt = find('.curr', otype='v').Value()

InitSimulation()
psglobals.OptimizeSimulation = False
while psglobals.CurrentTime <= tmax:
	TimeStep()
	tarr.append(psglobals.CurrentTime)
	varr1.append(v1.V())
	varr2.append(v1.I())
	varr4.append(v1.P())
#	varr5.append(v4.P())


fig0=plt.figure(num=10, figsize=(8, 4))
plt.plot(tarr,varr1, linestyle='-', marker='o', color = 'blue')

# plt.plot(tarr,varr3, linestyle='-', marker='o', color = 'blue')

plt.xlabel('T')
plt.ylabel('V')


fig1=plt.figure(num=11, figsize=(8, 4))
plt.plot(tarr,varr2, linestyle='-', marker='o', color = 'green')
#plt.plot(tarr,varr3)
plt.xlabel('T')
plt.ylabel('I')

fig2=plt.figure(num=12, figsize=(8, 4))
plt.plot(tarr,varr4, linestyle='-', marker='o', color = 'blue')
#plt.plot(tarr,varr5, linestyle='-', marker='o', color = 'gray')
plt.xlabel('T')
plt.ylabel('P')


plt.show()

