import sys
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy # to get includes

with open('pscan2/version.py') as f: exec(f.read())


cmdclass = {'build_ext': build_ext}

ext_modules = [Extension("pscan2.ElementModels_c", 
			["pscan2/ElementModels_c.pyx"], 
			include_dirs = [numpy.get_include()],
			language="c++"
			)
	]

data_files = ['ElementModels_c.pyx']


if sys.platform.startswith('linux'):
	ext_modules.append(Extension("pscan2.KLUSolver", ["pscan2/KLUSolver.pyx"], 
				include_dirs = [numpy.get_include(),
						"../../../../SuiteSparse/include"],
				libraries=["amd", "colamd", "btf", "suitesparseconfig", "klu"],
				library_dirs = ['../../../../SuiteSparse/lib']))

elif sys.platform.startswith('win'):
	ext_modules.append(Extension("pscan2.KLUSolver", ["pscan2/KLUSolver.pyx"], 
				include_dirs = [numpy.get_include(),
						"../../../external/SuiteSparse/KLU/Include",
						"../../../external/SuiteSparse/AMD/Include",
						"../../../external/SuiteSparse/COLAMD/Include",
						"../../../external/SuiteSparse/BTF/Include",
						"../../../external/SuiteSparse/SuiteSparse_config"],
				libraries=["KLU"],
				library_dirs = ['pscan2']))
	data_files.append('KLU.dll')
else:
	print("Error: unsupported platform ", sys.platform)
	sys.exit(0)

setup(name='pscan2',
      version=PSCAN2_VERSION,
      description='PSCAN2: Superconductor Circuit Simulator',
      author='Pavel Shevchenko',
      author_email='pscan2sim@gmail.com',
	  classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU Lesser General Public License v2 or later (LGPLv2+)',
        'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: Implementation :: CPython'
        'Topic :: Scientific/Engineering :: Electronic Design Automation (EDA)',
      ],
	  cmdclass = cmdclass,
	  ext_modules=ext_modules,
      license='LGPL2',
      packages=['pscan2', 'pscan2.psui'],
      package_data = {'pscan2': data_files, 'pscan2.psui':['images/*.png']})
