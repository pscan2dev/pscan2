# -*- coding: utf-8 -*-
from pscan2 import *
import sys, time

# Initilaize PSCAN2 and load circuit
initialize(sys.argv)

xi = find('XI')
xi.SetValue(1.25)

si1 = find('mt11.i1', type = 'e')
si2 = find('md2.mspl.i1', type = 'e')
hi1 = find('mha.i1', type = 'e')
hi2 = find('mha.i2', type = 'e')
hi3 = find('mha.i3', type = 'e')
hi4 = find('mha.i4', type = 'e')


tbegin = time.time()
# Simulate circuit for 64*50+40 time units. Output messages into terminal and log file
simulate(5000, print_messages = True)
telapsed = time.time() - tbegin
print("Time {:.2f}".format(telapsed))
print("XI {:.2f}".format(xi.Value()))
print("SI1 {:.2f}".format(si1.I()))
print("SI2 {:.2f}".format(si2.I()))
print("HI1 {:.2f}".format(hi1.I()))
print("HI2 {:.2f}".format(hi2.I()))
print("HI3 {:.2f}".format(hi3.I()))
print("HI4 {:.2f}".format(hi4.I()))

sys.exit(0)
