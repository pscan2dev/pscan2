# -*- coding: utf-8 -*-
from pscan2 import *
from pscan2 import globals
from pscan2.Simulation import TimeStep, InitSimulation
import sys, numpy, time
import matplotlib.pyplot as plt

initialize(sys.argv)

tarr = []
varr1 = []
varr2 = []
varr3 = []
varr4 = []
varr5 = []
varr6 = []


v1 = find('.mt19.m1.j1', 'e')
v2 = find('.mt19.m2.j1', 'e')
v3 = find('.mt19.m3.j1', 'e')
v4 = find('.mt19.m4.j1', 'e')
v5 = find('.mt19.m5.j1', 'e')
v6 = find('.mt19.m6.j1', 'e')

InitSimulation()
globals.RootCircuit.InitilizeRules()

while globals.CurrentTime <= 2000:
	TimeStep()
	tarr.append(globals.CurrentTime)
	varr1.append(v1.V())
	varr2.append(v2.V())
	varr3.append(v3.V())
	varr4.append(v4.V())
	varr5.append(v5.V())
	varr6.append(v6.V())	


plt.plot(tarr,varr1, linestyle='-', marker='D', color = 'red')
plt.plot(tarr,varr2, linestyle='-', marker='D', color = 'blue')
plt.plot(tarr,varr3, linestyle='-', marker='D', color = 'green')
plt.plot(tarr,varr4, linestyle='-', marker='D', color = 'black')
plt.plot(tarr,varr5, linestyle='-', marker='*', color = 'yellow')
plt.plot(tarr,varr6, linestyle='-', marker='*', color = 'orange')

plt.xlabel('T')
plt.ylabel('V')
plt.grid(True)
plt.show()
