# -*- coding: utf-8 -*-
from pscan2 import *
from pscan2 import globals
from pscan2.Simulation import TimeStep, InitSimulation
import sys, numpy, time
import matplotlib.pyplot as plt

initialize(sys.argv)

#print("Global Parameters:")
#for param in globals.Parameters.values():
#	print(param)

#print("Global Internals:")
#for intern in globals.InternalsList:
#	print(intern)

#print("Nodes:\n", globals.NodeMap, "\n\n")

#print("Elements:", len(globals.ElementList))
#print("Nodes:", len(globals.NodeMap))

#for elem in globals.ElementList:
#	print(elem)

#print("\nCircuit scripts:\n")	
#for circuit in globals.CircuitScriptMap.values():
#	print(circuit)

#print("Total Parameters: " + str(globals.TotalParameters))
#print("Total Externals: " + str(globals.TotalExternals))

tarr = []
varr1 = []
varr2 = []
varr3 = []
varr4 = []
varr5 = []
varr6 = []

#p = find('tff1.J2')
#plist = ['tff1.XJ2', 'tff1.XJ3']
#for param in plist:
#	[pmin, pmax] = margin(param, 500)
#	print("Margins for {}, value {:.3f} [{:.2f},{:.2f}]".format(param, find(param).Value(), pmin, pmax))

#opt = Optimizer([['tff1.XJ2',0.4], ['tff1.XJ3',0.4]], ['tff1.J2', 'tff1.J3', 'tff1.I1'], 500)
#opt.optimize()

#tbegin = time.time()
#simulate(200, print_messages = True)
#telapsed = time.time() - tbegin
#print("Time {:.2f}".format(telapsed))
#sys.exit(0)
	
xi = find('XI')
xi.SetValue(1.0)


v1 = find('mtl1.j1', type='e')
v2 = find('mtl1.j2', type='e')

InitSimulation()
globals.RootCircuit.InitilizeRules()

while globals.CurrentTime <= 400:
	TimeStep()
	tarr.append(globals.CurrentTime)
	varr1.append(v1.V())
	varr2.append(v2.V())
#	varr3.append(v3.V())
#	varr4.append(v6.I())
#	varr5.append(v4.I())
#	varr6.append(v5.I())
#	varr6.append(v6.V())
	(status, messages) = globals.RootCircuit.ProcessRules()
#	if len(messages) > 0:
#		print("----------------------------")
#		for msg in messages:
#			print(msg)




plt.plot(tarr,varr1, linestyle='-', marker='D', color = 'red')
plt.plot(tarr,varr2, linestyle='-', marker='D', color = 'blue')
#plt.plot(tarr,varr3, linestyle='-', marker='D', color = 'green')
#plt.plot(tarr,varr4, linestyle='-', marker='*', color = 'blue')
#plt.plot(tarr,varr5, linestyle='-', marker='*', color = 'yellow')
#plt.plot(tarr,varr6, linestyle='-', marker='*', color = 'red')

plt.xlabel('T')
plt.ylabel('V')
plt.grid(True)
plt.show()
