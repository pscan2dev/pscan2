# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals
from . import psconfig
import re

def substitute_model(ename, model, replace_map):
    etype = ename[0]
    if etype in replace_map:
        repl_list = replace_map[etype]
        for repl in repl_list:
            m = re.match(repl[0], model)
            if m is not None:
                repl_str = repl[1].format(name=ename)
                return(re.sub(repl[0], repl_str, model))
    return(model)

class CircuitNetlistDef:
    def __init__(self, name, nodes):
        self.name = name
        self.nodes = nodes
        self.elements = []
        self.subcircuits = []
        
    def __repr__(self):
        res = "Circuit " + self.name + str(self.nodes) + "\n"
        for elem in self.elements:
            res += str(elem) + "\n"
        for elem in self.subcircuits:
            res += str(elem) + "\n"
        return res
        
    def AddElement(self, ename, nodes, model):
        new_model = substitute_model(ename, model, psconfig.model_replace_map)
        self.elements.append((ename, nodes, new_model))
        
    def AddSubCircuit(self, sname, nodes, model):
        self.subcircuits.append((sname, nodes, model))
        
    def Name(self):
        return self.name
        
    def Nodes(self):
        return self.nodes
        
    def GetElements(self):
        return self.elements
        
    def GetSubCircuits(self):
        return self.subcircuits
        
        
def ReadSpiceCircuit(fin, nline, sname, nodes):
    nline2 = nline
    cir = CircuitNetlistDef(sname, nodes)
    line = fin.readline().lower()
    nline2 += 1
    while line != '' and line[:5] != '.ends' and line[:4] != '.end':
        line = line[:-1]
        #print("Line:" + line)
        if line[0] == '*' or line[0] == '\n' or line[:5] == "0 0 0":
            line = fin.readline().lower()
            nline += 1
            continue
        if line[:7] == '.subckt':
            lst = line.split()
            if len(lst) > 2:
                psglobals.CircuitNetlists[lst[1].lower()] = ReadSpiceCircuit(fin, nline2, lst[1], lst[2:])
        else:
            lst = line.split()
            elem = lst[0]
            if elem[0] == 'x':
                # Subcircuit can connect to any number of nodes. Last element is the circuit name
                cir.AddSubCircuit(elem[1:], lst[1:-1], lst[-1])
            elif elem[0] == 'm':
                # Mutual inductance connected to one node
                model = ' '.join(lst[2:])
                cir.AddElement(elem, lst[1:2], model)
            else:
                # Other elements connected to two nodes
                model = ' '.join(lst[3:])
                cir.AddElement(elem, lst[1:3], model)
        line = fin.readline().lower()
        nline += 1
    return cir
    

def ReadSpice(circuit_name, fname):
    print("Load circuit {} from the netlist file {}".format(circuit_name, fname))
    fin = open(fname, 'r')
    psglobals.CircuitNetlists[circuit_name] = ReadSpiceCircuit(fin, 1, circuit_name, [])

if __name__ == '__main__':
    ReadSpice("test3", "test3.cir")
    print(psglobals.CircuitNetlists)
