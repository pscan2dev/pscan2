﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import sys
from . import pscan_lex
import ply.yacc as yacc
from .ExpressionAST import *

start = 'expr'

tokens = (
	# Literals (identifier, integer constant, double constant, string constant)
    'IDENT','ICONST', 'FCONST', 'SCONST',

    # Operators
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 
    'AND', 'OR', 'NOT', 'EQ', 'NE', 'GE', 'LE', 'GT', 'LT',

    # Delimeters ( ) [ ] { } , . ; :
    'LPAREN', 'RPAREN','COMMA'
	)

precedence =  [
	('left', 'OR'), 
	('left', 'AND'), 
	('left', 'EQ', 'NE'), 
	('left', 'LE', 'GE', 'LT', 'GT'), 
	('left', 'PLUS', 'MINUS'), 
	('left', 'TIMES', 'DIVIDE'), 
	('right', 'UNISUB', 'NOT')]

	
def p_expr_list_1(p):
	'''expr_list : '''
	p[0] = []

def p_expr_list_2(p):
	'''expr_list : expr'''
	p[0] = [p[1]]

def p_expr_list_3(p):
	'''expr_list : expr_list COMMA expr'''
	p[0] = p[1] + [p[3]]


#### Arithmetic expressions

def p_expr_binary(p):
	'''expr : expr PLUS expr
            | expr MINUS expr
            | expr TIMES expr
            | expr DIVIDE expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_BINARY, p[2], p[1], p[3])

def p_expr_iconst(p):
	'''expr : ICONST'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_FCONST, int(p[1]))

def p_expr_fconst(p):
	'''expr : FCONST'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_FCONST, float(p[1]))

def p_expr_sconst(p):
	'''expr : SCONST'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_SCONST, p[1])

def p_expr_ident(p):
	'''expr : IDENT'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_IDENT, p[1])

def p_expr_fcall(p):
	'''expr : IDENT LPAREN expr_list RPAREN'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_FCALL, p[1], p[3])

def p_expr_group(p):
	'''expr : LPAREN expr RPAREN'''
	p[0] = p[2]

def p_expr_unary(p):
	'''expr : MINUS expr %prec UNISUB'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_UNARY, p[1], p[2])

#### Relational expressions

def p_expr_rel(p):
	'''expr : expr LT expr
               | expr LE expr
               | expr GT expr
               | expr GE expr
               | expr EQ expr
               | expr NE expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_BINARY, p[2], p[1], p[3])

#### Logical expressions

def p_expr_log(p):
	'''expr : expr OR expr
               | expr AND expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_BINARY, p[2], p[1], p[3])

def p_expr_not(p):
	'''expr : NOT expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_UNARY, p[1], p[2])

def p_error(t):
	if t:
		print("Syntax error at '%s'" % t.value)
	else:
		print("Syntax error at EOF")

# optimize=1 - load parsetab.py on startup
expr_parser = yacc.yacc(debug=False, tabmodule="expr_parsetab", optimize=True)

if __name__ == "__main__":
	inptext = r"""1+2"""
	res = expr_parser.parse(inptext, lexer=pscan_lex.lexer, debug=1)

	print(res)

