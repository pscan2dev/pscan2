# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from .expr_parser import expr_parser
from . import pscan_lex
from . import psglobals
from .Expression import Expression
from .ExpressionAST import ExpressionAST

from .ElementModels_c import InductorModel, ResistorModel, CapacitorModel, \
    CurrentSourceModel, PhaseSourceModel, VoltageSourceModel, JunctionModel, \
    JunctionRSJNModel, JunctionRSJPHModel, JunctionTJMModel, InductorImpedanceModel, \
    MutualInductanceModel


class Element:
    """
    Element in the circuit

    Methods
    ---------
    Name() : string
        Name of the element
    ElementType() : one-character string
        Type of the element
    I() : float
        Current through the element
    P() : float
        Phase across the element
    V() : float
        Voltage across the element
    N() : int
        Number of flux quanta passed the element
    Inc() : int
        Equal to 1 at the moment, when attribute N changes in positive direction
        (flux going through the element)
    Dec() : int
        Equal to 1 at the moment, when attribute N changes in negative direction
        (anti-flux going through the element)
    """
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        self.name = name
        self.etype = ''
        self.circuit = circuit
        cir_fname = circuit.FullName()
        if cir_fname == ".":
            self.full_name = self.name
        else:
            self.full_name = cir_fname[1:] + "." + self.name
        self.index = index
        self.value_index = value_index
        self.nodes = nodes
        self.virtual_nodes = virtual_nodes
        self.model_ast = expr_parser.parse(model, lexer=pscan_lex.lexer)
        if self.model_ast is None:
            raise Exception('Invalid model for element: ' + self.full_name + ' model: ' + model)
        self.model_args = ()
        self.model_parameters = []
        self.impedance_model = False

    def __repr__(self):
        if len(self.model_args) > 0:
            return("Elem: {} {} {} [{}] = {}".format(self.name, str(self.nodes), 
                                                     str(self.virtual_nodes),
                                                     self.index, str(self.model_args)))
        else:
            return("Elem: {} {} {} [{}] = {}".format(self.name, str(self.nodes),
                                                     str(self.virtual_nodes),
                                                     self.index, str(self.model_ast)))
    def Name(self):
        return(self.name)
        
    def FullName(self):
        return self.full_name    

    def FindType(self):
        return('e')

    def Index(self):
        return(self.index)
        
    def ValueIndex(self):
        return(self.value_index)

    def Nodes(self):
        return(self.nodes)

    def VirtualNodes(self):
        return(self.virtual_nodes)

    def ElementType(self):
        return(self.etype)

    def IsImpedanceTypeModel(self):
        return(self.impedance_model)

    def SetVirtualNodes(self, virtual_nodes):
        self.virtual_nodes = virtual_nodes
        self.impedance_model = True

    def I(self):
        return(psglobals.ElementCurrent[self.value_index])

    def P(self):
        return(psglobals.ElementPhase[self.value_index])

    def V(self):
        return(psglobals.ElementVoltage[self.value_index])

    def N(self):
        return(psglobals.ElementN[self.value_index])

    def Inc(self):
        return(psglobals.ElementInc[self.value_index])

    def Dec(self):
        return(psglobals.ElementDec[self.value_index])

    def UpdateModel(self):
        #print("UpdateModel: ", self)
        self.model_args = (Expression(self.model_ast, self.circuit, False),)
        self.UpdateParameters()

    def UpdateParameters(self):
        #print("UpdateParameters: ", self)
        self.model_parameters = []
        for arg in self.model_args:
            self.model_parameters.append(arg.Value())
        self.model.SetParameters(self.model_parameters)
            
class Inductor(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        self.model = InductorModel(self.nodes[0], self.nodes[1], self.value_index)
        self.etype = 'l'

    def SwitchToImpedanceModel(self, virtual_node):
        self.virtual_nodes = [virtual_node]
        self.model = InductorImpedanceModel(self.nodes[0], self.nodes[1], virtual_node, self.value_index)
        self.impedance_model = True

class Resistor(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        self.model = ResistorModel(self.nodes[0], self.nodes[1], self.value_index)
        self.etype = 'r'

class Capacitor(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        self.model = CapacitorModel(self.nodes[0], self.nodes[1], self.value_index)
        self.etype = 'c'

class CurrentSource(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        self.model = CurrentSourceModel(self.nodes[0], self.nodes[1], self.value_index)
        self.etype = 'i'

class VoltageSource(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        # Create a new virtual node
        psglobals.TotalNodes += 1
        psglobals.TotalVirtualNodes += 1
        virtual_node = psglobals.TotalNodes
        self.virtual_nodes = [virtual_node]
        
        self.model = VoltageSourceModel(self.nodes[0], self.nodes[1], virtual_node, self.value_index)
        self.etype = 'u'

    def GetPhaseChange(self):
        return(0.0)

class PhaseSource(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        # Create a new virtual node
        psglobals.TotalNodes += 1
        psglobals.TotalVirtualNodes += 1
        virtual_node = psglobals.TotalNodes
        self.virtual_nodes = [virtual_node]

        self.model = PhaseSourceModel(self.nodes[0], self.nodes[1], virtual_node, self.value_index)
        self.etype = 'p'

class Junction(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        self.model = JunctionModel(self.nodes[0], self.nodes[1], self.value_index)
        self.etype = 'j'

    def UpdateModel(self):
        model_exp = Expression(self.model_ast, self.circuit, False)
        self.model_args = tuple(model_exp.fargs)
        self.model_parameters = []
        for arg in self.model_args:
            self.model_parameters.append(arg.Value())

        if model_exp.etype == ExpressionAST.EXP_TYPE_FCALL and \
            model_exp.fname == "rsjn" and len(model_exp.fargs) == 5:
            # Change model to RSJN
            self.model = JunctionRSJNModel(self.nodes[0], self.nodes[1], self.value_index)  
        elif model_exp.etype == ExpressionAST.EXP_TYPE_FCALL and \
            model_exp.fname == "rsjph" and len(model_exp.fargs) == 4:
            # Change model to RSJN
            self.model = JunctionRSJPHModel(self.nodes[0], self.nodes[1], self.value_index)
        elif model_exp.etype == ExpressionAST.EXP_TYPE_FCALL and \
            model_exp.fname == "tjm" and len(model_exp.fargs) == 6:
            # Change model to TJM
            self.model = JunctionTJMModel(self.nodes[0], self.nodes[1], self.value_index)
            self.model.InitTJMModel(self.model_parameters)
        elif model_exp.etype != ExpressionAST.EXP_TYPE_FCALL or \
            model_exp.fname != "rsj" or len(model_exp.fargs) != 3:
            raise Exception("Invalid model for Junction: " + model_exp.fname + \
		" model: " + str(model_exp.fargs))
        self.model_args = tuple(model_exp.fargs)
        self.UpdateParameters()

#class Transformer(Element):
#   def __init__(self, name, circuit, index, value_index, nodes, model):
#       Element.__init__(self, name, circuit, index, value_index, nodes, model)
#       self.model = TransformerModel(self.nodes[0], self.nodes[1], self.nodes[2], self.nodes[3], self.index)
#       self.etype = 't'

#   def UpdateModel(self):
#       model_exp = Expression(self.model_ast, self.circuit, False)
#       if model_exp.etype != ExpressionAST.EXP_TYPE_FCALL or \
#           model_exp.fname != "trans" or len(model_exp.fargs) != 3:
#           raise Exception("Invalid model for Transformer: ")
#       self.model_args = tuple(model_exp.fargs)
#       self.UpdateParameters()

class MutualInductance(Element):
    def __init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model):
        Element.__init__(self, name, circuit, index, value_index, nodes, virtual_nodes, model)
        self.etype = 'm'
        if self.model_ast.etype != ExpressionAST.EXP_TYPE_FCALL or self.model_ast.fname != "llm":
            raise Exception("Invalid model for Mutual Inductance: " + name + " in " + circuit.Name())
        model_exp = Expression(self.model_ast, self.circuit, False)
        if not isinstance(model_exp.l1elem, Element) or model_exp.l1elem.ElementType() != 'l':
            raise Exception("Invalid first parameter for Mutual Inductance model: " + str(model_exp.l1elem))

        if not isinstance(model_exp.l2elem, Element) or model_exp.l2elem.ElementType() != 'l':
            raise Exception("Invalid second parameter for Mutual Inductance model: " + str(model_exp.l2elem))

        # First inductor
        nodes1 = model_exp.l1elem.Nodes()
        if model_exp.l1elem.IsImpedanceTypeModel():
            # Already impedance-type model. Use assigned virtual node
            if len(model_exp.l1elem.VirtualNodes()) == 1:
                virtual_node1 = model_exp.l1elem.VirtualNodes()[0]
            else:
                raise Exception("Error processing mutual inductance " + name + \
                    " L1 inductance model is wrong " + repr(model_exp.l1elem.VirtualNodes()))
        else:
            # Create a new virtual node
            psglobals.TotalNodes += 1
            psglobals.TotalVirtualNodes += 1
            virtual_node1 = psglobals.TotalNodes
            model_exp.l1elem.SwitchToImpedanceModel(virtual_node1)
            
        # Second inductor
        nodes2 = model_exp.l2elem.Nodes()
        if model_exp.l2elem.IsImpedanceTypeModel():
            # Already impedance-type model. Use assigned virtual node
            if len(model_exp.l2elem.VirtualNodes()) == 1:
                virtual_node2 = model_exp.l2elem.VirtualNodes()[0]
            else:
                raise Exception("Error processing mutual inductance " + name + \
                    " L2 inductance model is wrong " + repr(model_exp.l2elem.VirtualNodes()))
        else:
            # Create a new virtual node
            psglobals.TotalNodes += 1
            psglobals.TotalVirtualNodes += 1
            virtual_node2 = psglobals.TotalNodes
            model_exp.l2elem.SwitchToImpedanceModel(virtual_node2)

        self.model = MutualInductanceModel(virtual_node1, virtual_node2, value_index)
        self.virtual_nodes = [virtual_node1, virtual_node2]
        

    def UpdateModel(self):
        self.model_args = (Expression(self.model_ast.fargs[2], self.circuit, False),)
        self.UpdateParameters()


def CreateElement(name, circuit, index, value_index, nodes, virtual_nodes, model):
    etype = name[0]
    if etype == 'l' :
        return Inductor(name, circuit, index, value_index, nodes, virtual_nodes, model)
    elif etype == 'r' :
        return Resistor(name, circuit, index, value_index, nodes, virtual_nodes, model)
    elif etype == 'c' :
        return Capacitor(name, circuit, index, value_index, nodes, virtual_nodes, model)
    elif etype == 'j' :
        return Junction(name, circuit, index, value_index, nodes, virtual_nodes, model)
    elif etype == 'i' :
        return CurrentSource(name, circuit, index, value_index, nodes, virtual_nodes, model)
    elif etype == 'u' :
        return VoltageSource(name, circuit, index, value_index, nodes, virtual_nodes, model)
    elif etype == 'p' :
        return PhaseSource(name, circuit, index, value_index, nodes, virtual_nodes, model)
    elif etype == 'm' :
        return MutualInductance(name, circuit, index, value_index, nodes, virtual_nodes, model)
    else:
        raise Exception("Unknown element type " + etype)
