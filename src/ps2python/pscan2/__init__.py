# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import os
from .util import *
from .LoadCircuit import LoadCircuit
from .Optimizer import Optimizer
from .GenerateHDL import ProcessSpice
from . import psglobals
from . import SimulationParameters
from . import psconfig
from . import version

# Disable warnings
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# For testing
#from .ElementModels_c import BuildStaticMatrix, BuildMatrix, UpdateMatrix, UpdateRHS, UpdateValues, InitElementModels

__all__ = ['util', 'Optimizer', 'initialize', 'simulate', 'margin', 
		   'find', 'find_re', 'param_find', 'param_re', 'all_externals', 'load_parameters', 
		   'save_parameters', 'ivcurve', 'psglobals', 'SimulationParameters', 'psconfig', 'ProcessSpice',
		   'InitSimulation', 'TimeStep', 'TestVector', 'save_all_parameters', 'load_all_parameters', 'version']

def PscanVersion():
	return version.PSCAN2_VERSION

def load_ini_files(glbl):
	try:
		with open(os.path.expanduser("~/.pscanrc.py"), "r") as fh:
			exec(fh.read()+"\n", glbl, {})
	except:
		pass

	try:			
		with open("pscanrc.py", "r") as fh:
			exec(fh.read()+"\n", glbl, {})
	except:
		pass

print("PSCAN2: Superconductor Circuit Simulator version {}".format(PscanVersion()))
print("Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)")

_initialize_flag = False

def initialize(argv):
	"""
	initialize PSCAN2 and load circuit netlist and definition files

	-----------

	<args> is the array of arguments, where the last argument is the 
	name of root circuit and base name of the circuit netlist file 
	(full name of the file will have “.cir” extension appended). 
	All previous arguments, if existed, will be names of circuit 
	definition files, preloaded into memory.  After loading netlist file, 
	PSCAN2 will try to load circuit definition files used in the netlist. 
	If circuit definition was preloaded, it will use that definition, 
	otherwise it will try to load corresponding “.hdl” file.
	"""
	global _initialize_flag
	if _initialize_flag:
		return

	initialize_logger(argv[1])

	load_ini_files({'psglobals':psglobals, 
				 'SimulationParameters':SimulationParameters,
				 'psconfig':psconfig})
	LoadCircuit(argv[1], argv[2:])
	_initialize_flag = True
