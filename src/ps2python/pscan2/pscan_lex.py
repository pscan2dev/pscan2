﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import ply.lex as lex

# Reserved words
reserved = {
    'circuit':'CIRCUIT', 'parameter':'PARAMETER', 'external':'EXTERNAL', 'internal':'INTERNAL', 
	'value':'VALUE', 'rule':'RULE', 'freeze':'FREEZE',
	'and':'AND', 'or':'OR', 'not':'NOT',
	'eq':'EQ', 'ne':'NE', 'ge':'GE', 'le':'LE', 'gt':'GT', 'lt':'LT'}

expression_tokens = (
	# Literals (identifier, integer constant, double constant, string constant)
    'IDENT','ICONST', 'FCONST', 'SCONST',

    # Operators
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 
    
    # Assignment (=)
    'EQUALS',
    
    # Delimeters ( ) [ ] { } , . ; :
    'LPAREN', 'RPAREN',
    'LBRACKET', 'RBRACKET',
    'LBRACE', 'RBRACE',
    'COMMA', 'POINT', 'SEMI'
	)

tokens = tuple(reserved.values()) + expression_tokens

# Completely ignored characters
t_ignore           = ' \t\x0c'

# Newlines
def t_NEWLINE(t):
	r'\n+'
	t.lexer.lineno += t.value.count("\n")

# Operators
t_PLUS             = r'\+'
t_MINUS            = r'-'
t_TIMES            = r'\*'
t_DIVIDE           = r'/'
t_OR               = r'\|\|'
t_AND              = r'&&'
t_NOT              = r'!'
t_LT               = r'<'
t_GT               = r'>'
t_LE               = r'<='
t_GE               = r'>='
t_EQ               = r'=='
t_NE               = r'!='

# Assignment operators

t_EQUALS           = r'='

# Delimeters
t_LPAREN           = r'\('
t_RPAREN           = r'\)'
t_LBRACKET         = r'\['
t_RBRACKET         = r'\]'
t_LBRACE           = r'\{'
t_RBRACE           = r'\}'
t_COMMA            = r','
t_POINT            = r'\.'
t_SEMI             = r';'

# Identifiers and reserved words

def t_IDENT(t):
	r'[A-Za-z_][\w_]*'
	t.value = t.value.lower()
	if t.value in reserved:
		t.type = reserved[t.value]
	return t

# Integer literal
t_ICONST = r'\d+'

# Floating literal
t_FCONST = r'((\d+)(\.\d+)(e(\+|-)?(\d+))? | (\d+)e(\+|-)?(\d+))'

# String literal
def t_SCONST(t):
    r'\"([^\\\n]|(\\.))*?\"'
    t.value = t.value[1:-1]
    return t

# Comments
def t_comment(t):
	r'\#.*'

def t_error(t):
	print("Illegal character %s" % repr(t.value[0]))
	t.lexer.skip(1)

lexer = lex.lex(optimize = True, lextab='pslextab')
if __name__ == "__main__":
	lex.runmain(lexer)
