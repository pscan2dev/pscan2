#!/usr/bin/env python

"""
setup_models.py  to build element models code with cython
"""
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy # to get includes


setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("KLUSolver", 
				   ["KLUSolver.pyx"], 
				   include_dirs = [numpy.get_include(),
								   "../../../external/SuiteSparse/KLU/Include",
								   "../../../external/SuiteSparse/AMD/Include",
								   "../../../external/SuiteSparse/COLAMD/Include",
								   "../../../external/SuiteSparse/BTF/Include",
								   "../../../external/SuiteSparse/SuiteSparse_config"],
				   libraries=["KLU"],
				   library_dirs = ['../../../external/SuiteSparse/vs2015/KLU/Release'])]
)
