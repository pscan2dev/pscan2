﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals
from .Expression import Expression
from .ExpressionAST import ExpressionAST


class Rule:
    def __init__(self, circuit, ruledef):
        self.circuit = circuit
        self.name = ruledef.name
        cir_fname = circuit.FullName()
        if cir_fname == ".":
            self.full_name = self.name
        else:
            self.full_name = cir_fname[1:] + "." + self.name
        self.exp_index = 0
        self.exp_list = [Expression(ruledef.initexp, circuit, True)]
        self.incdec_list = []
        for exp in ruledef.explist:
            self.exp_list.append(Expression(exp, circuit, True))
        for exp in self.exp_list:
            self.incdec_list.append(exp.IncDecList())

    def Name(self):
        return self.name
        
    def FullName(self):
        return self.full_name

    def FindType(self):
        return('r')

    def IsActive(self):
        return(self.exp_index != 0)

    def CurrentState(self):
        if self.exp_index != 0:
            return((self.full_name, self.exp_index))
        else:
            return(None)
            
    def Value(self):
        return self.exp_index

    def Initialize(self):
        self.exp_index = 0
        for exp in self.exp_list:
            if exp.etype == ExpressionAST.EXP_TYPE_SEQUENCE:
                for inner_exp in exp.explist:
                    inner_exp.status = False
        self.circuit.AddIncDec(self.incdec_list[0])

    def Step(self):
        done = False
        messages = []
        while not done:
            if self.exp_list[self.exp_index].etype == ExpressionAST.EXP_TYPE_SEQUENCE:
                explist = self.exp_list[self.exp_index].explist
                finished = True
                for exp in explist:
                    if not exp.status:
                        if exp.Value():
                            exp.status = True
                            messages.append((psglobals.CurrentTime, self.full_name, self.exp_index, exp.eindex, 'passed'))
                            # Update JJ IncDec ???
                        else:
                            finished = False
                if finished:
                    for exp in explist:
                        exp.status = False
                    self.circuit.RemoveIncDec(self.incdec_list[self.exp_index])
                    if self.exp_index == len(self.exp_list) - 1:
                        self.exp_index = 0
                        messages.append((psglobals.CurrentTime, self.full_name, 'deactivated'))
                        done = True
                    else:
                        self.exp_index += 1
                    self.circuit.AddIncDec(self.incdec_list[self.exp_index])
                else:
                    done = True
            else:
                msg_lst = [""]
                if self.exp_list[self.exp_index].Value(msg_lst):
                    if self.exp_index == 0:
                        messages.append((psglobals.CurrentTime, self.full_name, 'activated'))
                    else:
                        if msg_lst[0] != "":
                            messages.append((psglobals.CurrentTime, self.full_name, self.exp_index, msg_lst[0], 'message'))
                        messages.append((psglobals.CurrentTime, self.full_name, self.exp_index, 'passed'))
                    self.circuit.RemoveIncDec(self.incdec_list[self.exp_index])
                    if self.exp_index == len(self.exp_list) - 1:
                        self.exp_index = 0
                        messages.append((psglobals.CurrentTime, self.full_name, 'deactivated'))
                        done = True
                    else:
                        self.exp_index += 1
                    self.circuit.AddIncDec(self.incdec_list[self.exp_index])
                else:
                    done = True
        return(messages)

