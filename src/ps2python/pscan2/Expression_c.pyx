# cython: boundscheck=False, wraparound=False, cdivision=True, language_level=3, linetrace=False, binding=False, profile=False

# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import numpy as np
# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution).
cimport numpy as np

import cython

#from cpython cimport array
#import array

from . import psglobals

from libc cimport math
		
cdef enum:
	EXP_TYPE_INVALID = -1,
	EXP_TYPE_ICONST = 1,
	EXP_TYPE_FCONST = 2,
	EXP_TYPE_SCONST = 3,
	EXP_TYPE_IDENT = 4,
	EXP_TYPE_UNARY = 5,
	EXP_TYPE_BINARY = 6,
	EXP_TYPE_FCALL = 7,
	EXP_TYPE_SEQUENCE = 8,
	EXP_TYPE_POINTS = 9,
	EXP_TYPE_INDEXED_VAL = 10,
	EXP_TYPE_INT_INDEXED_VAL = 11,
	EXP_TYPE_UCHAR_INDEXED_VAL = 12
	
cdef enum:
	EXP_OP_INVALID = -1,
	EXP_OP_NOT = 1,
	EXP_OP_GT = 2,
	EXP_OP_GE = 3,
	EXP_OP_LT = 4,
	EXP_OP_LE = 5,
	EXP_OP_NE = 6,
	EXP_OP_EQ = 7,
	EXP_OP_AND = 8,
	EXP_OP_OR = 9,
	EXP_OP_PLUS = 10,
	EXP_OP_MINUS = 11,
	EXP_OP_MULT = 12,
	EXP_OP_DIVIDE = 13
	
cdef enum:
	EXP_FCALL_SET = 1,
	EXP_FCALL_ASSIGN = 2,
	EXP_FCALL_PSFQ = 3
	
		
cdef inline double smooth_step(double x):
	if x <= 0.0:
		return(0.0)
	elif x >= 1.0:
		return(1.0)
	else:
		return(-2.0*x*x*x+3.0*x*x)
		

cdef class Expression_c:
	cdef int expr_type
	cdef int op
	cdef int fcall_type
	cdef double const_val
	cdef int pindex
	cdef double* parray
	cdef int* iarray
	cdef unsigned char* ucarray
	cdef Expression_c op1
	cdef Expression_c op2
	cdef Expression_c op3
	
	def __init__(self):
		self.expr_type = EXP_TYPE_INVALID

	def InitConst(self, int expr_type, double const_val):
		self.expr_type = expr_type
		self.const_val = const_val
		
	def InitIndexArray(self, int expr_type, int pindex, parray):
		cdef np.ndarray[np.float64_t, ndim=1, mode="c"] fview
		cdef np.ndarray[np.int32_t, ndim=1, mode="c"] iview
		cdef np.ndarray[np.uint8_t, ndim=1, mode="c"] cview

		self.expr_type = expr_type
		self.pindex = pindex
		if expr_type == EXP_TYPE_INDEXED_VAL:
			fview = parray
			self.parray = <double *>fview.data
		elif expr_type == EXP_TYPE_INT_INDEXED_VAL:
			iview = parray
			self.iarray = <int *>iview.data
		elif expr_type == EXP_TYPE_UCHAR_INDEXED_VAL:
			cview = parray
			self.ucarray = <unsigned char *>cview.data
		else:
			# Error
			pass

	def InitUnaryOp(self, int expr_type, int op, Expression_c op_exp):
		self.expr_type = expr_type
		self.op = op
		self.op1 = op_exp
		
	def InitBinaryOp(self, int expr_type, int op, Expression_c exp1, Expression_c exp2):
		self.expr_type = expr_type
		self.op = op
		self.op1 = exp1
		self.op2 = exp2
		
	def InitFcallArg1(self, int expr_type, int fcall_type, Expression_c arg1):
		self.expr_type = expr_type
		self.fcall_type = fcall_type
		self.op1 = arg1
		
	def InitFcallArg2(self, int expr_type, int fcall_type, Expression_c arg1, Expression_c arg2):
		self.expr_type = expr_type
		self.fcall_type = fcall_type
		self.op1 = arg1
		self.op2 = arg2
		
	def InitFcallSet(self, int expr_type, int fcall_type, int pindex, 
						np.ndarray[np.int32_t, ndim=1, mode="c"] iarray not None):
		self.expr_type = expr_type
		self.fcall_type = fcall_type
		self.pindex = pindex
		self.iarray = <int *>iarray.data
		
	def InitFcallAssign(self, int expr_type, int fcall_type, int pindex, 
						np.ndarray[np.float64_t, ndim=1, mode="c"] parray not None, Expression_c val):
		self.expr_type = expr_type
		self.fcall_type = fcall_type
		self.pindex = pindex
		self.parray = <double *>parray.data
		self.op1 = val
		
	def InitFcallPsfq(self, int expr_type, int fcall_type, Expression_c p, Expression_c d, Expression_c s):
		self.expr_type = expr_type
		self.fcall_type = fcall_type
		self.op1 = p
		self.op2 = d
		self.op3 = s
		
	def Value(self):
		cdef double dt
		cdef double op1_val
		cdef double op2_val
		cdef double op3_val
		
		if self.expr_type == EXP_TYPE_ICONST:
			return(self.const_val)
		elif self.expr_type == EXP_TYPE_FCONST:
			return(self.const_val)
		elif self.expr_type == EXP_TYPE_IDENT:
			return(self.parray[self.pindex])
		elif self.expr_type == EXP_TYPE_INDEXED_VAL:
			return(self.parray[self.pindex])
		elif self.expr_type == EXP_TYPE_INT_INDEXED_VAL:
			return(self.iarray[self.pindex])
		elif self.expr_type == EXP_TYPE_UCHAR_INDEXED_VAL:
			return(self.ucarray[self.pindex])
		elif self.expr_type == EXP_TYPE_UNARY:
			if self.op == EXP_OP_NOT:
				if self.op1.Value() == 1.0:
					return(0.0)
				else:
					return(1.0)
			elif self.op == EXP_OP_PLUS:
				return(self.op1.Value())
			elif self.op == EXP_OP_MINUS:
				return(-self.op1.Value())
			else:
				return(0.0)
		elif self.expr_type == EXP_TYPE_BINARY:
			op1_val = self.op1.Value()
			op2_val = self.op2.Value()
			if self.op == EXP_OP_MULT:
				return(op1_val * op2_val)
			elif self.op == EXP_OP_PLUS:
				return(op1_val + op2_val)
			elif self.op == EXP_OP_MINUS:
				return(op1_val - op2_val)
			elif self.op == EXP_OP_DIVIDE:
				return(op1_val / op2_val)
			elif self.op == EXP_OP_GT:
				if op1_val > op2_val:
					return(1.0)
				else:
					return(0.0)
			elif self.op == EXP_OP_GE:
				if op1_val >= op2_val:
					return(1.0)
				else:
					return(0.0)
			elif self.op == EXP_OP_LT:
				if op1_val < op2_val:
					return(1.0)
				else:
					return(0.0)
			elif self.op == EXP_OP_LE:
				if op1_val <= op2_val:
					return(1.0)
				else:
					return(0.0)
			elif self.op == EXP_OP_NE:
				if op1_val != op2_val:
					return(1.0)
				else:
					return(0.0)
			elif self.op == EXP_OP_EQ:
				if op1_val == op2_val:
					return(1.0)
				else:
					return(0.0)
			elif self.op == EXP_OP_AND:
				if op1_val == 1 and op2_val == 1:
					return(1.0)
				else:
					return(0.0)
			elif self.op == EXP_OP_OR:
				if op1_val == 1 or op2_val == 1:
					return(1.0)
				else:
					return(0.0)
			else:
				return(0.0)
		elif self.expr_type == EXP_TYPE_FCALL:
			if self.fcall_type == EXP_FCALL_SET:
				self.iarray[self.pindex] = 1
				return(1.0)
			elif self.fcall_type == EXP_FCALL_ASSIGN:
				self.parray[self.pindex] = self.op1.Value()
				return(1.0)
			elif self.fcall_type == EXP_FCALL_PSFQ:
				# !!!!!!!
				op1_val = self.p.Value()
				op2_val = self.d.Value()
				op3_val = self.s.Value()
				dt = psglobals.CurrentTime - op3_val

				if dt < 0:
					return(0.0)
				else:
					return(2.0 * math.M_PI * (math.floor(dt / op1_val) + smooth_step((math.fmod(dt,op1_val)/op2_val))))
				pass
			else:
				return(0.0)
		else:
			return(0.0)
