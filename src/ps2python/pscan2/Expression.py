﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

# September 14, 2018, PSFQN function is added by V. Semenov 
# September 16, 2018, PSFQN function is deleted by V. Semenov
# September 16, 2018, Function stepsWithPeriods and two Expressions
# are added by V. Semenov

# The new additions solve the known problem with perioding repetion
# of pulse patterns. This is a posibility to appearance of a new pulse
# when calculation time is close to expires.   
# The problem solved if the number of repetitions is predefined.
# There is a natural way to add the number of repetition if
# we offer the following mnemonic rule:
# NPSFQ(N, P, W, T1, T2, ..., Tn ) contains the following list of input parameters:
# N - Number of repetitions. (N = 1 corresponds to "no repetitions.)
# P - Period of repetitions.
# W - Width of pulses
# T1, T2 ... is any desired sequence of times when pulses are generated.
# SFQ is a mnemonic notation of the shape of the pulses.
# 
# Similarly we can introduce a similar generator of rectangle pulses.
# Such generators are indipensable for simulations of memory cells.
# NPRECT(N, P, W, T1, T2, T3, T4) contains the following list of input parameters: 
# N - Number of repetitions. (N = 1 corresponds to "no repetitions.)
# P - Period of repetitions.
# W - Width of raising/falling fronts
# T1, T2 ... is any desired sequence of times when pulses are generated.
# With the following reservation:
# We know that time (tcurr) is always positive. As a result, we can use
# signs of T1, T2, ... Tn to mark raising and falling fronts. 
# This option allows to create sequence of positive and negative pulses
# that are convenient to simulate writing and reading patterns
# RECT is a mnemonic notation of the shape of the pulses.
 
# The new mnemonic is compatible with the original generator PSFQ.
# Now PSFQ( P, W, T1) means
# P - Period of repetitions.
# W - Width of raising/falling fronts
# T1 - Time when the pulse is generated.

import math, random
from . import psglobals
from . import SimulationParameters as SP
from .ExpressionAST import ExpressionAST
from .Node import Node

def smooth_step(x):
    if x <= 0.0:
        return(0.0)
    elif x >= 1.0:
        return(1.0)
    else:
        return(-2.0*x*x*x+3.0*x*x)

# -------- new step function 2018-09-16 ------
def stepsWithPeriods(N, period, width, iCurr):
# N - the number of requested periods
# period, width - period and width of the sequence of jumps
# iCurr = current time - time of the first jump 
    tmpN = min(max(0.0,((iCurr - width)// period +1.0)), N-1) # // is the "floor divide" operator
    dt = (iCurr - tmpN*period)/width # "stretched time for smooth_step
#    print('st func in ', N, period, width, iCurr, end = ' .. ')
#    print('st func out ', tmpN, dt, smooth_step(dt))  
    return(tmpN + smooth_step(dt))
# -----------end of new step function --------

class Expression:
    def __init__(self, ast_exp, circuit, phase_dependence = False):
        self.circuit = circuit
        if circuit == None:
            local_params = {}
            local_externals = {}
            local_internals = {}
            local_values = {}
        else:
            local_params = circuit.Parameters()
            local_externals = circuit.Externals()
            local_internals = circuit.Internals()
            local_values = circuit.Values()
        self.etype = ast_exp.etype
        if self.etype == ExpressionAST.EXP_TYPE_ICONST:
            self.iconst_val = ast_exp.iconst_val
        elif self.etype == ExpressionAST.EXP_TYPE_FCONST:
            self.fconst_val = ast_exp.fconst_val
        elif self.etype == ExpressionAST.EXP_TYPE_SCONST:
            self.sconst_val = ast_exp.sconst_val
        elif self.etype == ExpressionAST.EXP_TYPE_IDENT:
            self.ident = ast_exp.ident
            if self.ident == "tcurr":
                self.index = 0
                self.array = psglobals.InternalValues
            elif self.ident in local_params:
                self.index = local_params[self.ident].Index()
                self.array = psglobals.ParameterValues
            elif self.ident in local_externals:
                self.index = local_externals[self.ident].Index()
                self.array = psglobals.ExternalValues
            elif self.ident in local_internals:
                self.index = local_internals[self.ident].Index()
                self.array = psglobals.InternalValues
            elif phase_dependence and self.ident in local_values:
                self.index = local_values[self.ident].Index()
                self.array = psglobals.ValueValues
            elif self.ident in psglobals.Parameters:
                self.index = psglobals.Parameters[self.ident].Index()
                self.array = psglobals.ParameterValues
            elif self.ident in psglobals.Internals:
                self.index = psglobals.Internals[self.ident].Index()
                self.array = psglobals.InternalValues
            else:
                self.etype == ExpressionAST.EXP_TYPE_INVALID
                raise Exception("Unknown identifier " + self.ident + " in " + str(circuit))
        elif self.etype == ExpressionAST.EXP_TYPE_UNARY:
            self.operator = ast_exp.operator
            self.operand = Expression(ast_exp.operand, circuit, phase_dependence)
        elif self.etype == ExpressionAST.EXP_TYPE_BINARY:
            self.operator = ast_exp.operator
            self.operand1 = Expression(ast_exp.operand1, circuit, phase_dependence)
            self.operand2 = Expression(ast_exp.operand2, circuit, phase_dependence)
        elif self.etype == ExpressionAST.EXP_TYPE_FCALL:
            self.func = None
            self.fname = ast_exp.fname.lower()
            if self.fname in ['p','i','v','n','inc','dec','get'] :
                self.GetFuncValueIndex(self.fname, ast_exp.fargs, circuit, phase_dependence)
            elif self.fname == 'set':
                if len(ast_exp.fargs) == 1:
                    param = self.GetNodeOrElement(circuit, ast_exp.fargs[0])
                    if not isinstance(param, Node):
                        raise Exception("Cannot find node " + param + " in " + str(circuit))
                    self.index = param.ValueIndex()
                    self.fargs = []
                else:
                    self.etype == ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for function set() in" + str(circuit))
            elif self.fname == 'psfq':
                if len(ast_exp.fargs) == 3:
                    self.p = Expression(ast_exp.fargs[0], circuit, phase_dependence)
                    self.d = Expression(ast_exp.fargs[1], circuit, phase_dependence)
                    self.s = Expression(ast_exp.fargs[2], circuit, phase_dependence)
                else:
                    self.etype == ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for function psfq() in" + str(circuit))

# ------------------- begin NPSFQ ----------------
            elif self.fname == 'npsfq':
                if len(ast_exp.fargs) > 3:
                    self.np = Expression(ast_exp.fargs[0], circuit, phase_dependence)
                    self.p = Expression(ast_exp.fargs[1], circuit, phase_dependence)
                    self.w = Expression(ast_exp.fargs[2], circuit, phase_dependence)
                    self.s = []
                    for iTime in range(3, len(ast_exp.fargs)):
                        self.s.append(Expression(ast_exp.fargs[iTime], circuit, phase_dependence))
                else:
                    self.etype == ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for function psfqn() in" + str(circuit)) 
# ------------------- end NPSFQ ---------------- 

# ------------------- begin NPRECT ----------------
            elif self.fname == 'nprect':
                if len(ast_exp.fargs) > 3:
                    self.np = Expression(ast_exp.fargs[0], circuit, phase_dependence)
                    self.p = Expression(ast_exp.fargs[1], circuit, phase_dependence)
                    self.w = Expression(ast_exp.fargs[2], circuit, phase_dependence)
                    self.s = []
                    for iTime in range(3, len(ast_exp.fargs)):
                        self.s.append(Expression(ast_exp.fargs[iTime], circuit, phase_dependence))
                else:
                    self.etype == ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for function psfqn() in" + str(circuit))
# ------------------- end NPRECT ---------------- 

            elif self.fname == 'tvec':
                if len(ast_exp.fargs) == 1:
                    tvname_exp = Expression(ast_exp.fargs[0], circuit, phase_dependence)
                    if tvname_exp.etype == ExpressionAST.EXP_TYPE_SCONST:
                        if tvname_exp.sconst_val in psglobals.TestVectorsMap:
                            self.tvec = psglobals.TestVectorsMap[tvname_exp.sconst_val]
                        else:
                            self.etype == ExpressionAST.EXP_TYPE_INVALID
                            raise Exception("Invalid test vector name in function tvec() in " + str(circuit))
                    else:
                        self.etype = ExpressionAST.EXP_TYPE_INVALID
                        raise Exception("Invalid first argument for function tvec() in " + str(circuit))
                else:
                    self.etype == ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for function tvec() in" + str(circuit))
            elif self.fname == 'assign':
                if len(ast_exp.fargs) == 2 and ast_exp.fargs[0].etype == ExpressionAST.EXP_TYPE_IDENT:
                    self.fargs = [Expression(ast_exp.fargs[0], circuit, phase_dependence),
                                  Expression(ast_exp.fargs[1], circuit, phase_dependence)]
                else:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for assignment expression in" + str(circuit))
                if self.fargs[0].etype == ExpressionAST.EXP_TYPE_INVALID or \
                        self.fargs[1].etype == ExpressionAST.EXP_TYPE_INVALID:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid parameters for assignment expression in " + str(circuit))
            elif self.fname == 'exit':
                if len(ast_exp.fargs) == 2:
                    mexp = Expression(ast_exp.fargs[0], circuit, phase_dependence)
                    if mexp.etype == ExpressionAST.EXP_TYPE_SCONST:
                        self.message = mexp.sconst_val
                    else:
                        self.etype = ExpressionAST.EXP_TYPE_INVALID
                        raise Exception("Invalid first argument for function exit() in " + str(circuit))
                    self.exit_condition = Expression(ast_exp.fargs[1], circuit, phase_dependence)
                else:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of arguments for function exit() in " + str(circuit))
                self.fargs = [] 
            elif self.fname == 'freeze' or self.fname == 'unfreeze':
                self.fargs = []
                for arg in ast_exp.fargs:                    
                    try:
                        jelem = self.GetNodeOrElement(circuit, arg)
                        self.fargs.append(jelem.Name())
                    except:
                        raise Exception("Cannot get Junction name from " + str(arg))
            elif self.fname == 'rsj':
                if len(ast_exp.fargs) != 3:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for " + self.fname + " in " + str(circuit))
                self.fargs = []
                for arg in ast_exp.fargs:
                    self.fargs.append(Expression(arg, circuit, phase_dependence))
            elif self.fname == 'rsjn':
                if len(ast_exp.fargs) != 5:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for " + self.fname + " in " + str(circuit))
                self.fargs = []
                for arg in ast_exp.fargs:
                    self.fargs.append(Expression(arg, circuit, phase_dependence))
            elif self.fname == 'rsjph':
                if len(ast_exp.fargs) != 4:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for " + self.fname + " in " + str(circuit))
                self.fargs = []
                for arg in ast_exp.fargs:
                    self.fargs.append(Expression(arg, circuit, phase_dependence))
            elif self.fname == 'tjm':
                if len(ast_exp.fargs) != 6:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for " + self.fname + " in " + str(circuit))
                self.fargs = []
                for arg in ast_exp.fargs:
                    self.fargs.append(Expression(arg, circuit, phase_dependence))        
            elif self.fname == 'llm':
                if len(ast_exp.fargs) != 3:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for " + self.fname + " in " + str(circuit))
                self.l1elem = self.GetNodeOrElement(circuit, ast_exp.fargs[0])
                self.l2elem = self.GetNodeOrElement(circuit, ast_exp.fargs[1])
                #self.fargs = [Expression(ast_exp.fargs[2], circuit, phase_dependence)]
            elif self.fname in ['print','min','max','round']:
                self.fargs = []
                for arg in ast_exp.fargs:
                    self.fargs.append(Expression(arg, circuit, phase_dependence))
            elif self.fname == 'integrate':
                if len(ast_exp.fargs) != 1:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of parameters for " + self.fname + " in " + str(circuit))
                self.last_fvalue = 0.0
                self.last_value = 0.0
                self.fargs = [Expression(ast_exp.fargs[0], circuit, phase_dependence),]
            elif self.fname == 'if':
                if len(ast_exp.fargs) == 3:
                    self.test_exp = Expression(ast_exp.fargs[0], circuit, phase_dependence)
                    if self.test_exp == ExpressionAST.EXP_TYPE_INVALID:
                        self.etype = ExpressionAST.EXP_TYPE_INVALID
                        raise Exception("Invalid first argument for function if() in " + str(circuit))
                    self.true_exp = Expression(ast_exp.fargs[1], circuit, phase_dependence)
                    self.false_exp = Expression(ast_exp.fargs[2], circuit, phase_dependence)
                else:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of arguments for function if() in " + str(circuit))
                self.fargs = [self.test_exp, self.true_exp, self.false_exp]
            elif self.fname == 'avert':
                if len(ast_exp.fargs) > 1 and len(ast_exp.fargs) < 5:
                    self.fargs = [Expression(ast_exp.fargs[0], circuit, phase_dependence)]
                    arg_values = [0.0]
                    for arg in ast_exp.fargs[1:]:
                        exp = Expression(arg, circuit, phase_dependence)
                        arg_values.append(exp.Value())
                    self.aver = AverT(*arg_values)
                else: 
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid number of arguments for function avert() in " + str(circuit))
            elif self.fname == 'pyeval':
                if len(ast_exp.fargs) != 1 and ast_exp.fargs[0].etype != ExpressionAST.EXP_TYPE_SCONST:
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Invalid parameters for " + self.fname + " in " + str(circuit))
                self.fargs = [Expression(ast_exp.fargs[0], circuit, phase_dependence)]
            elif self.fname in dir(math):
                # One of the math module functions
                self.func = getattr(math, self.fname)
                if not callable(self.func):
                    self.etype = ExpressionAST.EXP_TYPE_INVALID
                    raise Exception("Not a math module function " + self.fname + " in " + str(circuit))
                # Do not check number of arguments
                self.fargs = []
                for arg in ast_exp.fargs:
                    self.fargs.append(Expression(arg, circuit, phase_dependence))
            else:
                self.etype = ExpressionAST.EXP_TYPE_INVALID
                raise Exception("Unknown function " + self.fname + " in " + str(circuit))
        elif self.etype == ExpressionAST.EXP_TYPE_SEQUENCE:
            self.explist = []
            eindex = 0
            for exp in ast_exp.explist:
                expr = Expression(exp, circuit, phase_dependence)
                expr.status = False
                expr.eindex = eindex
                eindex += 1
                self.explist.append(expr)
        else:
            self.etype == ExpressionAST.EXP_TYPE_INVALID

    def __repr__(self):
        res = ""
        if self.etype == ExpressionAST.EXP_TYPE_ICONST:
            res += str(self.iconst_val)
        elif self.etype == ExpressionAST.EXP_TYPE_FCONST:
            res += str(self.fconst_val)
        elif self.etype == ExpressionAST.EXP_TYPE_SCONST:
            res += "'" + str(self.sconst_val) + "'"
        elif self.etype == ExpressionAST.EXP_TYPE_IDENT:
            res += str(self.ident)
        elif self.etype == ExpressionAST.EXP_TYPE_UNARY:
            res += str(self.operator) + str(self.operand)
        elif self.etype == ExpressionAST.EXP_TYPE_BINARY:
            res += str(self.operand1) + " " + str(self.operator) + " " + str(self.operand2)
        elif self.etype == ExpressionAST.EXP_TYPE_FCALL:
            res += str(self.fname) + "(" + str(self.fargs) + ")"
        elif self.etype == ExpressionAST.EXP_TYPE_SEQUENCE:
            res += str(self.explist)
        elif self.etype == ExpressionAST.EXP_TYPE_INDEXED_VAL:
            res += self.fname + "(" + self.farg_name + "[" + str(self.index) + "])"
        else:
            pass
        return(res)

    def GetNodeOrElementName(self, expr):
        if expr.etype == ExpressionAST.EXP_TYPE_ICONST:
            # Node number
            return(str(expr.iconst_val))
        elif expr.etype == ExpressionAST.EXP_TYPE_FCONST:
            # Node number
            return(str(int(expr.fconst_val)))
        elif expr.etype == ExpressionAST.EXP_TYPE_SCONST:
            # Node name or element name
            return(expr.sconst_val)
        elif expr.etype == ExpressionAST.EXP_TYPE_IDENT:
            # Node name or element name
            return(expr.ident)
        else:
            return('')

    def GetNodeOrElement(self, circuit, expr):
        if expr.etype == ExpressionAST.EXP_TYPE_ICONST:
            # Node number
            param = str(expr.iconst_val)
        elif expr.etype == ExpressionAST.EXP_TYPE_FCONST:
            # Node number
            param = str(int(expr.fconst_val))
        elif expr.etype == ExpressionAST.EXP_TYPE_SCONST:
            # Node name or element name
            param = expr.sconst_val
        elif expr.etype == ExpressionAST.EXP_TYPE_IDENT:
            # Node name or element name
            param = expr.ident
        elif expr.etype == ExpressionAST.EXP_TYPE_POINTS:
            # First operand is a subcircuit
            cktname = self.GetNodeOrElementName(expr.operand1)
            subcircuits = circuit.Subcircuits()
            if cktname in subcircuits:
                return(self.GetNodeOrElement(subcircuits[cktname], expr.operand2))
            else:
                raise Exception("Cannot find subcircuit "  + str(circuit) + ":" + cktname)
        else:
            raise Exception("Invalid parameter for function in " + str(circuit))

        if param in circuit.Nodes():
            return(circuit.Nodes()[param])
        elif param in circuit.Elements():
            return(circuit.Elements()[param])
        else:
            raise Exception("Cannot find node or element " + str(circuit) + ":" + param)

    def IncDecList(self):
        result = [set(),set()]
        if self.etype == ExpressionAST.EXP_TYPE_UNARY:
            result = self.operand.IncDecList()
        elif self.etype == ExpressionAST.EXP_TYPE_BINARY:
            res1 = self.operand1.IncDecList()
            res2 = self.operand2.IncDecList()
            result[0] = res1[0] | res2[0]
            result[1] = res2[1] | res2[1]
        elif self.etype == ExpressionAST.EXP_TYPE_INDEXED_VAL:
            if self.fname == 'inc':
                result[0].add(self.farg_name)
            elif self.fname == 'dec':
                result[1].add(self.farg_name)
        elif self.etype == ExpressionAST.EXP_TYPE_FCALL:
            if self.fname != 'freeze' and self.fname != 'unfreeze':
                for arg in self.fargs:
                    arg_res = arg.IncDecList()
                    result[0] |= arg_res[0]
                    result[1] |= arg_res[1]
        elif self.etype == ExpressionAST.EXP_TYPE_SEQUENCE:
            for exp in self.explist:
                exp_res = exp.IncDecList()
                result[0] |= exp_res[0]
                result[1] |= exp_res[1]
        return result
            
    def GetFuncValueIndex(self, fname, fargs, circuit, phase_dependence):
        if circuit is not None and phase_dependence:
            if len(fargs) == 1:
                param = self.GetNodeOrElement(circuit, fargs[0])
                self.etype = ExpressionAST.EXP_TYPE_INDEXED_VAL
                self.index = param.ValueIndex()
                self.farg_name = param.Name()
                if isinstance(param, Node):
                    if fname == 'p':
                        self.array = psglobals.NodePhase
                    elif fname == 'i':
                        self.array = psglobals.NodeCurrent
                    elif fname == 'v':
                        self.array = psglobals.NodeVoltage
                    elif fname == 'get':
                        self.array = psglobals.NodeState
                    else:
                        raise Exception("Invalid function" + fname + " in "+ str(circuit))
                else:
                    if fname == 'p':
                        self.array = psglobals.ElementPhase
                    elif fname == 'i':
                        self.array = psglobals.ElementCurrent
                    elif fname == 'v':
                        self.array = psglobals.ElementVoltage
                    elif fname == 'n':
                        self.array = psglobals.ElementN
                    elif fname == 'inc':
                        self.array = psglobals.ElementInc
                    elif fname == 'dec':
                        self.array = psglobals.ElementDec
                    else:
                        raise Exception("Invalid function " + fname + " in "+ str(circuit))
                    
            else:
                raise Exception("Invalid number of parameters for function " + fname)
        else:
            raise Exception("Expression cannot depends on phase")

    def Value(self, msg_lst = None):
        if self.etype == ExpressionAST.EXP_TYPE_FCONST:
            return(self.fconst_val)
        elif self.etype == ExpressionAST.EXP_TYPE_ICONST:
            return(float(self.iconst_val))
        elif self.etype == ExpressionAST.EXP_TYPE_SCONST:
            return(self.sconst_val)
        elif self.etype == ExpressionAST.EXP_TYPE_UNARY:
            if self.operator == '-':
                return(-self.operand.Value())
            elif self.operator == '+':
                return(self.operand.Value())
            elif self.operator == '!':
                if self.operand.Value() == 1.0:
                    return(0.0)
                else:
                    return(1.0)
            else:
                return(0.0)
        elif self.etype == ExpressionAST.EXP_TYPE_INDEXED_VAL:
            return(self.array[self.index])
        elif self.etype == ExpressionAST.EXP_TYPE_BINARY:
            if self.operator == '+':
                return(self.operand1.Value() + self.operand2.Value())
            elif self.operator == '-':
                return(self.operand1.Value() - self.operand2.Value())
            elif self.operator == '*':
                return(self.operand1.Value() * self.operand2.Value())   
            elif self.operator == '/':
                return(self.operand1.Value() / self.operand2.Value())
            elif self.operator == '>':
                if self.operand1.Value() > self.operand2.Value():
                    return(1.0)
                else:
                    return(0.0)
            elif self.operator == '<':
                if self.operand1.Value() < self.operand2.Value():
                    return(1.0)
                else:
                    return(0.0)
            elif self.operator == '>=':
                if self.operand1.Value() >= self.operand2.Value():
                    return(1.0)
                else:
                    return(0.0)
            elif self.operator == '<=':
                if self.operand1.Value() <= self.operand2.Value():
                    return(1.0)
                else:
                    return(0.0)
            elif self.operator == '!=':
                if self.operand1.Value() != self.operand2.Value():
                    return(1.0)
                else:
                    return(0.0)
            elif self.operator == '==':
                if self.operand1.Value() == self.operand2.Value():
                    return(1.0)
                else:
                    return(0.0)
            elif self.operator == '&&':
                if self.operand1.Value() == 1 and self.operand2.Value() == 1:
                    return(1.0)
                else:
                    return(0.0)
            elif self.operator == '||':
                if self.operand1.Value() == 1 or self.operand2.Value() == 1:
                    return(1.0)
                else:
                    return(0.0)
            else:
                return(0.0)
        elif self.etype == ExpressionAST.EXP_TYPE_IDENT:
            return(self.array[self.index])
        elif self.etype == ExpressionAST.EXP_TYPE_FCALL:
            if self.fname == 'psfq':
                dt = psglobals.CurrentTime - self.s.Value()
                if dt < 0:
                    return(0.0)
                else:
                    period= self.p.Value()
                    duration = self.d.Value()
                    return(2.0 * math.pi * (math.floor(dt / period) + smooth_step((dt % period)/duration)))

# -------- begin  NPSFQ --------------------
            if self.fname == 'npsfq':
                self.nperiod = self.np.Value()
                self.period = self.p.Value()
                self.width = self.w.Value() 
                tmpPh = 0.0 
#                print('\ntime ', psglobals.CurrentTime, end = '   ')
                for i in range(len(self.s)):                
                    iTime = self.s[i].Value()
                    dt = psglobals.CurrentTime - iTime
                    tmpPh = tmpPh + 2.0 * math.pi \
                        * stepsWithPeriods(self.nperiod, self.period, self.width, dt)
#                    print(' i, period, iTime, dt, width, tmpPh ',i,self.period, self.iTime,self.dt,self.width, self.tmpPh, end = '  ')                    
                return(tmpPh)
# ---------- end NPSFQ -------------------- 
 
# -------- begin  NPRECT --------------------
            if self.fname == 'nprect':
                self.nperiod = self.np.Value()
                self.period = self.p.Value()
                self.width = self.w.Value() 
                tmpCur = 0.0 
#                print('\ntime ', psglobals.CurrentTime, end = '   ')
                for i in range(len(self.s)):                
                    iTime = self.s[i].Value()
                    dt = psglobals.CurrentTime - abs(iTime)
                    tmpCur = tmpCur + math.copysign(1.0, iTime) \
                        * stepsWithPeriods(self.nperiod, self.period, self.width, dt)
#                    print(' i, period, iTime, dt, width, tmpPh ',i,self.period, self.iTime,self.dt,self.width, self.tmpPh, end = '  ')                    
                return(tmpCur)
# ---------- end NPRECT -------------------- 
                  
            if self.fname == 'tvec':
                return self.tvec.value(psglobals.CurrentTime)
            elif self.fname == 'assign':
                self.fargs[0].array[self.fargs[0].index] = self.fargs[1].Value()
                return(1.0)
            elif self.fname == 'set':
                psglobals.NodeStateNew[self.index] = 1
                return(1.0)
            elif self.fname == 'exit':
                if self.exit_condition.Value():
                    if self.circuit is not None:
                        self.circuit.exit_rule_triggered = True
                        self.circuit.exit_rule_message = self.message
                return(1.0)
            elif self.fname == 'print':
                print_message = ""
                for arg in self.fargs:
                    if arg.etype == ExpressionAST.EXP_TYPE_SCONST:
                        print_message += arg.sconst_val
                    else:
                        print_message += str(arg.Value())
                if msg_lst is not None:
                    msg_lst[0] +=  print_message
                return(1.0)
            elif self.fname == 'freeze':
                if self.circuit is not None:
                    self.circuit.FreezeJunctions(self.fargs)
                return(1.0)
            elif self.fname == 'unfreeze':
                if self.circuit is not None:
                    self.circuit.UnFreezeJunctions(self.fargs)
                return(1.0)
            elif self.fname == 'min':
                return(min(q.Value() for q in self.fargs))
            elif self.fname == 'max':
                return(max(q.Value() for q in self.fargs))
            elif self.fname == 'round':
                return(round(self.fargs[0].Value(),int(self.fargs[1].Value())))
            elif self.fname == 'integrate':
                val = self.fargs[0].Value()
                self.last_value += SP.TimeSteps[0] * (self.last_fvalue + val) / 2.0
                self.last_fvalue = val
                return(self.last_value) 
            elif self.fname == 'if':
                if self.test_exp.Value() == 1:
                    return self.true_exp.Value()
                else:
                    return self.false_exp.Value()
            elif self.fname == 'avert':
                return self.avert.step(psglobals.CurrentTime, self.fargs[0].Value())
            elif self.fname == 'pyeval':
                return eval(self.fargs[0].sconst_val)
            elif self.func is not None:
                args = []
                for arg in self.fargs:
                    args.append(arg.Value())
                # Do not check number of arguments
                return(self.func(*args))
            else:
                return(0.0)
        else:
            return(0.0)
