﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import numpy 
from numpy import matlib
from array import array
from scipy.sparse import csc_matrix
from . import SimulationParameters as SP
from .Expression import smooth_step
from . import psglobals

from .KLUSolver import KLUSolver
klu_solver = KLUSolver()

from .ElementModels_c import BuildStaticMatrix, BuildMatrix, UpdateMatrix, UpdateRHS, UpdateValues, InitElementModels

def NextIndex(i):
    return((i+1) % (SP.MAX_ORDER+1))

def CreateSimulationArrays():
	global klu_solver

	col_list = [set() for i in range(0, psglobals.TotalNodes)]
	for elem in psglobals.ElementList:
		nodes = elem.Nodes()
		virtual_nodes = elem.VirtualNodes()
		if len(nodes) == 2:
			if len(virtual_nodes) == 0:
				i = nodes[0]
				j = nodes[1]
				if i == 0:
					col_list[j-1].add(j-1)
				elif j == 0:
					col_list[i-1].add(i-1)
				else:
					col_list[i-1].add(i-1)
					col_list[j-1].add(i-1)
					col_list[i-1].add(j-1)
					col_list[j-1].add(j-1)
			if len(virtual_nodes) == 1:
				i = nodes[0]
				j = nodes[1]
				q = virtual_nodes[0]
				if i == 0:
					col_list[j-1].add(q-1)
					col_list[q-1].add(j-1)
				elif j == 0:
					col_list[i-1].add(q-1)
					col_list[q-1].add(i-1)
				else:
					col_list[i-1].add(q-1)
					col_list[j-1].add(q-1)
					col_list[q-1].add(i-1)
					col_list[q-1].add(j-1)
					
				if elem.Name()[0] == 'l':
					col_list[q-1].add(q-1)
		elif len(nodes) == 0 and len(virtual_nodes) == 2:
			# Mutual inductance
			q = virtual_nodes[0]
			v = virtual_nodes[1]
			col_list[q-1].add(v-1)
			col_list[v-1].add(q-1)

	for i in range(0, psglobals.TotalNodes):
		clist = list(col_list[i])
		clist.sort()
		SP.NNZ += len(clist)
		col_list[i] = clist
		
	#print("Matrix {}x{}, NNZ {} {:.2f}%".format(psglobals.TotalNodes, psglobals.TotalNodes, SP.NNZ, 
	#								  (100.0*nnz)/(psglobals.TotalNodes*psglobals.TotalNodes)))
	SP.Ap = array('i', [0]*(psglobals.TotalNodes+1))
	SP.Ai = array('i')
	for i in range(0, psglobals.TotalNodes):
		clist = col_list[i]
		SP.Ap[i+1] = SP.Ap[i] + len(clist)
		SP.Ai.fromlist(clist)

	ind1 = SP.Ap[0]
	for i in range(1, psglobals.TotalNodes+1):
		ind2 = SP.Ap[i]
		col = i - 1
		for j in range(ind1, ind2):
			row = SP.Ai[j]
			SP.SparseIndexMap[(row,col)] = j
		ind1 = ind2
	klu_solver.analyze(psglobals.TotalNodes, SP.Ap, SP.Ai)

	SP.X = numpy.zeros((SP.MAX_ORDER+1, psglobals.TotalNodes), dtype = numpy.float64)
	SP.Xpredicted = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
	SP.DX = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
	SP.DXprev = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
	SP.DDX = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
	Adata = numpy.zeros((SP.NNZ), dtype = numpy.float64)
	SP.A = csc_matrix((Adata, SP.Ai, SP.Ap), shape=(psglobals.TotalNodes, psglobals.TotalNodes))
	A0data = numpy.zeros((SP.NNZ), dtype = numpy.float64)
	SP.A0 = csc_matrix((A0data, SP.Ai, SP.Ap), shape=(psglobals.TotalNodes, psglobals.TotalNodes))
	A1data = numpy.zeros((SP.NNZ), dtype = numpy.float64)
	SP.A1 = csc_matrix((A1data, SP.Ai, SP.Ap), shape=(psglobals.TotalNodes, psglobals.TotalNodes))
	A2data = numpy.zeros((SP.NNZ), dtype = numpy.float64)
	SP.A2 = csc_matrix((A2data, SP.Ai, SP.Ap), shape=(psglobals.TotalNodes, psglobals.TotalNodes))
	SP.B = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
	SP.B1 = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)

def InitGlobalArrays():
	psglobals.NodePhase.fill(0.0)
	psglobals.NodeVoltage.fill(0.0)
	psglobals.NodeDVoltage.fill(0.0)
	psglobals.NodeState.fill(0)
	psglobals.NodeStateNew.fill(0)
	
	psglobals.ElementPhase.fill(0.0)
	psglobals.ElementVoltage.fill(0.0)
	psglobals.ElementCurrent.fill(0.0)
	psglobals.ElementN.fill(0)
	psglobals.ElementInc.fill(0)
	psglobals.ElementDec.fill(0)
	
	#psglobals.ParameterValues.fill(0.0)
	#psglobals.ExternalValues.fill(0.0)
	psglobals.InternalValues.fill(0.0)
	psglobals.ValueValues.fill(0.0)
	
#sim_trace = open("sim_trace.txt", "w")

def InitSimulation():
	global X, Xpredicted, DX, DXprev, DDX, A, A0, A1, A2, B, B1, TimeSteps
	global StepError, InitialSmoothFactor
	global InterpCoeff, InterpCoeffDt, InterpCoeffDDt, InterpOrder
	global Xind, DXInd

	#global sim_trace
	#sim_trace = open("sim_trace.txt", "w")

	SP.X.fill(0.0)
	SP.Xpredicted.fill(0.0)
	SP.DX.fill(0.0)
	SP.DXprev.fill(0.0)
	SP.DDX.fill(0.0)
	SP.A.data.fill(0.0)
	SP.A0.data.fill(0.0)
	SP.A1.data.fill(0.0)
	SP.A2.data.fill(0.0)
	SP.B.fill(0.0)
	SP.B1.fill(0.0)
	
	for i in range(0, SP.MAX_ORDER+1):
		SP.TimeSteps[i] = (i+1)*1.0

	SP.LastTimeStep = 0.0
	SP.StepError = 0.0
	SP.OrderSteps = 0
	SP.InitialSmoothFactor = 0.0

	SP.InterpOrder = SP.MIN_ORDER
	SP.InterpCoeff = [0.0]*(SP.MAX_ORDER+1)
	SP.InterpCoeffDt = [0.0]*(SP.MAX_ORDER+1)
	SP.InterpCoeffDDt = [0.0]*(SP.MAX_ORDER+1)

	SP.Xind = 0
	SP.DXInd = 0

	psglobals.CurrentTime = 0.0

	InitGlobalArrays()
	InitElementModels()
	BuildStaticMatrix(SP.A0.data)


def CalculateInterpCoeff():
	t = [0] * len(SP.TimeSteps)
	t[0] = SP.TimeSteps[-1]
	for i in range(1, len(t)):
		t[i] = t[0] - SP.TimeSteps[i-1]

	s1 = (t[1] - t[2])*(t[1] - t[3])
	s2 = (t[2] - t[1])*(t[2] - t[3])
	s3 = (t[3] - t[1])*(t[3] - t[2])

	SP.InterpCoeff[0] = (t[0] - t[2])*(t[0] - t[3])/s1
	SP.InterpCoeff[1] = (t[0] - t[1])*(t[0] - t[3])/s2
	SP.InterpCoeff[2] = (t[0] - t[1])*(t[0] - t[2])/s3

	d1 = (t[0] - t[1])*(t[0] - t[2])
	d2 = (t[1] - t[0])*(t[1] - t[2])
	d3 = (t[2] - t[0])*(t[2] - t[1])
	
	SP.InterpCoeffDt[0] = (2.0*t[0] - t[1] - t[2])/d1
	SP.InterpCoeffDt[1] = (2.0*t[0] - t[0] - t[2])/d2
	SP.InterpCoeffDt[2] = (2.0*t[0] - t[0] - t[1])/d3

	SP.InterpCoeffDDt[0] = 2.0/d1
	SP.InterpCoeffDDt[1] = 2.0/d2
	SP.InterpCoeffDDt[2] = 2.0/d3


def SelectTimeStep():
	if SP.TimeStepReverted:
		# Time step was just reverted - keep the same time step
		TStep = SP.TimeSteps[0]
		SP.TimeStepReverted = False
	else:
		if abs(SP.StepError) < SP.TimeStepEpsilon:
			ErrTimeStep = SP.DTMax
		else:
			ErrTimeStep = SP.TimeSteps[0] * pow(SP.DXMax / SP.StepError, 1.0 / SP.InterpOrder)

		if abs(SP.StepPhaseChange) < SP.TimeStepEpsilon:
			PhaseTimeStep = SP.DTMax
		else:
			PhaseTimeStep = 0.7*SP.TimeSteps[0] * SP.MaxPhaseChange/SP.StepPhaseChange

		NewTimeStep = min(ErrTimeStep, PhaseTimeStep)

		if psglobals.CurrentTime < SP.INITIAL_RAMP:
			# max time step will be 1.0 until tcurr < initial ramp
			TStep = min(1.0, max(SP.DTMin,  NewTimeStep))
		else:
			TStep = min(SP.DTMax, max(SP.DTMin,  NewTimeStep))

	for i in range(SP.MAX_ORDER, 0, -1):
		SP.TimeSteps[i] = SP.TimeSteps[i - 1] + TStep
	SP.TimeSteps[0] = TStep

def UpdateTimeSteps(newTimeStep):
    dt = SP.TimeSteps[0] - newTimeStep
    for i in range(0, SP.MAX_ORDER+1):
        SP.TimeSteps[i] -= dt
		
#@profile   
def UpdateGlobalParameters():
	for param in psglobals.Parameters.values():
		param.Update()
		
#@profile   
def UpdateGlobalInternals():
	# Update tcurr
	psglobals.InternalValues[0] = psglobals.CurrentTime
	# Update the rest of global internals
	for intvar in psglobals.InternalsList:
		intvar.Update()

#@profile
def UpdateCircuitParameters():
	UpdateGlobalParameters()
	UpdateGlobalInternals()
	for cirdef in psglobals.CircuitScriptMap.values():
		for param in cirdef.parameters.values():
			param.Update()
	psglobals.RootCircuit.UpdateElementParameters()

#@profile
def PredictVariables():
	SP.X[SP.Xind] = SP.InterpCoeff[0] * SP.X[SP.Xind - 1]
	for j in range(1, SP.InterpOrder):
		ind = SP.Xind - j - 1
		SP.X[SP.Xind] += SP.InterpCoeff[j] * SP.X[ind]

	numpy.copyto(SP.Xpredicted, SP.X[SP.Xind])

	SP.DX = SP.InterpCoeffDt[0] * SP.X[SP.Xind]
	SP.DDX = SP.InterpCoeffDDt[0] * SP.X[SP.Xind]
	for j in range(1, SP.InterpOrder):
		ind = SP.Xind - j
		SP.DX += SP.InterpCoeffDt[j] * SP.X[ind]
		SP.DDX += SP.InterpCoeffDDt[j] * SP.X[ind]

#@profile
def BuildSystem():
	SP.A1.data.fill(0.0)
	SP.A2.data.fill(0.0)

	SP.B.fill(0.0)

	BuildMatrix(SP.A1.data, SP.A2.data, SP.B)
	SP.A.data = SP.A0.data + SP.A1.data + SP.A2.data

#@profile
def GetNewtonIter():
	# Use ufunc !!!!
	SP.X[SP.Xind] += SP.B1
	SP.DX = SP.X[SP.Xind] * SP.InterpCoeffDt[0]
	SP.DDX = SP.X[SP.Xind] * SP.InterpCoeffDDt[0]
	for j in range(1, SP.InterpOrder):
		ind = SP.Xind - j
		SP.DX += SP.InterpCoeffDt[j] * SP.X[ind]
		SP.DDX += SP.InterpCoeffDDt[j] * SP.X[ind]

#@profile
def GetStepChanges():
	result = 0.0

	for i in range(0, psglobals.TotalNodes):
		if abs(SP.B1[i]) > SP.TimeStepEpsilon:
			xinit = SP.X[SP.Xind][i] + SP.B1[i]
			if abs(xinit) < 1.0:
				xinit = 1.0
			result = max(result, abs(SP.B1[i]/xinit))
	return result

#@profile
def GetChanges():
	newx = SP.X[SP.Xind] + SP.B1
	#newdx = newx * SP.InterpCoeffDt[0]
	#for j in range(1, SP.InterpOrder):
	#	ind = SP.Xind-j
	#	newdx += SP.InterpCoeffDt[j] * SP.X[ind]
	dx = numpy.abs(newx - SP.X[SP.Xind-1])
	#ddx = numpy.abs(newdx - SP.DXprev)
	# For now - only phase difference
	return((numpy.amax(dx), 0.0))

#@profile
def UpdateSystem():
	SP.A2.data.fill(0.0)
	SP.B.fill(0.0)

	UpdateMatrix(SP.A2.data, SP.B)
	SP.A.data = SP.A0.data + SP.A1.data + SP.A2.data

#@profile
def GetAccuracy():
	SP.B.fill(0.0)
	UpdateRHS(SP.B)

	return(numpy.amax(numpy.abs(SP.B)))

#@profile	
def AcceptTimeStep():
	numpy.copyto(SP.DXprev, SP.DX)

	numpy.copyto(psglobals.NodePhase, SP.X[SP.Xind])
	numpy.copyto(psglobals.NodeVoltage, SP.DX)
	numpy.copyto(psglobals.NodeDVoltage, SP.DDX)

	UpdateValues()
	psglobals.RootCircuit.UpdateValues()

#@profile
def TimeStep():
	# Update nodes states
	numpy.copyto(psglobals.NodeState, psglobals.NodeStateNew)
	psglobals.NodeStateNew.fill(0)

	SP.Xind = NextIndex(SP.Xind)
	SelectTimeStep()
	#print("{} tsinit {}".format(psglobals.CurrentTime, SP.TimeSteps[0]), file=sim_trace)
	done = False
	
	while(not done):
		global klu_solver

		CalculateInterpCoeff()
		psglobals.CurrentTime += SP.TimeSteps[0]
		SP.InitialSmoothFactor = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)
		SP.LastTimeStep = SP.TimeSteps[0]
		UpdateCircuitParameters()
		PredictVariables()
		BuildSystem()
		NewtonIter = 0
		while(not done):
			NewtonIter += 1
			# SolveSystem A*X=B	
			numpy.copyto(SP.B1, SP.B)
			klu_solver.solveSparseMatrix(SP.A.data, SP.B1)

			#print("T ", psglobals.CurrentTime, " A ", SP.A[0], " B ", SP.B1[0])

			SP.StepError = numpy.amax(numpy.abs(SP.B1))
			#print("{} NewtonIter {} err {}".format(psglobals.CurrentTime, NewtonIter, SP.StepError), file=sim_trace)
			if SP.StepError > SP.Eps1:
				SP.TimeStepReverted = True
				if SP.TimeSteps[0] / 2.0 < SP.DTMin:
                    # Step error is high, but we cannot decrease time step anymore
					GetChanges()
					GetNewtonIter()
					AcceptTimeStep()
					return(-2)
				else:
                    # Revert back time step
					#print("{} NewtonIter {} err {} - time step reverted".format(psglobals.CurrentTime, NewtonIter, SP.StepError), file=sim_trace)
					psglobals.CurrentTime -= SP.TimeSteps[0]
					SP.InitialSmoothFactor = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)
                    # Decrease time step 
					NewTimeStep = max(SP.DTMin, 0.7*SP.TimeSteps[0] * pow(SP.Eps2/SP.StepError, 0.33))
					#print("{} NewtonIter {} err {} - new tstep {}".format(psglobals.CurrentTime, NewtonIter, SP.StepError, NewTimeStep), file=sim_trace)
					UpdateTimeSteps(NewTimeStep)
					break

			(SP.StepPhaseChange, maxdx) = GetChanges()
			#print("{} NewtonIter {} err {} - phase change {}".format(psglobals.CurrentTime, NewtonIter, SP.StepError, SP.StepPhaseChange), file=sim_trace)
			if SP.StepPhaseChange > SP.MaxPhaseChange:
				SP.TimeStepReverted = True
				if SP.TimeSteps[0] / 2.0 < SP.DTMin:
					AcceptTimeStep()
					return(-2)
				else:
					psglobals.CurrentTime -= SP.TimeSteps[0]
					SP.InitialSmoothFactor = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)
					NewTimeStep = max(SP.DTMin, 0.9*SP.TimeSteps[0] * SP.MaxPhaseChange/SP.StepPhaseChange)
					#print("{} NewtonIter {} err {} - phase change {}, new tstep {}".format(psglobals.CurrentTime, NewtonIter, SP.StepError, SP.StepPhaseChange, NewTimeStep), file=sim_trace)
					UpdateTimeSteps(NewTimeStep)
				break

			GetNewtonIter()

			if SP.StepError > SP.Eps2:
				StepAccuracy = GetAccuracy()
				#print("{} NewtonIter {} err {} RHS {}".format(psglobals.CurrentTime, NewtonIter, SP.StepError, StepAccuracy), file=sim_trace)

				if StepAccuracy > SP.RHSEps:
					# Need another Newton step
					if NewtonIter > SP.MaxNewtonIter:
						NewTimeStep = SP.TimeSteps[0] * pow(SP.Eps2 / SP.StepError, 1.0 / (SP.InterpOrder + 1));
						#print("{} NewtonIter {} err {} - max Newton Iter, new tstep {}".format(psglobals.CurrentTime, NewtonIter, SP.StepError, NewTimeStep), file=sim_trace)
						SP.TimeStepReverted = True
						if NewTimeStep < SP.DTMin:
							# Step error is high, but we cannot decrease time step
							AcceptTimeStep()
							return(-2)
						else:
							# Revert back time step
							psglobals.CurrentTime -= SP.TimeSteps[0]
							SP.InitialSmoothFactor = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)
							# Decrease time step 
							UpdateTimeSteps(NewTimeStep)
							break
					
					UpdateSystem()
					continue
			
			done = True

	#print("{} NewtonIter {} err {} - time step accepted".format(psglobals.CurrentTime, NewtonIter, SP.StepError), file=sim_trace)
	AcceptTimeStep()
	SP.OrderSteps -= 1

	FirstTimeStep = False
	return(0)
	