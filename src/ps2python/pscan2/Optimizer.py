# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)
# ak 20190910            

import math, logging, time, random
from . import psglobals
from .Simulation import TimeStep, InitSimulation
from .Element import Element
from .Node import Node
from .ParameterVar import ParameterVar
from .ExternalVar import ExternalVar
from .util import margin, check_margins, find, simulate, print_header


class cw_Opt_Param:
    """
    A parameter to optimize. Includes desired margins, current margins and current value
    """
    DefaultCurVal = 1.0
    DefaultFreePlay = 0.03
    Default_Margins = 0.3
    DefaultEps = 0.002
    
    def __init__(self, param, **kwargs):
        """
        <param> is a list of a parameter's name or an object and its required margins.
        """
        self.logger = logging.getLogger('pscan2')
        
        if 'messages_level' in kwargs:
            self.verbose = (kwargs['messages_level'] > 0)
        else:
            self.verbose = True

        if 'maxtime' in kwargs:
            self.max_time = kwargs['maxtime']
        else:
            self.max_time = find('tseq').Value()
            
        if 'eps' in kwargs:
            self.eps = kwargs['eps']
        else:
            self.eps = cw_Opt_Param.DefaultEps

        if 'gap' in kwargs:
            self.gap = kwargs['gap']
        else:
            self.gap = cw_Opt_Param.DefaultFreePlay
            
        if not isinstance(param, list):
            param=[param]
        if len(param)==1:
            self.lmarg = cw_Opt_Param.Default_Margins
            self.rmarg = cw_Opt_Param.Default_Margins
        elif len(param)==2:
            self.lmarg = param[1]
            self.rmarg = param[1]
        elif len(param)==3:
            self.lmarg = param[1]
            self.rmarg = param[2]
        else:
            raise Exception("ERROR: Unknown record in optimized parameter list:\n" + str(param))

        if isinstance(param[0], str):
            self.param = find(param[0])
        elif isinstance(param[0], ParameterVar) or isinstance(param[0], ExternalVar):
            self.param = param[0]
        else:
            raise Exception("ERROR: Unknown type of optimized parameter " + str(param[0]))

        self.FName = self.param.FullName()
        self.Name = self.param.Name()
        self.init_val=self.param.Value()
        self.maxmarg = max(self.lmarg, self.rmarg) + self.gap
        
#        self.current_margins()

        
    def current_margins(self):
        (self.lcurmarg, self.rcurmarg) = margin(self.param, self.max_time, eps=0.5*self.eps, max_margin=self.maxmarg)
        if self.verbose:
            self.logger.info("Margins for {}: -{:.2f}%/+{:.2f}%".format(self.FName, self.lcurmarg*100, self.rcurmarg*100))
        return self.lcurmarg != 0.0 or self.rcurmarg != 0.0
        
    def is_done(self):
        return self.lcurmarg >= self.lmarg and self.rcurmarg >= self.rmarg
    
    def check_margins(self):
        lval=self.init_val * (1.0 - min(max(0, self.lcurmarg - self.gap), self.lmarg))  
        rval=self.init_val * (1.0 + min(max(0, self.rcurmarg - self.gap), self.rmarg))  
        self.SetVal(lval)
        if not simulate(self.max_time, False):
            self.InitVal()
            return False
        self.SetVal(rval)
        if simulate(self.max_time, False):
            self.InitVal()
            return True
        else:
            self.InitVal()
            return False
    
    def SetVal(self, val):
        self.param.SetValue(val)
    
    def InitVal(self):
        self.param.SetValue(self.init_val)
    
    def SetInitVal(self, val):
        self.init_val=val

    def is_better(self):
        if self.lcurmarg < self.lmarg + self.gap :
            dval = self.lcurmarg + self.eps
            lval = self.init_val * (1.0 - dval)
            rval = self.init_val * (1.0 + min(self.rcurmarg, self.rmarg+self.gap, dval))
            self.SetVal(lval)
            if simulate(self.max_time, False):
                self.SetVal(rval)
                if simulate(self.max_time, False):
                    self.InitVal()
                    return True
        if self.rcurmarg < self.rmarg + self.gap :
            dval = self.rcurmarg + self.eps 
            rval = self.init_val * (1.0 + dval)
            lval = self.init_val * (1.0 - min(self.lcurmarg, self.lmarg+self.gap, dval))
            self.SetVal(rval)
            if simulate(self.max_time, False):
                self.SetVal(lval)
                if simulate(self.max_time, False):
                    self.InitVal()
                    return True
        self.InitVal()
        return False


        
class cw_Var_Param:
    """
    A parameter to vary. Includes current value and variation limits
    """
    DefaultLimits = {'j':(1.0, 3.0), 'v':(0.5, 2.0), 'other':(0.1, 10.0)}

    def __init__(self, param):
        """
        <param> is list consisting of a parameter's name or an object and its variation limits.
        """
        self.logger = logging.getLogger('pscan2')
        self.verbose = True
        
        if not isinstance(param, list):
            param=[param]
        if isinstance(param[0], str):
            self.param = find(param[0])
        elif isinstance(param[0], ParameterVar) or isinstance(param[0], ExternalVar):
            self.param = param[0]
        else:
            raise Exception("ERROR: Unknown type of change parameter " + str(param[0]))

        self.FName = self.param.FullName()
        self.Name = self.param.Name()
    
        if len(param)==3:
            self.minval = param[1]
            self.maxval = param[2]
        elif len(param)==1:
            ptype = self.Name[0]
            if not ptype in cw_Var_Param.DefaultLimits:
                ptype = 'other'
            (self.minval, self.maxval) = cw_Var_Param.DefaultLimits[ptype]
        else:
            raise Exception("ERROR: Unknown record in change parameter list:\n" + str(param))
            
        self.Name = self.param.FullName()
        self.init_val = self.prev_val = self.param.Value()
        if self.init_val > self.maxval or self.init_val < self.minval:
            self.logger.info("WARNING: Current value of parameter {} ({:.2f}) is not within its limits [{:.2f} - {:.2f}]".format(self.Name, self.init_val, self.minval, self.maxval))

    def SetVal(self, val):
        if val > self.maxval or val < self.minval:
            if self.verbose:
                self.logger.info("Denied parameter {} value set to {:.3f}".format(self.Name, val))
            return False
        self.param.SetValue(val)
        return True
    
    def RestoreVal(self):
        self.param.SetValue(self.prev_val)
        
    def AcceptVal(self):
        self.prev_val = self.param.Value()
        
    def GetVal(self):
        return self.param.Value()

        
    
class Optimizer:
    """
    Optimizes margins of circuit parameter
    --------------
    <opt_list> is a list of parameters to optimize. Each list item is a 
    list of a parameter name or a parameter object and required margins.

    <var_list> is a list of parameters allowed to change. Each list item 
    can be a parameter name in the format as in function find() or parameter object.

    <max_time> is a maximal simulation time.

    For example:
    optimized_list = [['xi',0.3], ['xj',0.3], ['mi.xj1', 0.3]]
    var_list = param_re('not1', 'j*')+param_re('not1', 'i*')+param_re('not1', 'l*')
    opt = cw_Optimizer(optimized_list, var_list, 400)
    opt.optimize()
    """

    ParameterSteps = [0.5, 0.3, 0.1, 0.05, 0.01]
    DefaultMinValue = 0.05
    ReportFileName = "opt_results.txt"

    def __init__(self, opt_list, change_list, max_time, callback = None):
        """
        <opt_list> is a list of parameters to optimize. Each list item is a 
        list of parameter name or parameter object and required margin.

        <change_list> is a list of parameters allowed to change. Each list item can 
        be a parameter name in the format as in function find() or parameter object.

        <max_time> is a maximal simulation time.
        """
        self.logger = logging.getLogger('pscan2')
        self.fname = Optimizer.ReportFileName
        self.parameter_steps = Optimizer.ParameterSteps
        self.opt_list = []
        self.var_list = []
        self.check_list = []
        self.callback = callback
        for rec in change_list:
            a=cw_Var_Param(rec)
            if a.init_val >= Optimizer.DefaultMinValue:
                self.var_list.append(a)
            else:
                self.logger.info("WARNING: Parameter {} has too small initial value. Ignored!".format(a.FName))
        self.max_time = max_time
        for rec in opt_list:
            a=cw_Opt_Param(rec, maxtime=max_time)
            if a.init_val >= Optimizer.DefaultMinValue:
                self.opt_list.append(a)
            else:
                self.logger.info("ERROR: Parameter {} has too small initial value. Ignored!".format(a.FName))
        
        self.multipar = len(self.opt_list) > 1
        if not self.multipar:
            self.opt_list[0].gap = self.gap = 0
        
        if len(opt_list)==0:
            self.logger.info("ERROR: There is no parameters suitable for optimization")
            return
        self.check_list = list(range(len(self.opt_list)))
        
    def _output_state(self):
        fp=open(self.fname,"a")
        fp.write(print_header())
        self.logger.info("--------------------------------\nParameters:")
        fp.write("# Parameters changed:\n")
        for p in self.var_list:
            if p.init_val != p.GetVal():
                s="{}\t{:.2f} -> {:.2f}".format(p.FName, p.init_val, p.GetVal())
                self.logger.info(s)
                fp.write(s+"\n")
        self.logger.info("--------------------------------\nMargins:")
        fp.write("# Margins:\n")
        for p in self.opt_list:
            s="{}\t-{:.1f}% / +{:.1f}%\t(required -{:.1f}% / +{:.1f}%)".format(p.FName, p.lcurmarg*100, p.rcurmarg*100, p.lmarg*100, p.rmarg*100)
            self.logger.info(s)
            fp.write(s+"\n")
        self.logger.info("--------------------------------")
        fp.close()

        
    def _calculate_margins(self):
        self.logger.info("\n--------------")
        for p in self.opt_list:
            if not p.current_margins():
                self.logger.info("ERROR: Non-operating initial point!")
                return False
        self.logger.info("--------------\n")
        return True

        
    def _check_other_margins(self, except_for=None):
        if not self.multipar:
            return False
        for n in self.check_list:
            p = self.opt_list[n]
            if p==except_for:
                continue
            if not p.check_margins():
                self.check_list.remove(n)
                self.check_list.insert(0, n)
                return(p.FName)
        return False

        
    def optimize(self):
        self.logger.info(print_header() + "Parameters to change: ")
        for p in self.var_list:
            self.logger.info("{} = {:.2f} range [{:.2f},{:.2f}]".format(p.FName, p.GetVal(), p.minval, p.maxval))
        self.logger.info("\nCalculate initial margins:")
        if not self._calculate_margins():
            return
        self.opstate = True
        no_break = True
        while self.opstate and no_break:
            self.opstate = False
            for p in self.opt_list:
                if no_break == False:
                    break
                if p.is_done():
                    continue
                self.logger.info("\nOptimizing parameter {}".format(p.FName))
                self.logger.info("Initial margins -{:.2f}%/+{:.2f}% (required -{:.2f}%/+{:.2f}%)".format(p.lcurmarg*100, p.rcurmarg*100, p.lmarg*100, p.rmarg*100))
                for pstep in self.parameter_steps:
                    if callable(self.callback) and not self.callback():  # check for interruption
                        self.logger.info("Break optimization")
                        no_break = False
                        break
                    self.logger.info("Change step to {:.2f}".format(pstep))
                    if self._optimize_parameter(p, pstep):  # Run single-parameter optimization with step pstep
                        if p.is_done():
                            self.logger.info("Optimization of {} succeeded".format(p.FName))
                            break
                    if self._optimize_parameter(p, self.random_step()):  # Run single-parameter optimization with random step
                        if p.is_done():
                            self.logger.info("Optimization of {} succeeded".format(p.FName))
                            break
            if self.opstate:
                if not self._calculate_margins():
                    self.logger.info("ERROR: Something is wrong with HDL description! Quit optimization.")
                    return
        self._output_state()
        if no_break:
            self.logger.info("\nOptimization is finished.")
        else:
            self.logger.info("\nOptimization has been interrupted.")
        self.logger.info("====================================\n{}\n====================================\n".format(time.ctime()))

        
    def _optimize_parameter(self, op, pstep):
        init_value = op.init_val
        flag = True
        flag1 = False
        while flag:
            flag = False
            for vp in self.var_list:
                if vp.GetVal() >= vp.maxval:
                    self.logger.info("Parameter {} ({:.2f}) is at its upper limit ".format(vp.FName, vp.GetVal()))
                else:
                    newval = min(vp.GetVal() * (1.0 + pstep), vp.maxval)  # Step to the right
                    self.logger.info("{} {:.3f} -> {:.3f}".format(vp.Name, vp.GetVal(), newval ))
                    vp.SetVal(newval)
                    if op.is_better():
                        self.var_list.remove(vp)    # move affecting parameter to the top
                        self.var_list.insert(0, vp)
                        self.logger.info("Changing parameter {}: {:.2f}  ->  {:.2f} improved margins".format(vp.FName, vp.prev_val, newval))
                        qname = self._check_other_margins(except_for=op)
                        if not qname:
                            self.logger.info("Accepted")
                            vp.AcceptVal()             # accept change
                            op.current_margins()    # recalculate margins
                            flag = flag1 = self.opstate = True  # Go for one more time
                            if op.is_done():                # quit if done
                                return True
                        else:
                            self.logger.info("Rejected by {}".format(qname))
                    vp.RestoreVal()        # Restore previous value (or the new value, if it has been accepted)
                            
        if flag1:   # return on success or try the other direction on failure
            return True
            
        flag = True
        while flag:
            flag = False
            for vp in self.var_list:
                if vp.GetVal() <= vp.minval:
                    self.logger.info("Parameter {} ({:.2f}) is at its lower limit ".format(vp.FName, vp.GetVal()))
                else:
                    newval = max(vp.GetVal() * (1.0 - pstep), vp.minval)    # Step to the left
                    vp.SetVal(newval)
                    if op.is_better():
                        self.var_list.remove(vp)    #move parameter to the top
                        self.var_list.insert(0, vp)
                        self.logger.info("Changing parameter {}: {:.2f}  ->  {:.2f} improved margins".format(vp.FName, vp.prev_val, newval))
                        qname=self._check_other_margins(except_for=op)
                        if not qname:
                            self.logger.info("Accepted")
                            vp.AcceptVal()             #accept change
                            op.current_margins()
                            flag = self.opstate = True   # One more time
                            if op.is_done():                # quit if done
                                return True
                        else:
                            self.logger.info("Rejected by {}".format(qname))
                    vp.RestoreVal()        # Restore previous value (or new value, if it has been accepted)
                    
        return self.opstate



    def random_step(self):
        x0=min(self.parameter_steps)
        x1=max(self.parameter_steps)
        return x0 + (x1 - x0) * random.random()
