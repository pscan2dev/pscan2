# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from .ReadSpice import CircuitNetlistDef, ReadSpice
from .expr_parser import expr_parser
from .pscan_parser import parser
from .CircuitAST import CircuitAST, VardeclAST
from .Circuit import Circuit
from .ExpressionAST import ExpressionAST
from .ParameterVar import ParameterVar
from .ExternalVar import ExternalVar
from .InternalVar import InternalVar
from .ValueVar import ValueVar
from .Element import *
from .Rule import *
from .Node import *
from .Simulation import TimeStep, CreateSimulationArrays
from . import SimulationParameters as SP
from . import pscan_lex
from . import psglobals

import os, sys, numpy, traceback

parameters_index = 0;
externals_index = 0;
internals_index = 0;
values_index = 0;

def CreateGlobalParameters():
    global parameters_index

    for paramdef in psglobals.ParametersDef:
        param = ParameterVar("", paramdef, parameters_index)
        psglobals.Parameters[param.Name()] = param
        parameters_index += 1

def CreateGlobalInternals():
    global internals_index

    # Special global internal tcurr with index 0
    tcurr_vardecl = VardeclAST("tcurr", ExpressionAST(ExpressionAST.EXP_TYPE_FCONST, 0.0))
    internal = InternalVar("", tcurr_vardecl, internals_index, None, True)
    psglobals.InternalsList.append(internal)
    psglobals.Internals[internal.Name()] = internal
    internals_index += 1
    
    for intdef in psglobals.InternalsDef:
        internal = InternalVar("", intdef, internals_index, None)
        psglobals.InternalsList.append(internal)
        psglobals.Internals[internal.Name()] = internal
        internals_index += 1

def CreateCircuitParameters():
    global parameters_index
    for cirdef in psglobals.CircuitScriptMap.values():
        for paramdef in cirdef.parameter_list:
            param = ParameterVar(cirdef.Name(), paramdef, parameters_index)
            if param.Name() in cirdef.parfile_parameters:
                param.SetValue(cirdef.parfile_parameters[param.Name()])
            cirdef.parameters[param.Name()] = param
            parameters_index += 1

def CreateCircuitHierarhy(cirdef, cirpath, name, instnodes):
    circuit_ast = psglobals.CircuitScriptMap[cirdef.Name()]
    circuit_ast.SetUsedFlag()
    circuit = Circuit(circuit_ast, cirpath, name, instnodes)
    
    psglobals.TotalExternals += len(circuit_ast.external_list)
    psglobals.TotalInternals += len(circuit_ast.internal_list)
    psglobals.TotalValues += len(circuit_ast.value_list)
    cirnodemap = {0:0, '0':0}
    cirdefnodes = cirdef.Nodes()
    for i in range(len(cirdefnodes)):
        cirnodemap[cirdefnodes[i]] = instnodes[i]
        circuit.AppendNode(Node(cirdefnodes[i], circuit, instnodes[i]))
    #currpath = cirpath + (name,)
    for (ename, nodes, model) in cirdef.GetElements():
        if ename[0] != 'm':
            realnodes = []
            for node in nodes:
                if node in cirnodemap:
                    realnodes.append(cirnodemap[node])
                else:
                    psglobals.TotalNodes += 1
                    realnodes.append(psglobals.TotalNodes)
                    cirnodemap[node] = psglobals.TotalNodes
                    circuit.AppendNode(Node(node, circuit, psglobals.TotalNodes))
                    epath = cirpath+(node,)
                    psglobals.NodeMap[epath] = psglobals.TotalNodes
            element = CreateElement(ename, circuit, psglobals.TotalElements, 
                                    psglobals.TotalElementValues, realnodes, [], model)
            psglobals.TotalElements += 1
            psglobals.TotalElementValues += 1
            psglobals.ElementList.append(element)
            if ename[0] == 'p' or ename[0] == 'u':
                psglobals.TotalSourceElements += 1
                psglobals.SourceElementList.append(element)
            else:
                psglobals.TotalNonSourceElements += 1
                psglobals.NonSourceElementList.append(element)
            circuit.AppendElement(element)
    
    # Second run for mutual inductances and other impedance-model elements
    for (ename, nodes, model) in cirdef.GetElements():
        if ename[0] == 'm':
            element = CreateElement(ename, circuit, psglobals.TotalElements, 
                                    psglobals.TotalElementValues, [], [], model)
            psglobals.TotalElements += 1
            psglobals.TotalElementValues += 1
            psglobals.ElementList.append(element)
            psglobals.TotalNonSourceElements += 1
            psglobals.NonSourceElementList.append(element)
            circuit.AppendElement(element)


    for (sname, nodes, model) in cirdef.GetSubCircuits():
        realnodes = []
        for node in nodes:
            if node in cirnodemap:
                realnodes.append(cirnodemap[node])
            else:
                psglobals.TotalNodes += 1
                realnodes.append(psglobals.TotalNodes)
                cirnodemap[node] = psglobals.TotalNodes
                psglobals.NodeMap[cirpath+(node,)] = psglobals.TotalNodes
                
        currpath = cirpath + (sname,)
        circuit.AppendSubcircuit(CreateCircuitHierarhy(psglobals.CircuitNetlists[model], currpath, sname, realnodes))
    
    return circuit    
        
def CreateGlobalArrays():
    psglobals.NodePhase = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
    psglobals.NodeVoltage = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
    psglobals.NodeDVoltage = numpy.zeros((psglobals.TotalNodes), dtype = numpy.float64)
    psglobals.NodeState = numpy.zeros((psglobals.TotalNodes), dtype = numpy.int32)
    psglobals.NodeStateNew = numpy.zeros((psglobals.TotalNodes), dtype = numpy.int32)
    
    psglobals.ElementPhase = numpy.zeros((psglobals.TotalElementValues), dtype = numpy.float64)
    psglobals.ElementVoltage = numpy.zeros((psglobals.TotalElementValues), dtype = numpy.float64)
    psglobals.ElementCurrent = numpy.zeros((psglobals.TotalElementValues), dtype = numpy.float64)
    psglobals.ElementN = numpy.zeros((psglobals.TotalElementValues), dtype = numpy.int32)
    psglobals.ElementInc = numpy.zeros((psglobals.TotalElementValues), dtype = numpy.uint8)
    psglobals.ElementDec = numpy.zeros((psglobals.TotalElementValues), dtype = numpy.uint8)
        
    psglobals.TotalParameters += len(psglobals.ParametersDef)
    # Add 1 for tcurr
    psglobals.TotalInternals += len(psglobals.InternalsDef) + 1
    
    psglobals.ParameterValues = numpy.zeros((psglobals.TotalParameters), dtype = numpy.float64)
    psglobals.ExternalValues = numpy.zeros((psglobals.TotalExternals), dtype = numpy.float64)
    psglobals.InternalValues = numpy.zeros((psglobals.TotalInternals), dtype = numpy.float64)
    psglobals.ValueValues = numpy.zeros((psglobals.TotalValues), dtype = numpy.float64)

def CreateCircuitVariables(circuit):
    global externals_index, internals_index, values_index

    # Update subcircuit
    for subckt in circuit.subcircuits.values():
        CreateCircuitVariables(subckt)

    # Externals
    for extdef in circuit.circuit_ast.external_list:
        extvar = ExternalVar(circuit.FullPath(), extdef, externals_index, circuit)
        circuit.externals[extvar.Name()] = extvar
        externals_index += 1

    # Internals
    for intdef in circuit.circuit_ast.internal_list:
        internal = InternalVar(circuit.FullPath(), intdef, internals_index, circuit)
        circuit.internal_list.append(internal)
        circuit.internals[internal.Name()] = internal
        internals_index += 1

    # Values
    for valdef in circuit.circuit_ast.value_list:
        val = ValueVar(circuit.FullPath(), valdef, values_index, circuit)
        circuit.value_list.append(val)
        circuit.values[val.Name()] = val
        values_index += 1

    # Element models
    for elem in circuit.elements.values():
        elem.UpdateModel()
        
    # Rules
    for ruledef in circuit.circuit_ast.rule_list:
        rule = Rule(circuit, ruledef)
        circuit.AppendRule(rule)

    # Freeze list
    for ename in circuit.circuit_ast.freeze_list:
        circuit.freeze_junc.add(ename)
    
def FindCircuitFile(fname, prepend_current_dir = False):
    if 'PSCAN_CIRCUIT_PATH' in os.environ:
        cirpath = os.environ['PSCAN_CIRCUIT_PATH']
    else:
        cirpath = ''
    dirlist = cirpath.split(';')
    if prepend_current_dir:
        dirlist += ['.']
    for dir in dirlist:
        pfile = dir + "/" + fname
        if os.path.isfile(pfile):
            return(os.path.normpath(pfile))
    return(fname)

def LoadScriptFile(fname):
    circuit_ast = None
    print("Load script file {}".format(fname))
    pscan_lex.lexer.lineno = 1
    parser.fname = fname
    parlst = parser.parse(open(fname, "r").read(), lexer=pscan_lex.lexer, tracking=True)
    for pelem in parlst:
        if pelem[0] == 'CIRCUIT':
            circuit_ast = CircuitAST(pelem[1], pelem[2], pelem[3])
            psglobals.CircuitScriptMap[circuit_ast.Name()] = circuit_ast
        elif pelem[0] == 'PARAMETER':
            for vardecl in pelem[1]:
                if vardecl[0] == 'VARDECL':
                    psglobals.ParametersDef.append(VardeclAST(vardecl[1], vardecl[2]))
        elif pelem[0] == 'INTERNAL':
            for vardecl in pelem[1]:
                if vardecl[0] == 'VARDECL':
                    psglobals.InternalsDef.append(VardeclAST(vardecl[1], vardecl[2]))
        else:
            print("Unknown script element in file " + fname + " : " + str(pelem))
    # Load .par file, if exists
    if circuit_ast is not None:
        (fbase, fext) = os.path.splitext(fname)
        if fbase != '':
            parfilename = FindCircuitFile(fbase + ".par", True)
            try:
                parfile = open(parfilename, "r")
            except:
                # .par file do not exists
                return

            line = parfile.readline()
            while line != '':
                if line[0] != '#':
                    try:
                        lst = line.split()
                        if len(lst) >= 4 and lst[2] != '#' and lst[1] == '=' and lst[3] == ',':
                            # New format <param> = <val> ,
                            parname = lst[0]
                            parval = float(lst[2])
                        else:
                            # Old format <param> <val>
                            parname = lst[0]
                            parval = float(lst[1])
                        circuit_ast.UpdateParameterValue(parname, parval)
                    except:
                        print("Cannot read parameter value from par file " + fname + " line: " + line)
                    line = parfile.readline()


def ReadCircuits(circuit_name, script_files):
    for fname in script_files:
        LoadScriptFile(fname)
            
    ReadSpice(circuit_name, FindCircuitFile(circuit_name + ".cir"))
    for cname in psglobals.CircuitNetlists.keys():
        if cname not in psglobals.CircuitScriptMap:
            LoadScriptFile(FindCircuitFile(cname + '.hdl'))


def CleanupCircuitScripts():
    # Remove unused circuit definitions
    cirnames = list(psglobals.CircuitScriptMap.keys())
    for cirname in cirnames:
        if not psglobals.CircuitScriptMap[cirname].GetUsedFlag():
            del psglobals.CircuitScriptMap[cirname]

    # Calculate total number of Parameters
    for cirdef in psglobals.CircuitScriptMap.values():
        psglobals.TotalParameters += len(cirdef.parameter_list)


def InitVariables():
    UpdateGlobalInternals()
    psglobals.RootCircuit.UpdateElementParameters()

def InitializeElementModels():
    for elem in psglobals.ElementList:
        nodes = elem.Nodes()
        virtual_nodes = elem.VirtualNodes()
        if len(nodes) == 2:
            # Regular two-node element
            if len(virtual_nodes) == 0:
                i = nodes[0]
                j = nodes[1]
                if i == 0:
                    ind = [0, 0, 0, SP.SparseIndexMap[(j-1,j-1)]]
                elif j == 0:
                    ind = [SP.SparseIndexMap[(i-1,i-1)], 0, 0, 0]
                else:
                    ind = [SP.SparseIndexMap[(i-1,i-1)], 
                           SP.SparseIndexMap[(i-1,j-1)],
                           SP.SparseIndexMap[(j-1,i-1)],
                           SP.SparseIndexMap[(j-1,j-1)]]
                elem.model.InitModel(ind[0], ind[1], ind[2], ind[3])
            if elem.etype == 'l' and len(virtual_nodes) == 1:
                # Inpedance model of the inductor
                i = nodes[0]
                j = nodes[1]
                q = virtual_nodes[0]
                if i == 0:
                    ind = [0, 
                           SP.SparseIndexMap[(j-1,q-1)],
                           0,
                           SP.SparseIndexMap[(q-1,j-1)],
                           SP.SparseIndexMap[(q-1,q-1)]]
                elif j == 0:
                    ind = [SP.SparseIndexMap[(i-1,q-1)],
                           0,
                           SP.SparseIndexMap[(q-1,i-1)],
                           0,
                           SP.SparseIndexMap[(q-1,q-1)]]
                else:
                    ind = [SP.SparseIndexMap[(i-1,q-1)], 
                           SP.SparseIndexMap[(j-1,q-1)],
                           SP.SparseIndexMap[(q-1,i-1)],
                           SP.SparseIndexMap[(q-1,j-1)],
                           SP.SparseIndexMap[(q-1,q-1)]]
                elem.model.InitModel(ind[0], ind[1], ind[2], ind[3], ind[4])
            if (elem.etype == 'u' or elem.etype == 'p') and len(virtual_nodes) == 1:
                # Voltage source
                i = nodes[0]
                j = nodes[1]
                q = virtual_nodes[0]
                if i == 0:
                    ind = [0, 
                           SP.SparseIndexMap[(j-1,q-1)],
                           0,
                           SP.SparseIndexMap[(q-1,j-1)]]
                elif j == 0:
                    ind = [SP.SparseIndexMap[(i-1,q-1)],
                           0,
                           SP.SparseIndexMap[(q-1,i-1)],
                           0]
                else:
                    ind = [SP.SparseIndexMap[(i-1,q-1)], 
                           SP.SparseIndexMap[(j-1,q-1)],
                           SP.SparseIndexMap[(q-1,i-1)],
                           SP.SparseIndexMap[(q-1,j-1)]]
                elem.model.InitModel(ind[0], ind[1], ind[2], ind[3])
        elif elem.etype == 'm' and len(nodes) == 0 and len(virtual_nodes) == 2:
            # Mutual inductance
            q = virtual_nodes[0]
            v = virtual_nodes[1]
            ind = [0, SP.SparseIndexMap[(q-1,v-1)], 
                   SP.SparseIndexMap[(v-1,q-1)], 0]
            elem.model.InitModel(ind[0], ind[1], ind[2], ind[3])
        else:
            raise Exception("Invalid nodes for element: " + str(elem))

def LoadCircuit(circuit_name, scripts):
    #try:
        ReadCircuits(circuit_name, scripts)
    #except Exception as err:
        #print("Error: {}".format(err.args[0]))
        #sys.exit(0)

    #try:
        root_cirdef = psglobals.CircuitNetlists[circuit_name]
    #except:
        #print("No root circuit")
        #sys.exit(0)

    #try:
        psglobals.RootCircuit = CreateCircuitHierarhy(root_cirdef, (), circuit_name, [])
    #except Exception as err:
        #traceback.print_exc()
        #print("Error: {}".format(err.args[0]))
        #sys.exit(0)

    #try:
        CleanupCircuitScripts()
        CreateGlobalArrays()
        CreateSimulationArrays()
        CreateGlobalParameters()
        CreateGlobalInternals()
        CreateCircuitParameters()
        CreateCircuitVariables(psglobals.RootCircuit)
        InitializeElementModels()
    #except Exception as err:
        #traceback.print_exc()
        #print("Error: {}".format(err.args[0]))
        #sys.exit(0)
