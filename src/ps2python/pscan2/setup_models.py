#!/usr/bin/env python

"""
setup_models.py  to build element models code with cython
"""
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy # to get includes

# from Cython.Compiler.Options import directive_defaults
# directive_defaults['linetrace'] = True
# directive_defaults['binding'] = True


setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("ElementModels_c", 
			["ElementModels_c.pyx"], 
			#extra_compile_args=['/openmp'],
			#define_macros = [('CYTHON_TRACE', '1'),('CYTHON_TRACE_NOGIL', '1')],
			include_dirs = [numpy.get_include()])]
)
