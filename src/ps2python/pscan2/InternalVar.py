# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals
from .Expression import Expression


class InternalVar:
    """
    Internal variable in circuit. If the whole circuit contains several 
    circuits, internal variables in each circuit will be independent.
    Internal variables can depend on parameters, external parameters, 
    and simulation time. But they cannoy depend on node's or element's 
    phases, currents, voltages, etc.

    Methods
    ---------
    Name() : string
        Name of the external parameter
    Value() : float
        Value of external parameter
    """
    def __init__(self, path, vardecl, index, circuit, no_update = False):
        self.path = path
        self.circuit = circuit
        self.name = vardecl.Name()
        self.no_update = no_update
        # circuit cannot be None
        if circuit == None:
            # Global internal parameter
            self.full_name = self.name
        else:
            cir_fname = circuit.FullName()
            if cir_fname == ".":
                self.full_name = "." + self.name
            else:
                self.full_name = cir_fname + "." + self.name
        self.value_exp = Expression(vardecl.Value(), circuit, False)
        self.value = 0.0
        self.index = index
        if not self.no_update:
            psglobals.InternalValues[self.index] = self.value
        
    def __repr__(self):
        return("Internal {}.{} = {} ({})".format(self.path, self.name, self.value, self.index))
            
    def Name(self):
        return(self.name)
        
    def Path(self):
        return(self.path)
        
    def FullName(self):
        return(self.full_name)    
        
    def FindType(self):
        return('v')

    def Value(self):
        return(self.value)
        
    def Index(self):
        return(self.index)

    def Update(self):
        if self.no_update:
            self.value = psglobals.InternalValues[self.index]
        else:
            self.value = self.value_exp.Value()
            psglobals.InternalValues[self.index] = self.value
