﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals
from . import SimulationParameters
from .Rule import *

class IncDecMap:
    def __init__(self):
        self.map = {}

    def __repr__(self):
        return str(self.map)

    def clear(self):
        self.map = {}

    def has(self, key):
        return key in self.map

    def add(self, key_set):
        for key in key_set:
            if key in self.map:
                self.map[key] += 1
            else:
                self.map[key] = 1
    
    def remove(self, key_set):
        for key in key_set:
            if key in self.map:
                self.map[key] -= 1
                if self.map[key] == 0:
                    del self.map[key]

class Circuit:
    def __init__(self, circuit_ast, path, name, instance_nodes):
        self.circuit_ast = circuit_ast
        self.path = path
        self.name = name
        #self.full_path = self.path + (self.name,)
        self.full_name = "." + ".".join(self.path)
        self.actual_nodes = instance_nodes
        self.nodes = {}
        self.elements = {}
        self.subcircuits = {}
        self.freeze_junc = set()
        self.inc_junc = IncDecMap()
        self.dec_junc = IncDecMap()
        self.externals = {}
        self.internal_list = []
        self.internals = {}
        self.value_list = []
        self.values = {}
        self.rule_list = []
        self.rule_map = {}
        self.exit_rule_triggered = False
        self.exit_rule_message = ''
        self.has_phase_sources = False
        self.subcircut_has_phase_sources = False

    def __repr__(self):
        return(self.circuit_ast.name + ":" + self.full_name)
        
    def Name(self):
        return self.name

    def CircuitName(self):
        return self.circuit_ast.name
    
    def Path(self):
        return self.path
        
    def FullPath(self):
        return self.path
        
    def FullName(self):
        return self.full_name

    def Nodes(self):
        return self.nodes
    
    def AppendNode(self, node):
        self.nodes[node.Name()] = node

    def AppendRule(self, rule):
        self.rule_list.append(rule)
        self.rule_map[rule.Name()] = rule
    
    def Elements(self):
        return self.elements

    def FindObject(self, path, otype='p'):
        #print('FindObject', str(path))
        if isinstance(path, str):
            path = path.split(".")
        if path[0] == "" and len(path) > 1:
            path = path[1:]
        if len(path) == 1:
            name = path[0].lower()
            if otype[0] == 'p':
                if name in self.circuit_ast.parameters:
                    return self.circuit_ast.parameters[name]
                elif name in self.externals:
                    return self.externals[name]
                else:
                    raise Exception("Object with type '{}' {} not found in circuit {}".format(otype, name, self.full_name))
            elif otype[0] == 'v':
                if name in self.internals:
                    return self.internals[name]
                elif name in self.values:
                    return self.values[name]
                else:
                    raise Exception("Object with type '{}' {} not found in circuit {}".format(otype, name, self.full_name))
            elif otype[0] == 'r':
                if name in self.rule_map:
                    return self.rule_map[name]
            elif otype[0] == 'n':
                if name in self.nodes:
                    return self.nodes[name]
            elif otype[0] == 'e':
                if name in self.elements:
                    return self.elements[name]
            else:
                raise Exception("Unknown type '{}', can be 'n' - nodes, 'e' - elements, 'p' - parameters, 'v' - values, 'r' - rules".format(otype))
            raise Exception("Object with type '{}' {} not found in circuit {}".format(otype, name, self.full_name))    
        else:
            subcircuit = path[0].lower()
            if subcircuit in self.subcircuits:
                return self.subcircuits[subcircuit].FindObject(path[1:], otype)
            else:
                raise Exception("Subcircuit {} not found in circuit {}".format(subcircuit, self.full_name))
        
    def FindRe(self, path, obj_re, otype='p'):
        if len(path) == 1:
            # Current circuit
            result = []
            # Search for nodes
            if otype[0] == 'n':
                for name in self.nodes:
                    if obj_re.match(name) != None:
                        result.append(self.nodes[name])
            elif otype[0] == 'e':
                for name in self.elements:
                    if obj_re.match(name) != None:
                        result.append(self.elements[name])
            elif otype[0] == 'r':
                for name in self.rule_map:
                    if obj_re.match(name) != None:
                        result.append(self.rule_map[name])
            elif otype[0] == 'p':
                for name in self.circuit_ast.parameters:
                    if obj_re.match(name) != None:
                        result.append(self.circuit_ast.parameters[name])
                for name in self.externals:
                    if obj_re.match(name) != None:
                        result.append(self.externals[name])
            elif otype[0] == 'v':
                for name in self.internals:
                    if obj_re.match(name) != None:
                        result.append(self.internals[name])
                for name in self.values:
                    if obj_re.match(name) != None:
                        result.append(self.values[name])
            else:
                raise Exception("Unknown type '{}', can be 'n' - nodes, 'e' - elements, 'p' - parameters, 'v' - values".format(otype))
            return result
        else:
            subcircuit = path[1].lower()
            if subcircuit in self.subcircuits:
                return self.subcircuits[subcircuit].FindRe(path[1:], obj_re, otype)
            else:
                raise Exception("Subcircuit {} not found in circuit {}".format(subcircuit, self.full_name))

    def LookupCircuit(self, path_list):
        sname = path_list[0].lower()
        if sname in self.subcircuits:
            subcircuit = self.subcircuits[sname]
        else:
            raise Exception("Subcircuit {} not found in circuit {}".format(sname, self.full_name))
        if len(path_list) == 1:
            # We found the circuit
            return subcircuit
        else:
            # Recursive call
            return subcircuit.LookupCircuit(path_list[1:])

    def GetAllExternals(self, obj_re):
        result = []
        for name in self.externals:
            if obj_re.match(name) != None:
                result.append(self.externals[name])
        for sname in self.subcircuits:
            result += self.subcircuits[sname].GetAllExternals(obj_re)
        return result


    def AppendElement(self, element):
        self.elements[element.Name()] = element
        if element.Name()[0] == 'p':
            self.has_phase_sources = True
        
    def AppendSubcircuit(self, subcircuit):
        self.subcircut_has_phase_sources = self.subcircut_has_phase_sources or subcircuit.has_phase_sources
        self.subcircuits[subcircuit.Name()] = subcircuit
    
    def Parameters(self):
        return(self.circuit_ast.parameters)

    def Externals(self):
        return(self.externals)

    def Internals(self):
        return(self.internals)

    def Values(self):
        return(self.values)

    def Subcircuits(self):
        return(self.subcircuits)
        
    def RuleList(self):
        return(self.rule_list)

    def UpdateInternals(self):
        for intern in self.internal_list:
            intern.Update()
        for subckt in self.subcircuits.values():
            subckt.UpdateInternals()

    def UpdateElementParameters(self):
        for xparam in self.externals.values():
            xparam.Update()
        for intern in self.internal_list:
            intern.Update()
        # If optimization flag is True and tcurr > 10, we will recalculate parameters only for phase sources
        if psglobals.OptimizeSimulation and psglobals.CurrentTime > SimulationParameters.INITIAL_RAMP:
            if self.has_phase_sources:
                for elem in self.elements.values():
                    if elem.Name()[0] == 'p':
                        elem.UpdateParameters()
            if self.subcircut_has_phase_sources:
                for subckt in self.subcircuits.values():
                    if subckt.subcircut_has_phase_sources:
                        subckt.UpdateElementParameters()
        else:
            for elem in self.elements.values():
                elem.UpdateParameters()
            for subckt in self.subcircuits.values():
                subckt.UpdateElementParameters()

    def UpdateValues(self):
        for val in self.value_list:
            val.Update()
        for subckt in self.subcircuits.values():
            subckt.UpdateValues()

    def FreezeJunctions(self, jlist):
        self.freeze_junc |= set(jlist)
        
    def UnFreezeJunctions(self, jlist):
        self.freeze_junc -= set(jlist)
            
    def AddIncDec(self, incdec_list):
        self.inc_junc.add(incdec_list[0])
        self.dec_junc.add(incdec_list[1])

    def RemoveIncDec(self, incdec_list):
        self.inc_junc.remove(incdec_list[0])
        self.dec_junc.remove(incdec_list[1])

    def CheckJunctions(self):
        messages = []
        for elem in self.elements.values():
            if elem.ElementType() == 'j':
                if elem.Inc() and (not self.inc_junc.has(elem.Name()) and elem.Name() not in self.freeze_junc):
                    messages.append("{:.1f}: invalid INC for junction {} in {}".format(psglobals.CurrentTime, elem.Name(), str(self)))

                if elem.Dec() and (not self.dec_junc.has(elem.Name()) and elem.Name() not in self.freeze_junc):
                    messages.append("{:.1f}: invalid DEC for junction {} in {}".format(psglobals.CurrentTime, elem.Name(), str(self)))
        return messages

    def InitilizeRules(self):
        self.inc_junc.clear()
        self.dec_junc.clear()
        for rule in self.rule_list:
            rule.Initialize()
        for subckt in self.subcircuits.values():
            subckt.InitilizeRules()

    def ProcessRules(self):
        messages = self.CheckJunctions()
        if len(messages) > 0:
            status = False
        else:
            status = True
        for rule in self.rule_list:
            messages += rule.Step()
        for subckt in self.subcircuits.values():
            (subcircuit_status, subcircuit_messages) = subckt.ProcessRules()
            status = status and subcircuit_status
            messages += subcircuit_messages
        if self.exit_rule_triggered:
            status = False
            messages.append((psglobals.CurrentTime, self.exit_rule_message, 'exit'))
            self.exit_rule_triggered = False
            self.exit_rule_message = ''
        return (status, messages)

    def CheckActiveRules(self):
        for rule in self.rule_list:
            if rule.IsActive():
                return True
        for subckt in self.subcircuits.values():
            if subckt.CheckActiveRules():
                return True
        return False

    def CurrentRulesStatus(self):
        result = []
        for rule in self.rule_list:
            rule_status = rule.CurrentState()
            if rule_status != None:
                result.append(rule_status)
        for subckt in self.subcircuits.values():
            srule_status = subckt.CurrentRulesStatus()
            if len(srule_status) > 0:
                result += srule_status
        return(result)



