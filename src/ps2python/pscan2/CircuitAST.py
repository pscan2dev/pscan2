﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

class CircuitAST:
    def __init__(self, name, node_list, circuit_defs_list):
        self.is_used = False
        self.name = name
        self.node_list = node_list
        self.freeze_list = []
        self.parameter_list = []
        self.parameters = {}
        self.parfile_parameters = {}
        self.external_list = []
        self.internal_list = []
        self.value_list = []
        self.rule_list = []
        for cdef in circuit_defs_list:
            if cdef[0] == 'FREEZE':
                self.freeze_list += cdef[1]
            elif cdef[0] == 'PARAMETER':
                for vdecl in cdef[1]:
                    if vdecl[0] == 'VARDECL':
                        self.parameter_list.append(VardeclAST(vdecl[1], vdecl[2]))
                    else:
                        print('Invalid parameter declaration ' + str(vdecl))
            elif cdef[0] == 'EXTERNAL':
                for vdecl in cdef[1]:
                    if vdecl[0] == 'VARDECL':
                        self.external_list.append(VardeclAST(vdecl[1], vdecl[2]))
                    else:
                        print('Invalid external declaration ' + str(vdecl))
            elif cdef[0] == 'INTERNAL':
                for vdecl in cdef[1]:
                    if vdecl[0] == 'VARDECL':
                        self.internal_list.append(VardeclAST(vdecl[1], vdecl[2]))
                    else:
                        print('Invalid indernal declaration ' + str(vdecl))
            elif cdef[0] == 'VALUE':
                for vdecl in cdef[1]:
                    if vdecl[0] == 'VARDECL':
                        self.value_list.append(VardeclAST(vdecl[1], vdecl[2]))
                    else:
                        print('Invalid value declaration ' + str(vdecl))
            elif cdef[0] == 'RULE':
                self.rule_list.append(RuleAST(cdef[1], cdef[2], cdef[3]))
    
    def __repr__(self):
        res = 'CircuitAST: ' + self.name + ' ' + str(self.node_list) + '\n'
        if len(self.parameter_list) > 0:
            res += '\tParameters:\n'
            for vardecl in self.parameter_list:
                res += '\t\t' + str(vardecl) + '\n'
        if len(self.external_list) > 0:
            res += '\tExternals:\n'
            for vardecl in self.external_list:
                res += '\t\t' + str(vardecl) + '\n'
        if len(self.internal_list) > 0:
            res += '\tInternals:\n'
            for vardecl in self.internal_list:
                res += '\t\t' + str(vardecl) + '\n'    
        if len(self.value_list) > 0:
            res += '\tValues:\n'
            for vardecl in self.value_list:
                res += '\t\t' + str(vardecl) + '\n'
        if len(self.rule_list) > 0:
            res += '\tRules:\n'
            for rule in self.rule_list:
                res += '\t\t' + str(rule) + '\n'
        return(res)
                
    def Name(self):
        return(self.name)

    def GetUsedFlag(self):
        return(self.is_used)

    def SetUsedFlag(self):
        self.is_used = True
        
    def UpdateParameterValue(self, pname, pvalue):
        self.parfile_parameters[pname] = pvalue
        
        
class VardeclAST:
    def __init__(self, name, value):
        self.ident = name
        self.value = value
        
    def __repr__(self):
        return('Vardecl: ' + self.ident + '=' + str(self.value))
        
    def Name(self):
        return self.ident
        
    def Value(self):
        return self.value

        
class RuleAST:
    def __init__(self, name, initexp, explist):
        self.name = name
        self.initexp = initexp
        self.explist = explist
        
    def __repr__(self):
        res = 'RuleAST: ' + self.name + ' (' + str(self.initexp) + ')\n'
        for exp in self.explist:
            res += '\t' + str(exp) + '\n'
        return(res)
        
