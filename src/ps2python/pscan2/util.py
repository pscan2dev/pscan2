# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)
# ak 20190910

import math, logging, time, fnmatch, re, array, os, time
from . import psglobals
from .Simulation import TimeStep, InitSimulation
from .Element import Element
from .Node import Node
from .ParameterVar import ParameterVar
from .ExternalVar import ExternalVar
from .Expression import Expression
from .expr_parser import expr_parser
from .pscan_lex import lexer as pscan_lexer
from .Average import Average
from . import SimulationParameters

_logger_initialized = False

def initialize_logger(name = 'pscan2'):
    global _logger_initialized
    if _logger_initialized:
        return

    logger = logging.getLogger('pscan2')
    logger.setLevel(logging.DEBUG)
    # Jupyter already created console handler
    # We have to reuse it, otherwise output will be doubled
    handler_console = None
    handlers = logger.handlers
    for h in handlers:
        if isinstance(h, logging.StreamHandler):
            handler_console = h
            break
    if handler_console is None:
        handler_console = logging.StreamHandler()
        logger.addHandler(handler_console)

    # create file handler which logs even info messages
    fh = logging.FileHandler(name+'.log')
    fh.setLevel(logging.DEBUG)

    # Remove old console handler
    logger.removeHandler(handler_console)

    handler_console.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(message)s')
    handler_console.setFormatter(formatter)
    fh.setFormatter(formatter)

    # add the handlers to logger
    logger.addHandler(handler_console)
    logger.addHandler(fh)
    logger.debug("***************************************")
    logger.debug(time.strftime("LOG STARTED: %a, %d %b %Y %H:%M:%S"))
    _logger_initialized = True

def create_expression(exp_str, circuit_path):
    path_lst = circuit_path.split(".")
    if path_lst[0] == '' and path_lst[1] == '':
        circuit = psglobals.RootCircuit
    else:
        circuit = psglobals.RootCircuit.LookupCircuit(path_lst[1:])
    
    ast_expr = expr_parser.parse(exp_str, lexer=pscan_lexer, debug=0)
    expr = Expression(ast_expr, circuit, phase_dependence = True)
    return expr

    
# Find object of certain type in a circuit hierarchy
# Supported object types are:
# 'e' - element
# 'n' - node
# 'p' - parameters or externals (default)
# 'v' - internals or values
# 'r' - rules
def find(full_path, otype='p'):
    """
    Find objects in circuit hierarchy by name.
    ------------------
    <name> is a hierarchical name of object. For global parameters or global 
    internals it is just a name. For example: find(XI). For parameters in circuits,
    full_path should be circuit name, "." and parameter name, for example find("jtl.p1", "p").
    For the objects in root circuit, name should start with . For example: find(.XJ1). 
    For objects on deeper levels of hierarchy, name should contain all subcurcuit instances, 
    separated by . For example: find(.m1.jtl1.XJ1). For elements, nodes, and rules
    "." in the beginning of name should be removed, so junction J1 in root circuit will
    have path "J1". In jtl1 it will have path "jtl1.J1"

    <otype> is a type of object to find:
    'e' - circuit element
    'n' - node
    'p' - parameter or external. Default value
    'v' - internal or value
    'r' - rules

    Function will return instance of found object, or raise exception, if object was not found
    """
    path = full_path.split(".")
    if len(path) == 1:
        pname = full_path.lower()
        # Global parameters
        if otype == 'p' and pname in psglobals.Parameters:
            return psglobals.Parameters[pname]
        elif otype == 'v' and pname in psglobals.Internals:
            return psglobals.Internals[pname]
        elif otype == 'e' or otype == 'n' or otype == 'r':
            return psglobals.RootCircuit.FindObject(path, otype)
        else:
            raise Exception("Object with type '{}' {} not found in psglobals".format(otype, pname))
    elif len(path) == 2 and otype[0] == 'p' and path[0] != '':
        return param_find(path[0], path[1])
    else:
        if path[0] == '':
            return psglobals.RootCircuit.FindObject(path[1:], otype)
        else:
            return psglobals.RootCircuit.FindObject(path, otype)
    

def find_re(full_path, otype='p'):
    """
    Similar to find() function, but returns list of objects, given simple regular 
    expression as element name. 
    -------------------
    It uses Python module fnmatch and support following special symbols in pattern:
    Pattern     Meaning
    *           matches everything
    ?           matches any single character
    [seq]       matches any character in seq
    [!seq]      matches any character not in seq

    <otype> is a type of objects to find, the same as in find() function
    'e' - circuit element
    'n' - node
    'p' - parameter or external. Default value
    'v' - internal or value
    'r' - rule

    Function will return list of found objects. If no objects match the pattern, list will be empty.
    For example: find_re(m1.xj*) will find all parameters in circuit m1, started with xj.
    """
    path = full_path.split(".")
    # Regular expression for the last element in full path
    obj_re = re.compile(fnmatch.translate(path[-1].lower()))
    if len(path) == 1:
        # Global parameters
        result = []
        if otype == 'p':
            for pname in psglobals.Parameters.keys():
                if obj_re.match(pname) != None:
                    result.append(psglobals.Parameters[pname])
        elif otype == 'v':
            for pname in psglobals.Internals.keys():
                if obj_re.match(pname) != None:
                    result.append(psglobals.Internals[pname])
        elif otype == 'r' or otype == 'e' or otype == 'n':
           return psglobals.RootCircuit.FindRe(path, obj_re, otype) 
        else:
            raise Exception("Unknown object type '{}'".format(otype))
        return result
    return psglobals.RootCircuit.FindRe(path[:-1], obj_re, otype)


def param_find(cirdef, pname):
    """
    Find parameter <parameter_name> in a circuit <circuit_name>. 
    --------------
    There is no need to specify full hierarchical path for parameters. 
    They can be found giving only circuit name. 
    
    Function will return object instance of the parameter, or raise exception.
    """
    circuit_ast = psglobals.CircuitScriptMap[cirdef]
    if pname in circuit_ast.parameters:
        return circuit_ast.parameters[pname]
    else:
        raise Exception("Cannot find parameter {} in circuit {}".format(pname, cirdef))

def param_re(cirdef, pattern):
    """
    Similar to param_find() function, but returns list of circuit parameters matches 
    simple regular expression, with same syntax as in find_re(). 
    -------------
    For example: param_re(not1, j*) returns all parameters in circuit not1, 
    started with character j.
    """
    circuit_ast = psglobals.CircuitScriptMap[cirdef]
    obj_re = re.compile(fnmatch.translate(pattern.lower()))
    result = []
    for name in circuit_ast.parameters:
        if obj_re.match(name) != None:
            result.append(circuit_ast.parameters[name])
    return result

def all_externals(full_path, pattern):
    if full_path[0] != '.':
        raise Exception("Circuit path {} should start with dot(.)".format(full_path))
    path = full_path.split(".")
    obj_re = re.compile(fnmatch.translate(pattern))
    if path[1] == '':
        circuit = psglobals.RootCircuit
    else:
        circuit = psglobals.RootCircuit.LookupCircuit(path[1:])
    return circuit.GetAllExternals(obj_re)


def update_circuit_parameters():
    # First update global internals
    for intdef in psglobals.InternalsList:
        intdef.Update()
    # Update all internals and element parameters in circuit, starting from root circuit
    psglobals.RootCircuit.UpdateElementParameters()

def print_header():
    return "\n#-------------------------\n#{}\n#{}\n#-------------------------\n".format(time.ctime(),os.getcwd())
        
def format_rule_message(rlist):
    if len(rlist) == 2:
        # Current rule state
        return("Rule {} active clause # {}".format(rlist[0], rlist[1]))
    if len(rlist) == 3:
        # Rule activate/deactivate/exit
        if rlist[2] == 'exit':
            return("{:.1f} Exit triggered: {}".format(rlist[0], rlist[1]))
        else:
            return("{:.1f} Rule {} {}".format(rlist[0], rlist[1], rlist[2]))
    elif len(rlist) == 4:
        # Clause passed
        return("{:.1f} Rule {} clause # {} {}".format(rlist[0], rlist[1], rlist[2], rlist[3]))
    elif len(rlist) == 5:
        # Expression in clause passed / print message
        if rlist[4] == 'message':
            return("{:.1f} Rule {} clause # {}: {}".format(rlist[0], rlist[1], rlist[2], rlist[3]))
        else:
            return("{:.1f} Rule {} clause # {} expression # {} {}".format(rlist[0], rlist[1], rlist[2], rlist[3]+1, rlist[4]))
    else:
        return(str(rlist))

def pretty_print_rules(messages):
    logger = logging.getLogger('pscan2')
    last_time = 0.0
    max_rulename = 0
    rule_map = {}
    for msg in messages:
        if len(msg) == 3 and msg[2] != 'exit':
            if msg[1] not in rule_map:
                rule_map[msg[1]] = array.array('d')
            rule_map[msg[1]].append(msg[0])
            max_rulename = max(max_rulename, len(msg[1]))
            last_time = msg[0]

    nchars = 80 - max_rulename - 3
    rule_format = "{{:{}s}} : ".format(max_rulename)
    dt = last_time / nchars
    output_chars = ['|', '*']
    
    for msg in messages:
        if len(msg) == 3 and msg[2] != 'exit':
            if msg[1] not in rule_map:
                rule_map[msg[1]] = array.array('d')
            rule_map[msg[1]].append(msg[0])

    rule_list = list(rule_map.keys())
    rule_list.sort()
    for rule in rule_list:
        rarray = rule_map[rule]
        outline = rule_format.format(rule)
        interval_end = dt
        current_index = 0
        current_status = 0
        while interval_end < last_time:
            char_index = current_status
            if current_index < len(rarray):
                while current_index < len(rarray) and rarray[current_index] <= interval_end:
                    current_index += 1
                    current_status = (current_status + 1) % 2
                    char_index = max(current_status, char_index)
            outline += output_chars[char_index]
            interval_end += dt
        logger.debug(outline)


def simulate(max_time, print_messages = True):
    """
    Simulate circuit and test SFQHDL rules.
    ------------
    <max_time> is the maximal simulation time. If, during simulation,  some junction 
    will switch, but it was unexpected (there are no any rule, which is waiting 
    for its switching in inc() or dec() expression), simulation will stop. 

    <print_message> if True, there will be trace of rule execution during simulation. 

    <pretty_print> if True, rule traces will be printed in nice ascii pseudo-graphical form. 

    If there was no any unexpected Josephson junction switched during simulation and there 
    are no any active rules left, function will returns True, otherwise False
    """
    logger = logging.getLogger('pscan2')
    InitSimulation()
    all_messages = []
    psglobals.RootCircuit.InitilizeRules()
    while psglobals.CurrentTime <= max_time:
        if psglobals.GUICallback is not None:
            psglobals.GUICallback()
        TimeStep()
        (status, messages) = psglobals.RootCircuit.ProcessRules()
        if print_messages and len(messages) > 0:
            all_messages += messages
            logger.debug("------------------------------")
            for msg in messages:
                logger.debug(format_rule_message(msg))
        if not status:
            if print_messages:
                logger.debug("HDL script failed")
                logger.debug("Current rules status:")
                messages = psglobals.RootCircuit.CurrentRulesStatus()
                for msg in messages:
                    logger.debug(format_rule_message(msg))
                pretty_print_rules(all_messages)
            return status
    end_status = not psglobals.RootCircuit.CheckActiveRules()
    if print_messages: 
        if not end_status:
            logger.debug("----------------------------")
            logger.debug("There are active rules at the END")
            logger.debug("Current rules status:")
            messages = psglobals.RootCircuit.CurrentRulesStatus()
            for msg in messages:
                logger.debug(format_rule_message(msg))
        else:
            logger.debug("OK")
        pretty_print_rules(all_messages)
    return(end_status)


def get_margin(p, max_time, x1, x2, eps, messages_level):
    """
    Finds border between operational and non-operational region using dichotomy
    -------------------
    <p> - the parameter
    <max_time> - maximal simulation time for simulate() function
    <x1> - operational value of p
    <x2> - non-operational value of p
    <eps> - precision
    <messages_level> argument set amount of printed messages:
        0 - no messages (default value)
        1 - basic messages about calculating <parameter> margins
        2 - all messages, including rules execution traces, during circuit simulations
    """
    logger = logging.getLogger('pscan2')
    if messages_level >= 2:
        simulate_messages = True
    else:
        simulate_messages = False

    if abs(x1 - x2) < 2.0*eps:
        if messages_level >= 1:
            logger.debug("Found margin {:.2f}".format((x1 + x2) / 2.0))
        return (x1 + x2) / 2.0

    xnew = x1 + (x2 - x1) * 0.68
    if messages_level >= 1:
        logger.debug("Check value {:.2f}".format(xnew))
    p.SetValue(xnew)
    if simulate(max_time, simulate_messages):
        return get_margin(p, max_time, xnew, x2, eps, messages_level)
    else:
        return get_margin(p, max_time, x1, xnew, eps, messages_level)


def margin(param, max_time, **kwargs):
    """
    Calculate operating margins of parameter.
    -----------
    <parameter> - name of parameter or circuit parameter or external object instance, 
    obtained with functions find() or param().

    <max_time> - maximal simulation time, the same as in simulate() function

    <messages_level> argument set amount of printed messages:
    0 - no messages (default value)
    1 - basic messages about calculating <parameter> margins
    2 - all messages, including rules execution traces, during circuit simulations

    <max_margin> is the maximal expected value of parameter margins. For example, 
    if parameter value is 1.0, max_marging equal to 0.4 means that circuit will 
    correctly works for parameter range from 0.6 to 1.4.
    Defaul value of max_margin is 0.4

    Function will returns list of two elements: left and right margins, 
    or list of two zeros, if circuit does not work with initial value of parameter.
    """
    if 'max_margin' in kwargs:
        max_margin = kwargs['max_margin']
    else:
        max_margin = 0.4

    if 'eps' in kwargs:
        eps = kwargs['eps']
    else:
        eps = 0.01

    if 'messages_level' in kwargs:
        messages_level = kwargs['messages_level']
    else:
        messages_level = 0
    
    if 'absolut' in kwargs:
        absolut = kwargs['absolut']
    else:
        absolut = False

    logger = logging.getLogger('pscan2')
    simulate_messages = messages_level >= 2

    if isinstance(param, str):
        p = find(param)
    elif isinstance(param, ParameterVar) or isinstance(param, ExternalVar):
        p = param
    else:
        raise Exception("Wrong type of margin() input argument, - it should be a String, a Parameter, or an External")

    init_value = p.Value()
    # Test initial point
    if messages_level >= 1:
        logger.debug("Find margins for {}".format(p.Name()))
        logger.debug("Test initial point {:.2f}".format(init_value))
    if not simulate(max_time, simulate_messages):
        if messages_level >= 1:
            logger.debug("Circuit does not work in the initial point")
        return([0.0,0.0])

    # Left margin
    val = init_value * (1 - max_margin)
    p.SetValue(val)
    if messages_level >= 1:
        logger.debug("Test max left margin {:.2f}".format(val))
    if simulate(max_time, simulate_messages):
        if messages_level >= 1:
            logger.debug("Works")
        left_margin = val
    else:
        if messages_level >= 1:
            logger.debug("Do not work")
        left_margin = get_margin(p, max_time, init_value, val, eps * init_value, messages_level)

    # Right margin
    val = init_value * (1 + max_margin)
    p.SetValue(val)
    if messages_level >= 1:
        logger.debug("Test max right margin {:.2f}".format(val))
    if simulate(max_time, simulate_messages):
        right_margin = val
    else:
        right_margin = get_margin(p, max_time, init_value, val, eps * init_value, messages_level)

    p.SetValue(init_value)
    if absolut:
        if messages_level >= 1:
            logger.debug("Parameter {}, value {:.2f}, margins [{:.2f},{:.2f}]".format(p.Name(), init_value, left_margin, right_margin))
        return [left_margin, right_margin]
    else:
        if abs(init_value) > eps:
            lm = (init_value - left_margin)/init_value
            rm = (right_margin - init_value)/init_value
            if messages_level >= 1:
                logger.debug("Parameter {}, value {:.2f}, margins [{:.2f},{:.2f}]".format(p.Name(), init_value, lm, rm))
            return [lm, rm]
        else:
            if messages_level >= 1:
                logger.debug("Parameter {}, value {:.2f} <= {:.3f} - No relative margins".format(p.Name(), init_value, eps))
            return [0.0, 0.0]


def check_margins(param, max_time, min_margin, max_margin, messages_level = 0):
    """
    Check operating margins of the parameter.
    -----------
    <parameter> - a name of the parameter or a circuit parameter or an external object instance, 
    obtained with functions find() or param().

    <max_time> - maximal simulation time, the same as in simulate() function

    <min_margin> - minimal value margin to check

    <max_margin> - maximal value of margin to check

    <messages_level> argument set amount of printed messages:
    0 - no messages
    1 - basic messages about calculating <parameter> margins
    2 - all messages, including rules execution traces, during circuit simulations

    Function will returns True if circuit works at minimal and maximal values and False otherwise.
    """
    logger = logging.getLogger('pscan2')
    simulate_messages = messages_level >= 2

    if isinstance(param, str):
        p = find(param)
    elif isinstance(param, ParameterVar) or isinstance(param, ExternalVar):
        p = param
    else:
        raise Exception("Wrong type of check_margin() input argument, can be string,Parameter, or External")
    init_value = p.Value()

    p.SetValue(init_value * (1 - min_margin))
    if messages_level >= 1:
        logger.debug("Test min margin {:.2f}".format(min_margin))
    if simulate(max_time, simulate_messages):
        if messages_level >= 1:
            logger.debug("Works")
    else:
        if messages_level >= 1:
            logger.debug("Does not work")
        p.SetValue(init_value)
        return False

    p.SetValue(init_value * (1 + max_margin))
    if messages_level >= 1:
        logger.debug("Test max margin {:.2f}".format(max_margin))
    if simulate(max_time, simulate_messages):
        if messages_level >= 1:
            logger.debug("Works")
    else:
        if messages_level >= 1:
            logger.debug("Does not work")
        p.SetValue(init_value)
        return False

    p.SetValue(init_value)
    return True


def save_parameters(cirlist):
    """
    save circuit parameters into parameters values files.
    ---------
    <circuit_list> is a list of circuit names to save. For each 
    circuit from the list parameters files <circuit>.par will be created.
    """
    for cirname in cirlist:
        if cirname in psglobals.CircuitScriptMap:
            circuit_ast = psglobals.CircuitScriptMap[cirname]
            plist = list(circuit_ast.parameters.values())
            if len(plist) > 0:
                try:
                    f = open(cirname+".par", "w")
                except:
                    Exception("Cannot open file " + cirname + ".par for writing")
                plist.sort()
                for param in plist:
                    f.write("{}\t = {:.3f} , \n".format(param.Name(), param.Value()))
                f.close()
        else:
            Exception("Cannot find circuit " + cirname)

def load_parameters(cirlist):
    """
    load circuit parameters from parameters values files.
    ---------
    <circuit_list> is a list of circuit names to load parameters. For each circuit 
    from the list, if parameters files <circuit>.par exists, it will be loaded.
    """
    for cirname in cirlist:
        if cirname in psglobals.CircuitScriptMap:
            circuit_ast = psglobals.CircuitScriptMap[cirname]
            try:
                f = open(cirname+".par", "r")
            except:
                print("Cannot open file " + cirname + ".par")
                continue
            line = f.readline()
            while line != '':
                if line[0] != '#':
                    lst = line.split()
                    if len(lst) >= 4 and lst[2] != '#' and lst[1] == '=' and lst[3] == ',':
                        # New format <param> = <val> ,
                        parname = lst[0]
                        parvalue = float(lst[2])
                    elif len(lst) >= 2:
                        # Old format <param> <val>
                        parname = lst[0]
                        parvalue = float(lst[1])
                    else:
                        print("Invalid line in par file for circuit {} line: {}".format(cirname, line))
                        line = f.readline()
                        continue

                    if parname in circuit_ast.parameters:
                        circuit_ast.parameters[parname].SetValue(parvalue)
                    else:
                        print("Cannot find parameter {} in circuit {}".format(parname, cirname))
                    
                line = f.readline()
            f.close()
        else:
            print("Cannot fine circuit "+cirname)


def save_all_parameters(fname):
    """
    Save all parameters in the circuit, including global parameters in the file fname
    """
    try:
        f = open(fname, "w")
    except:
        print("Cannot open file " + fname)
        return
    f.write(print_header())
    # Save global parameters
    plist = list(psglobals.Parameters.values())
    if len(plist) > 0:
        f.write("# Global parameters\n")
        plist = list(psglobals.Parameters.values())
        plist.sort()
        for param in plist:
            f.write("{}\t{:.3f}\t# {}\n".format(param.FullName(), param.Value(), param.DimValue()))
    # Parameters in circuits
    for circuit_ast in psglobals.CircuitScriptMap.values():
        plist = list(circuit_ast.parameters.values())
        if len(plist) > 0:
            f.write("# {} parameters\n".format(circuit_ast.Name()))
            plist.sort()
            for param in plist:
                f.write("{}\t{:.3f}\t#  {}\n".format(param.FullName(), param.Value(), param.DimValue()))
    f.close()

def load_all_parameters(fname):
    """
    Load parameters from the fname. File lines has format
    <parameter> <value>
    <parameter> can be a name of global parameter, or <circuit>.<name> for parameter in the circuit
    Comment line starter with #(sharp) symbol
    """
    try:
        f = open(fname, "r")
    except:
        print("Cannot open file " + fname)
        return
    lines = f.readlines()
    for line in lines:
        s = line.split("#")[0]
        if s != '':
            lst = s.split()
            if len(lst) == 2:
                nval=float(lst[1])
                plst = lst[0].split(".")
                if len(plst) == 1:
                    # Global parameter
                    if plst[0] in psglobals.Parameters:
                        pval=psglobals.Parameters[plst[0]].Value()
                        psglobals.Parameters[plst[0]].SetValue(nval)
                        if pval != nval:
                            print("{}\t{:.2f} -> {:.2f}".format(lst[0], pval, nval))
                    else:
                        print("Cannot access global parameter {}".format(plst[0]))
                elif len(plst) == 2:
                    # Circuit parameter in the form <circuit>.<param>
                    if plst[0] in psglobals.CircuitScriptMap:
                        circuit_ast = psglobals.CircuitScriptMap[plst[0]]
                        if plst[1] in circuit_ast.parameters:
                            pval=circuit_ast.parameters[plst[1]].Value()
                            circuit_ast.parameters[plst[1]].SetValue(nval)
                            if pval != nval:
                                print("{}\t{:.2f} -> {:.2f}".format(lst[0], pval, nval))
                        else:
                            print("Cannot find parameter {} in circuit {}".format(plst[1], plst[0]))    
                    else:
                        print("Unknown circuit ", plst[0])
                else:
                    print("Unknown parameter format ", lst[0])
            else:
                print("Wrong line ", line)
    f.close()


# def ivcurve_point(x, yexpr, yeps, twait, tmin, tmax):
#     tbegin = psglobals.CurrentTime
#     while psglobals.CurrentTime - tbegin < twait:
#         TimeStep()
#     yval = yexpr.Value()
#     tbegin = psglobals.CurrentTime
#     curtime = psglobals.CurrentTime
#     stime = 0
#     done = False
#     while stime < tmin or (stime < tmax and not done):
#         TimeStep()
#         dt = psglobals.CurrentTime - curtime
#         val = yexpr.Value()
#         ynew = (stime*yval + val*dt) / (stime+dt)
#         if abs(ynew) > yeps/10.0:
#             if abs((ynew-yval)/ynew) < yeps:
#                 done = True
#         else:
#             if abs(ynew-yval) < yeps:
#                 done = True
#         yval = ynew
#         curtime = psglobals.CurrentTime
#         stime = curtime - tbegin
#     return yval
    
# def ivcurve_point(x, yexpr, yeps, twait, tmin, tmax):
#     tbegin = psglobals.CurrentTime
#     while psglobals.CurrentTime - tbegin < twait:
#         TimeStep()
#     yval = yexpr.Value()
#     tbegin = psglobals.CurrentTime
#     done = False
#     npoints = 1
#     while psglobals.CurrentTime - tbegin < tmin or \
#         (psglobals.CurrentTime - tbegin < tmax and not done):
#         TimeStep()
#         val = yexpr.Value()
#         ynew = npoints*yval/(npoints+1) + val/(npoints+1)
#         if abs(ynew) > yeps/100.0:
#             if abs((ynew-yval)/ynew) < yeps:
#                 done = True
#         else:
#             if abs(ynew-yval) < yeps:
#                 done = True
#         yval = ynew
#         npoints += 1
#     return yval

def ivcurve_point(x, yexpr, yeps, twait, tmin, tmax):
    tbegin = psglobals.CurrentTime
    while psglobals.CurrentTime - tbegin < twait:
        TimeStep()
    
    aver = Average(psglobals.CurrentTime)
    tbegin = psglobals.CurrentTime
    done = False
    yaver_old = 0.0
    while psglobals.CurrentTime - tbegin < tmin or \
        (psglobals.CurrentTime - tbegin < tmax and not done):
        TimeStep()
        yaver = aver.step(psglobals.CurrentTime, yexpr.Value())
        if abs(yaver) < yeps:
            if abs(yaver - yaver_old) < yeps:
                done = True
        else:
            if abs((yaver - yaver_old)/yaver) < yeps:
                done = True
        yaver_old = yaver

    return yaver



def ivcurve(xpar, xmin, xmax, dx, xback, yexpr, yeps, twait, tmin, tmax):
    """
    calculate iv-curve
    --------------------------
    xpar - parameter to change. Can be found by function find(). For example find('.i1', 'p')
    xmin - minimal value of parameter
    xmax - maximal value of parameter
    dx - step for parameter's changes
    xback - if False parameter will be changed from xmin to xmax.
            if True parameter will be changed from xmin to xmax and when back to xmin
    yexpr - expression to average over the time for each value of parameter xpar
            Can be calculated by function create_expression(). For example create_expression("v(j1)", ".")
    yeps - relative accuracy to average expression yexpr over time
    twait - wait time between changing parameter xpar and starting averaging expression yexpr
    tmin - minimal time to average expression yexpr
    tmax - maximal time to average expression yexpr. Even if accuracy is less than yeps 
            averaging will stop
            
    Function returns list of [x,y] pairs
    """
    result = []
    # Turn off change parameters optimization 
    psglobals.OptimizeSimulation = False
    # Set max time step to 1.0
    SimulationParameters.DTMax = 1.0
    logger = logging.getLogger('pscan2')
    InitSimulation()
    x = xmin
    while x <= xmax:
        xpar.SetValueSmooth(x)
        y = ivcurve_point(x, yexpr, yeps, twait, tmin, tmax)
        logger.debug(str(x) + " " + str(y))
        result.append([x, y])
        x += dx
        
    if xback:
        logger.debug("Going back")
        x -= dx
        while x >= xmin:
            x -= dx
            xpar.SetValueSmooth(x)
            y = ivcurve_point(x, yexpr, yeps, twait, tmin, tmax)
            logger.debug(str(x) + " " + str(y))
            result.append([x, y])
            
    return result
