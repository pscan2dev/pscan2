from KLUSolver import KLUSolver
from array import array
import numpy

n = 5
Ap = array('i', [0, 2, 5, 9, 10, 12])
Ai = array('i', [0,  1,  0,   2,  4,  1,  2,   3,  4,  2,  1,  4])
Ax = array('d', [2., 3., 3., -1., 4., 4., -3., 1., 2., 2., 6., 1.])
Adense = numpy.array([
	[2, 3,  0,  0, 0],
	[3, 0,  4,  0, 6],
	[0, -1, -3, 2, 0],
	[0, 0,  1,  0, 0],
	[0, 4,  2,  0, 1]], dtype = numpy.float64)

b1 = array('d', [8., 45., -3., 3., 19.])
b2 = numpy.array([8., 45., -3., 3., 19.], dtype = numpy.float64)

ks = KLUSolver()
ks.analyze(n, Ap, Ai)
ks.solve(Ax, b1)
print("Result: " + str(b1))

ks.solveMatrix(Adense, b2)
print("Result2: " + str(b2))