# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import sys, re
from .expr_parser import expr_parser
from .ReadSpice import substitute_model
from . import pscan_lex
from . import psconfig

GLOBAL_PARAMETERS = set(['tcurr', 'xl', 'xj', 'xi', 'xr'])

def InsertFile(fout, fname):
	try:
		f = open(fname, "r")
	except:
		return
	line = f.readline()
	while line != '':
		fout.write(line)
		line = f.readline()
	f.close()


def LoadParamFile(circuit_name):
	result = {}
	try:
		f = open(circuit_name + ".par", "r")
	except:
		return result
	line = f.readline().lower()
	while line != '':
		if line[0] != '#':
			lst = line.split()
			if len(lst) >= 2:
				result[lst[0]] = float(lst[1])
		line = f.readline().lower()
	f.close()
	return result
	

def GenerateHDLFile(circuit_name, param_set, ext_set, is_root):
	fout = open(circuit_name + ".hdl", "w")

	if is_root:
		# For root circuit first insert content of global.thdl file
		InsertFile(fout, "global.thdl")

	param_values = LoadParamFile(circuit_name)
	
	print("CIRCUIT {}()\n{{".format(circuit_name), file=fout)
	if len(param_set) > 0:
		print("PARAMETER", file=fout)
		plst = list(param_set)
		plst.sort()
		for param in plst[:-1]:
			if param in param_values:
				val = param_values[param]
			else:
				val = 1.0
				print("Circuit {}: cannot find value for parameter {}".format(circuit_name, param), file=sys.stderr)
			print("\t{}={},".format(param, val), file=fout)
		param = plst[-1]
		if param in param_values:
			val = param_values[param]
		else:
			val = 1.0
			print("Circuit {}: cannot find value for parameter {}".format(circuit_name, param), file=sys.stderr)
		print("\t{}={};".format(param, val), file=fout)

	if len(ext_set) > 0:
		print("EXTERNAL", file=fout)
		plst = list(ext_set)
		plst.sort()
		for param in plst[:-1]:
			print("\t{}=1.0,".format(param), file=fout)
		print("\t{}=1.0;".format(plst[-1]), file=fout)

	# Insert content of template file
	InsertFile(fout, circuit_name + ".thdl")
	print("}", file=fout)

def GetModelParameters(elem, model, param_set, ext_set):
	# Ignore phase and voltage sources. They usially use internal parameters
	if elem[0] == 'p' or elem[0] == 'v':
		return

	new_model = substitute_model(elem, model.lower(), psconfig.model_replace_map)
	model_expr = expr_parser.parse(new_model, lexer=pscan_lex.lexer)
	pset = model_expr.GetIdentSet()
	for param in pset:
		if param not in GLOBAL_PARAMETERS:
			if param == 'x'+elem:
				ext_set.add(param)
			else:
				param_set.add(param)


def ProcessSpiceCircuit(fin, nline, sname, is_root = False):
	param_set = set()
	ext_set = set()
	nline2 = nline
	line = fin.readline().lower()
	nline2 += 1
	while line != '' and line[:5] != '.ends' and line[:4] != '.end':
		line = line[:-1]
		#print("Line:" + line)
		if line[0] == '*' or line[0] == '\n' or line[:5] == "0 0 0":
			line = fin.readline().lower()
			nline += 1
			continue
		if line[:7] == '.subckt':
			lst = line.split()
			if len(lst) > 2:
				ProcessSpiceCircuit(fin, nline2, lst[1])
		else:
			lst = line.split()
			elem = lst[0]
			if elem[0] == 'x':
				pass
			elif elem[0] == 'm':
				# Mutual inductance connected to one node
				model = ' '.join(lst[2:])
				param_set.add(elem)
				ext_set.add('x'+elem)
			else:
				# Other elements connected to two nodes
				model = ' '.join(lst[3:])
				GetModelParameters(elem, model, param_set, ext_set)
		line = fin.readline().lower()
		nline += 1
	GenerateHDLFile(sname, param_set, ext_set, is_root)
	

def ProcessSpice(circuit_name, fname):
	print("Load circuit {} from the netlist file {}".format(circuit_name, fname))
	fin = open(fname, 'r')
	ProcessSpiceCircuit(fin, 1, circuit_name, True)
