﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import math
from . import SimulationParameters as SP
from . import psglobals
from . import Simulation
from .Expression import smooth_step

# Admitance0 - no dependency from time, phase
# Admitance1 - dependent on time
# Admitance2 - dependent on phase
# RHSCurrent - RHS of node equations. Equal to negative current through element

def NQuanta(phase, oldn):
	x = phase / (2.0 * math.pi)
	if x - 0.5 - math.floor(x) > SP.PHASE_HISTERESIS:
		return((oldn, False, False))
	else:
		nq = math.floor(x + 0.5)
		if x < 0:
			nq = -nq

		if oldn < nq:
			return((nq, True, False))
		elif oldn > nq:
			return((nq, False, True))
		else:
			return((nq, False, False))


class InductorModel:
	def __init__(self, i, j):
		self.adm = 1.0
		self.i = i
		self.j = j

	def SetParameters(self, params):
		self.adm = 1.0/params[0]
		
	def Admitance0(self, matrix):
		if self.i == 0:
			matrix[self.j-1,self.j-1] += self.adm
		elif self.j == 0:
			matrix[self.i-1,self.i-1] += self.adm
		else:
			matrix[self.i-1,self.i-1] += self.adm
			matrix[self.i-1,self.j-1] -= self.adm
			matrix[self.j-1,self.i-1] -= self.adm
			matrix[self.j-1,self.j-1] += self.adm
	
	def Admitance1(self, matrix):
		pass
		
	def Admitance2(self, matrix):
		pass
		
	def RHSCurrent(self, vec):
		phases = SP.X[SP.Xind]

		if self.i == 0:
			vec[self.j-1] -= phases[self.j-1] * self.adm
		elif self.j == 0:
			vec[self.i-1] -= phases[self.i-1] * self.adm
		else:
			vec[self.i-1] -= (phases[self.i-1] - phases[self.j-1]) * self.adm
			vec[self.j-1] += (phases[self.i-1] - phases[self.j-1]) * self.adm

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage

		if self.i == 0:
			psglobals.ElementCurrent[eindex] = -phases[self.j-1] * self.adm
			psglobals.ElementPhase[eindex] = -phases[self.j-1]
			psglobals.ElementVoltage[eindex] = -voltages[self.j-1]
		elif self.j == 0:
			psglobals.ElementCurrent[eindex] = phases[self.i-1] * self.adm
			psglobals.ElementPhase[eindex] = phases[self.i-1]
			psglobals.ElementVoltage[eindex] = voltages[self.i-1]
		else:
			psglobals.ElementCurrent[eindex] = (phases[self.i-1] - phases[self.j-1]) * self.adm
			psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
			psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])

	
		
class ResistorModel:
	def __init__(self, i, j):
		self.res = 1.0
		self.i = i
		self.j = j
		
	def SetParameters(self, params):
		self.res = params[0]

	def Admitance0(self, matrix):
		pass
	
	def Admitance1(self, matrix):
		adm = SP.InterpCoeffDt[0]/self.res
		if self.i == 0:
			matrix[self.j-1,self.j-1] += adm
		elif self.j == 0:
			matrix[self.i-1,self.i-1] += adm
		else:
			matrix[self.i-1,self.i-1] += adm
			matrix[self.i-1,self.j-1] -= adm
			matrix[self.j-1,self.i-1] -= adm
			matrix[self.j-1,self.j-1] += adm
		
	def Admitance2(self, matrix):
		pass
		
	def RHSCurrent(self, vec):
		voltages = SP.DX
		if self.i == 0:
			vec[self.j-1] -= voltages[self.j-1]/self.res
		elif self.j == 0:
			vec[self.i-1] -= voltages[self.i-1]/self.res
		else:
			vec[self.i-1] -= (voltages[self.i-1] - voltages[self.j-1])/self.res
			vec[self.j-1] += (voltages[self.i-1] - voltages[self.j-1])/self.res

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage

		if self.i == 0:
			psglobals.ElementCurrent[eindex] = -voltages[self.j-1]/self.res
			psglobals.ElementPhase[eindex] = -phases[self.j-1]
			psglobals.ElementVoltage[eindex] = -voltages[self.j-1]
		elif self.j == 0:
			psglobals.ElementCurrent[eindex] = voltages[self.i-1]/self.res
			psglobals.ElementPhase[eindex] = phases[self.i-1]
			psglobals.ElementVoltage[eindex] = voltages[self.i-1]
		else:
			psglobals.ElementCurrent[eindex] = (voltages[self.i-1] - voltages[self.j-1])/self.res
			psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
			psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])

		
class CapacitorModel:
	def __init__(self, i, j):
		self.cap = 1.0
		self.i = i
		self.j = j
		
	def SetParameters(self, params):
		self.cap = params[0]

	def Admitance0(self, matrix):
		pass
	
	def Admitance1(self, matrix):
		adm = SP.InterpCoeffDDt[0] * self.cap
		
		if self.i == 0:
			matrix[self.j-1,self.j-1] += adm
		elif self.j == 0:
			matrix[self.i-1,self.i-1] += adm
		else:
			matrix[self.i-1,self.i-1] += adm
			matrix[self.i-1,self.j-1] -= adm
			matrix[self.j-1,self.i-1] -= adm
			matrix[self.j-1,self.j-1] += adm
		
	def Admitance2(self, matrix):
		pass
		
	def RHSCurrent(self, vec):
		voltages_deriv = SP.DDX
		
		if self.i == 0:
			vec[self.j-1] -= voltages_deriv[self.j-1] * self.cap
		elif self.j == 0:
			vec[self.i-1] -= voltages_deriv[self.i-1] * self.cap
		else:
			vec[self.i-1] -= (voltages_deriv[self.i-1] - voltages_deriv[self.j-1]) * self.cap
			vec[self.j-1] += (voltages_deriv[self.i-1] - voltages_deriv[self.j-1]) * self.cap

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage
		dvoltages = psglobals.NodeDVoltage

		if self.i == 0:
			psglobals.ElementCurrent[eindex] = -dvoltages[self.j-1]*self.cap
			psglobals.ElementPhase[eindex] = -phases[self.j-1]
			psglobals.ElementVoltage[eindex] = -voltages[self.j-1]
		elif self.j == 0:
			psglobals.ElementCurrent[eindex] = dvoltages[self.i-1]*self.cap
			psglobals.ElementPhase[eindex] = phases[self.i-1]
			psglobals.ElementVoltage[eindex] = voltages[self.i-1]
		else:
			psglobals.ElementCurrent[eindex] = (dvoltages[self.i-1] - dvoltages[self.j-1])*self.cap
			psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
			psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])

		
class CurrentSourceModel:
	def __init__(self, i, j):
		self.curr = 0.0
		self.i = i
		self.j = j
		
	def SetParameters(self, params):
		self.curr = params[0]

	def Admitance0(self, matrix):
		pass
	
	def Admitance1(self, matrix):
		pass
		
	def Admitance2(self, matrix):
		pass
		
	def RHSCurrent(self, vec):
		# Smooth current values in the beginning of simulation
		if psglobals.CurrentTime < SP.INITIAL_RAMP:
			current = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)*self.curr
		else:
			current = self.curr

		if self.i == 0:
			vec[self.j-1] += current
		elif self.j == 0:
			vec[self.i-1] += current
		else:
			vec[self.i-1] += current
			vec[self.j-1] -= current

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage
		dvoltages = psglobals.NodeDVoltage

		# Smooth current values in the beginning of simulation
		if psglobals.CurrentTime < SP.INITIAL_RAMP:
			current = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)*self.curr
		else:
			current = self.curr

		if self.i == 0:
			psglobals.ElementCurrent[eindex] = -current
			psglobals.ElementPhase[eindex] = -phases[self.j-1]
			psglobals.ElementVoltage[eindex] = -voltages[self.j-1]
		elif self.j == 0:
			psglobals.ElementCurrent[eindex] = current
			psglobals.ElementPhase[eindex] = phases[self.i-1]
			psglobals.ElementVoltage[eindex] = voltages[self.i-1]
		else:
			psglobals.ElementCurrent[eindex] = current
			psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
			psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])


class PhaseSourceModel:
	Linternal = 0.2
	def __init__(self, i, j):
		self.phase = 0.0
		self.i = i
		self.j = j
		
	def SetParameters(self, params):
		self.phase = params[0]

	def Admitance0(self, matrix):
		if self.i == 0:
			matrix[self.j-1,self.j-1] += 1.0/PhaseSourceModel.Linternal
		elif self.j == 0:
			matrix[self.i-1,self.i-1] += 1.0/PhaseSourceModel.Linternal
		else:
			matrix[self.i-1,self.i-1] += 1.0/PhaseSourceModel.Linternal
			matrix[self.i-1,self.j-1] -= 1.0/PhaseSourceModel.Linternal
			matrix[self.j-1,self.i-1] -= 1.0/PhaseSourceModel.Linternal
			matrix[self.j-1,self.j-1] += 1.0/PhaseSourceModel.Linternal
	
	def Admitance1(self, matrix):
		pass
		
	def Admitance2(self, matrix):
		pass
		
	def RHSCurrent(self, vec):
		phases = SP.X[SP.Xind]
		full_current = self.phase / PhaseSourceModel.Linternal
		# Smooth current values in the beginning of simulation
		current = SP.InitialSmoothFactor*full_current
		
		if self.i == 0:
			vec[self.j-1] += -current - phases[self.j-1] / PhaseSourceModel.Linternal
		elif self.j == 0:
			vec[self.i-1] += current - phases[self.i-1] / PhaseSourceModel.Linternal
		else:
			vec[self.i-1] -= -current + (phases[self.i-1] - phases[self.j-1]) / PhaseSourceModel.Linternal
			vec[self.j-1] += -current + (phases[self.i-1] - phases[self.j-1]) / PhaseSourceModel.Linternal

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage

		full_current = self.phase / PhaseSourceModel.Linternal
		# Smooth current values in the beginning of simulation
		if psglobals.CurrentTime < SP.INITIAL_RAMP:
			current = SP.InitialSmoothFactor*full_current
		else:
			current = full_current

		if self.i == 0:
			psglobals.ElementCurrent[eindex] = -current - phases[self.j-1] / PhaseSourceModel.Linternal
			psglobals.ElementPhase[eindex] = -phases[self.j-1]
			psglobals.ElementVoltage[eindex] = -voltages[self.j-1]
		elif self.j == 0:
			psglobals.ElementCurrent[eindex] = -current + phases[self.i-1] / PhaseSourceModel.Linternal
			psglobals.ElementPhase[eindex] = phases[self.i-1]
			psglobals.ElementVoltage[eindex] = voltages[self.i-1]
		else:
			psglobals.ElementCurrent[eindex] = -current + (phases[self.i-1] - phases[self.j-1]) / PhaseSourceModel.Linternal
			psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
			psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])

		(nq, inc_flag, dec_flag) = NQuanta(psglobals.ElementPhase[eindex], 
									 psglobals.ElementN[eindex])
		psglobals.ElementN[eindex] = nq
		psglobals.ElementInc[eindex] = inc_flag
		psglobals.ElementDec[eindex] = dec_flag


class VoltageSourceModel:
	Rinternal = 0.2
	def __init__(self, i, j):
		self.voltage = 0.0
		self.i = i
		self.j = j
		
	def SetParameters(self, params):
		self.voltage = params[0]

	def Admitance0(self, matrix):
		pass
	
	def Admitance1(self, matrix):
		adm = SP.InterpCoeffDt[0] / VoltageSourceModel.Rinternal
		if self.i == 0:
			matrix[self.j-1,self.j-1] += adm
		elif self.j == 0:
			matrix[self.i-1,self.i-1] += adm
		else:
			matrix[self.i-1,self.i-1] += adm
			matrix[self.i-1,self.j-1] -= adm
			matrix[self.j-1,self.i-1] -= adm
			matrix[self.j-1,self.j-1] += adm
		
	def Admitance2(self, matrix):
		pass
		
	def RHSCurrent(self, vec):
		voltages = SP.DX
		full_current = self.voltage / VoltageSourceModel.Rinternal
		# Smooth current values in the beginning of simulation
		current = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)*full_current

		if self.i == 0:
			vec[self.j-1] += -current - voltages[self.j-1] / PhaseSourceModel.Rinternal
		elif self.j == 0:
			vec[self.i-1] += current - voltages[self.i-1] / PhaseSourceModel.Rinternal
		else:
			vec[self.i-1] -= -current + (voltages[self.i-1] - voltages[self.j-1]) / PhaseSourceModel.Rinternal
			vec[self.j-1] += -current + (voltages[self.i-1] - voltages[self.j-1]) / PhaseSourceModel.Rinternal

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage

		full_current = self.voltage / VoltageSourceModel.Rinternal
		# Smooth current values in the beginning of simulation
		current = smooth_step(psglobals.CurrentTime/SP.INITIAL_RAMP)*full_current

		if self.i == 0:
			psglobals.ElementCurrent[eindex] = -current - voltages[self.j-1] / PhaseSourceModel.Rinternal
			psglobals.ElementPhase[eindex] = -phases[self.j-1]
			psglobals.ElementVoltage[eindex] = -voltages[self.j-1]
		elif self.j == 0:
			psglobals.ElementCurrent[eindex] = current - voltages[self.i-1] / PhaseSourceModel.Rinternal
			psglobals.ElementPhase[eindex] = phases[self.i-1]
			psglobals.ElementVoltage[eindex] = voltages[self.i-1]
		else:
			psglobals.ElementCurrent[eindex] = -current + (voltages[self.i-1] - voltages[self.j-1]) / PhaseSourceModel.Rinternal
			psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
			psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])

		(nq, inc_flag, dec_flag) = NQuanta(psglobals.ElementPhase[eindex], 
									 psglobals.ElementN[eindex])
		psglobals.ElementN[eindex] = nq
		psglobals.ElementInc[eindex] = inc_flag
		psglobals.ElementDec[eindex] = dec_flag

class JunctionModel:
	def __init__(self, i, j):
		self.crit_current = 0.0
		self.resistance = 1.0
		self.capacitance = 1.0
		self.i = i
		self.j = j

	def SetParameters(self, params):
		self.crit_current = params[0]
		self.resistance = params[1]
		self.capacitance = params[2]
		
	def Admitance0(self, matrix):
		pass
	
	def Admitance1(self, matrix):
		adm = SP.InterpCoeffDt[0]/self.resistance + SP.InterpCoeffDDt[0]*self.capacitance
		
		if self.i == 0:
			matrix[self.j-1,self.j-1] += adm
		elif self.j == 0:
			matrix[self.i-1,self.i-1] += adm
		else:
			matrix[self.i-1,self.i-1] += adm
			matrix[self.i-1,self.j-1] -= adm
			matrix[self.j-1,self.i-1] -= adm
			matrix[self.j-1,self.j-1] += adm
		
	def Admitance2(self, matrix):
		phases = SP.X[SP.Xind]
		
		if self.i == 0:
			matrix[self.j-1,self.j-1] += self.crit_current * math.cos(phases[self.j-1])
		elif self.j == 0:
			matrix[self.i-1,self.i-1] += self.crit_current * math.cos(phases[self.i-1])
		else:
			adm = self.crit_current * math.cos(phases[self.i-1]-phases[self.j-1])
			matrix[self.i-1,self.i-1] += adm
			matrix[self.i-1,self.j-1] -= adm
			matrix[self.j-1,self.i-1] -= adm
			matrix[self.j-1,self.j-1] += adm
		
	def RHSCurrent(self, vec):
		phases = SP.X[SP.Xind]
		voltages = SP.DX
		voltages_deriv = SP.DDX
		
		if self.i == 0:
			dphase = phases[self.j-1]
			dvoltage = voltages[self.j-1]
			ddvoltage = voltages_deriv[self.j-1]
			vec[self.j-1] -= self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
		elif self.j == 0:
			dphase = phases[self.i-1]
			dvoltage = voltages[self.i-1]
			ddvoltage = voltages_deriv[self.i-1]
			vec[self.i-1] -= self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
		else:
			dphase = phases[self.i-1] - phases[self.j-1]
			dvoltage = voltages[self.i-1] - voltages[self.j-1]
			ddvoltage = voltages_deriv[self.i-1] - voltages_deriv[self.j-1]
			curr = self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
			vec[self.i-1] -= curr
			vec[self.j-1] += curr

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage
		dvoltages = psglobals.NodeDVoltage

		if self.i == 0:
			psglobals.ElementCurrent[eindex] = self.crit_current * math.sin(phases[self.j-1]) + \
				 voltages[self.j-1] / self.resistance + dvoltages[self.j-1] * self.capacitance
			psglobals.ElementPhase[eindex] = phases[self.j-1]
			psglobals.ElementVoltage[eindex] = voltages[self.j-1]
		elif self.j == 0:
			psglobals.ElementCurrent[eindex] = self.crit_current * math.sin(phases[self.i-1]) + \
				 voltages[self.i-1] / self.resistance + dvoltages[self.i-1] * self.capacitance
			psglobals.ElementPhase[eindex] = phases[self.i-1]
			psglobals.ElementVoltage[eindex] = voltages[self.i-1]
		else:
			psglobals.ElementCurrent[eindex] = self.crit_current * math.sin(phases[self.i-1] - phases[self.j-1]) + \
				(voltages[self.i-1] - voltages[self.j-1]) / self.resistance + \
				(dvoltages[self.i-1] - dvoltages[self.j-1]) * self.capacitance
			psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
			psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])

		(nq, inc_flag, dec_flag) = NQuanta(psglobals.ElementPhase[eindex], 
									 psglobals.ElementN[eindex])
		psglobals.ElementN[eindex] = nq
		psglobals.ElementInc[eindex] = inc_flag
		psglobals.ElementDec[eindex] = dec_flag

# For transformer we assume that it is not connected to ground
# params[0] = L1 / (L1 * L2 - K * K)
# params[1] = L2 / (L1 * L2 - K * K)
# params[2] = K / (L1 * L2 - K * K)
class TransformerModel:
	def __init__(self, i, j, k, l):
		self.i = i
		self.j = j
		self.k = k
		self.l = l
		pass
		
	def Admitance0(self, matrix):			
		matrix[self.i-1,self.i-1] += params[1]
		matrix[self.j-1,self.i-1] -= params[1]
		matrix[self.j-1,self.j-1] += params[1]
		matrix[self.k-1,self.i-1] += params[2]
		matrix[self.i-1,self.k-1] += params[2]
		matrix[self.k-1,self.j-1] -= params[2]
		matrix[self.j-1,self.k-1] -= params[2]
		matrix[self.k-1,self.k-1] += params[0]
		matrix[self.l-1,self.i-1] -= params[2]
		matrix[self.i-1,self.l-1] -= params[2]
		matrix[self.l-1,self.j-1] += params[2]
		matrix[self.j-1,self.l-1] += params[2]
		matrix[self.l-1,self.k-1] -= params[0]
		matrix[self.l-1,self.l-1] += params[0]	
	
	def Admitance1(self, matrix):
		pass
		
	def Admitance2(self, matrix):
		pass
		
	def RHSCurrent(self, vec):
		phases = SP.X[SP.Xind]
		curr_ij = (phases[self.i-1]-phases[self.j-1])*params[1] + (phases[self.k-1]-phases[self.l-1])*params[2]
		curr_kl = (phases[self.k-1]-phases[self.l-1])*params[0] + (phases[self.i-1]-phases[self.j-1])*params[2]

		vec[self.i-1] -= curr_ij
		vec[self.j-1] += curr_ij
		vec[self.k-1] -= curr_kl
		vec[self.l-1] += curr_kl

	def Values(self, eindex):
		phases = psglobals.NodePhase
		voltages = psglobals.NodeVoltage

		curr_ij = (phases[self.i-1]-phases[self.j-1])*params[1] + (phases[self.k-1]-phases[self.l-1])*params[2]
		curr_kl = (phases[self.k-1]-phases[self.l-1])*params[0] + (phases[self.i-1]-phases[self.j-1])*params[2]
		
		psglobals.ElementCurrent[eindex] = curr_ij
		psglobals.ElementCurrent[eindex+1] = curr_kl
		psglobals.ElementPhase[eindex] = (phases[self.i-1] - phases[self.j-1])
		psglobals.ElementPhase[eindex+1] = (phases[self.k-1] - phases[self.l-1])
		psglobals.ElementVoltage[eindex] = (voltages[self.i-1] - voltages[self.j-1])
		psglobals.ElementVoltage[eindex+1] = (voltages[self.k-1] - voltages[self.l-1])


def BuildStaticMatrix(A0):
	for elem in psglobals.ElementList:
		if psglobals.OptimizeSimulation and psglobals.CurrentTime > SP.INITIAL_RAMP:
			if elem.etype == 'p' or elem.etype == 'v':
				elem.UpdateParameters()
		else:
			elem.UpdateParameters()

		elem.model.Admitance0(A0)

def BuildMatrix(A1, A2, B):
	for elem in psglobals.ElementList:
		if psglobals.OptimizeSimulation and psglobals.CurrentTime > SP.INITIAL_RAMP:
			if elem.etype == 'p' or elem.etype == 'v':
				elem.UpdateParameters()
		else:
			elem.UpdateParameters()
	
		elem.model.Admitance1(A1)
		elem.model.Admitance2(A2)
		elem.model.RHSCurrent(B)

def UpdateMatrix(A2, B):
	for elem in psglobals.ElementList:
		elem.model.Admitance2(A2)
		elem.model.RHSCurrent(B)

def UpdateRHS(B):
	for elem in psglobals.ElementList:
		elem.model.RHSCurrent(B)

def UpdateValues():
	for elem in psglobals.ElementList:
		elem.model.Values(elem.ValueIndex())
