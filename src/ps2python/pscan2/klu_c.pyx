# cython: profile=False
# cython: boundscheck=False
# cython: wraparound=False

import numpy as np
# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution).
cimport numpy as np

import cython
from cpython cimport array
from cpython cimport bool
import array

cdef extern from "klu.h":
    ctypedef struct klu_symbolic:
        pass
	ctypedef struct klu_numeric:
        pass
	ctypedef struct klu_common:
        pass
	
	int klu_defaults(klu_common *Common)
	klu_symbolic *klu_analyze(int n, int Ap [ ], int Ai [ ], klu_common *Common)
	
		
	

 