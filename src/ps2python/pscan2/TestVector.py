# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import math, array, logging
from . import psglobals
from .Expression import smooth_step

class TestVector:
    @staticmethod
    def printAll():
        logger = logging.getLogger('pscan2')

        names = list(psglobals.TestVectorsMap.keys())
        names.sort()
        for name in names:
            logger.debug(str(psglobals.TestVectorsMap[name]))

    def __init__(self, name, vec, tunit = 20.0, period = 0, repeat = 0, start = 20.0, pulse_duration = 5.0):
        self.name = name
        self.tunit = tunit
        self.start = start
        self.period = period
        self.repeat = repeat
        self.pulse_duration = pulse_duration

        if isinstance(vec, str):
            self.FromString(vec)
        elif isinstance(vec, list):   
            self.vector = array.array('I', vec)
        elif isinstance(vec, array.array):
            self.vector = array.array('I', vec)
        else:
            raise("Invalid vector specification" + str(vec))

        self.calculateNVector()

        if self.name != "":
            psglobals.TestVectorsMap[self.name] = self

    def setName(self, name):
        if self.name in psglobals.TestVectorsMap:
            del psglobals.TestVectorsMap[self.name]

        self.name = name
        if name != None and name != "":
            psglobals.TestVectorsMap[self.name] = self        

    def setPeriod(self, period):
        self.period = period
        self.calculateNVector()

    def FromString(self, vec_str):
        lst = []
        for ch in vec_str:
            if ch == '0':
                lst.append(0)
            elif ch == '1':
                lst.append(1)
        self.vector = array.array('I', lst)

    def calculateNVector(self):
        if self.period < len(self.vector):
            self.period = len(self.vector)
        elif self.period > len(self.vector):
            self.vector.extend([0]*(self.period - len(self.vector)))

        lst = []
        curn = 0
        for val in self.vector:
            if val == 1:
                curn += 1
            lst.append(curn)
        self.vectorn = array.array('I', lst)

    def __repr__(self):
        vec_str = ""
        for i in self.vector:
            vec_str += str(i)

        return "TestVector {}: {}, tu={}, per={}, r={}, s={}, pd={}".format(
            self.name, vec_str, self.tunit, self.period, self.repeat, self.start, self.pulse_duration)

    def __add__(self, other):
        new_vec =  self.vector + other.vector
        return TestVector("", new_vec, self.tunit, 0, self.repeat, self.start, self.pulse_duration)

    def __mul__(self, n):
        if not isinstance(n, int):
            raise("Unsupported multiplication by" + str(n))

        new_vec = array.array('I')
        for i in range(0, n):
            new_vec += self.vector

        return TestVector("", new_vec, self.tunit, 0, self.repeat, self.start, self.pulse_duration)

    def value(self, t):
        if t < self.start:
            return 0.0

        t -= self.start

        tper = self.tunit * self.period
        nper = int(t/tper)
        if self.repeat > 0 and nper >= self.repeat:
            return(2.0 * math.pi * self.repeat * self.vectorn[-1])
        
        t -= nper*tper
        offset = nper * self.vectorn[-1]

        n = int(t/self.tunit)
        
        if n < len(self.vector):
            if self.vector[n] == 1:
                t -= n * self.tunit
                if n > 0:
                    return(2.0 * math.pi * (offset + self.vectorn[n-1] + smooth_step(t/self.pulse_duration)))
                else:
                    return(2.0 * math.pi * (offset + smooth_step(t/self.pulse_duration)))
            else:
                return(2.0 * math.pi * (offset + self.vectorn[n]))
        else:
            return(2.0 * math.pi * (offset + self.vectorn[-1]))

        
if __name__ == '__main__':
    tv1 = TestVector("", "01001")
    print(tv1.vector, tv1.vectorn)

        
        
