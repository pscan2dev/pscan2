# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

TotalNodes = 0
TotalVirtualNodes = 0
TotalElements = 0
TotalSourceElements = 0
TotalNonSourceElements = 0
TotalElementValues= 0
TotalParameters = 0
TotalExternals = 0
TotalInternals = 0
TotalValues = 0
NodeMap = {}
ElementList = []
SourceElementList = []
NonSourceElementList = []
ElementArray = None
SourceElementArray = None
CircuitScriptMap = {}
ParametersDef = []
InternalsDef = []
CircuitNetlists = {}
DimUnits = {'mA':0.1, 'mV':0.67, 'Ohm':6.7, 'pH':3.29, 'ps':0.49, 'fF':73.3, 'fWb':0.329}
Par2Units = {'I':'mA', 'J':'mA', 'V':'mV', 'L':'pH', 'R':'Ohm', 'T':'ps', 'C':'fF', 'F':'fWb'}

RootCircuit = None

Parameters = {}
Internals = {}
InternalsList = []

TestVectorsMap = {}

CurrentTime = 0.0

OptimizeSimulation = True

# If not None, will be periodically called by simulate() function to update GUI
GUICallback = None
