﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

DTMax = 10.0
DTMin = 0.01
DTInit = 0.1
TimeStepEpsilon = 0.1
DXMax = 0.1

MaxPhaseChange = 0.628
MaxVoltageChange = 0.628

MaxNewtonIter = 5

Eps1 = 0.628
Eps2 = 0.1

RHSEps = 0.01

# Maximal order of interpolation polynomial. It can be unstable, if greater then 5
MAX_ORDER = 5
MIN_ORDER = 3

# Time to rump up sources values at the beginning of simulation
INITIAL_RAMP = 10.0

# Phase hysteresis used in N() function
PHASE_HISTERESIS = 2.0

# Variables used during simulation

LastTimeStep = 0.0
StepError = 0.0
StepPhaseChange  = 0.0
OrderSteps = 0

TimeStepReverted = False

InitialSmoothFactor = 0.0

# Keep last MAX_ORDER+1 time steps
TimeSteps = [0.0]*(MAX_ORDER+1)

InterpOrder = MIN_ORDER
InterpCoeff = [0.0]*(MAX_ORDER+1)
InterpCoeffDt = [0.0]*(MAX_ORDER+1)
InterpCoeffDDt = [0.0]*(MAX_ORDER+1)

NNZ = 0

Xind = 0
DXInd = 0

X = None
Xpredicted = None
DX = None
DXprev = None
DDX = None
A = None
A0 = None
A1 = None
A2 = None
B = None
B1 = None

Ai = None
Ap = None
SparseIndexMap = {}