﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import sys
from . import pscan_lex
import ply.yacc as yacc
from .ExpressionAST import *

start = 'file'

# Get the token map
tokens = pscan_lex.tokens

precedence =  [
	('right', 'EQUALS'), 
	('left', 'OR'), 
	('left', 'AND'), 
	('left', 'EQ', 'NE'), 
	('left', 'LE', 'GE', 'LT', 'GT'), 
	('left', 'PLUS', 'MINUS'), 
	('left', 'TIMES', 'DIVIDE'), 
	('right', 'UNISUB', 'NOT'),
	('right', 'POINT')]

def p_file(p):
	'''file : defs'''
	p[0] = p[1]

def p_defs_1(p):
	'''defs : def'''
	p[0] = [p[1]]

def p_defs_2(p):
	'''defs : defs def'''
	p[0] = p[1] + [p[2]]

def p_def_1(p):
	'''def : CIRCUIT IDENT LPAREN node_list RPAREN LBRACE circuit_defs_list RBRACE'''
	p[0] = ('CIRCUIT',p[2], p[4], p[7])

def p_def_2(p):
	'''def : PARAMETER vardecl_list SEMI'''
	p[0] = ('PARAMETER', p[2])

def p_def_3(p):
	'''def : INTERNAL vardecl_list SEMI'''
	p[0] = ('INTERNAL', p[2])

def p_circuit_defs_list_1(p):
	'''circuit_defs_list : circuit_def'''
	p[0] = [p[1]]

def p_circuit_defs_list_2(p):
	'''circuit_defs_list : circuit_defs_list circuit_def'''
	p[0] = p[1] + [p[2]]

def p_circuit_def_1(p):
	'''circuit_def : FREEZE var_list SEMI'''
	p[0] = ('FREEZE', p[2])

def p_circuit_def_2(p):
	'''circuit_def : PARAMETER vardecl_list SEMI'''
	p[0] = ('PARAMETER', p[2])

def p_circuit_def_3(p):
	'''circuit_def : EXTERNAL vardecl_list SEMI'''
	p[0] = ('EXTERNAL', p[2])

def p_circuit_def_4(p):
	'''circuit_def : INTERNAL vardecl_list SEMI'''
	p[0] = ('INTERNAL', p[2])

def p_circuit_def_5(p):
	'''circuit_def : VALUE vardecl_list SEMI'''
	p[0] = ('VALUE', p[2])

def p_circuit_def_6(p):
	'''circuit_def : RULE IDENT LPAREN expr RPAREN rule_list SEMI'''
	p[0] = ('RULE', p[2], p[4], p[6])

def p_node_list_1(p):
	'''node_list : '''
	p[0] = []

def p_node_list_2(p):
	'''node_list : node_item'''
	p[0] = [p[1]]

def p_node_list_3(p):
	'''node_list : node_list COMMA node_item'''
	p[0] = p[1] + [p[3]]

def p_node_item_1(p):
	'''node_item : IDENT'''
	p[0] = p[1]

def p_node_item_2(p):
	'''node_item : ICONST'''
	p[0] = p[1]
	
def p_var_list_1(p):
	'''var_list : '''
	p[0] = []

def p_var_list_2(p):
	'''var_list : IDENT'''
	p[0] = [p[1]]

def p_var_list_3(p):
	'''var_list : var_list COMMA IDENT'''
	p[0] = p[1] + [p[3]]

def p_vardecl_list_1(p):
	'''vardecl_list : '''
	p[0] = []

def p_vardecl_list_2(p):
	'''vardecl_list : vardecl'''
	p[0] = [p[1]]

def p_vardecl_list_3(p):
	'''vardecl_list : vardecl_list COMMA vardecl'''
	p[0] = p[1] + [p[3]]

def p_vardecl(p):
	'''vardecl : IDENT EQUALS expr'''
	p[0] = ('VARDECL', p[1], p[3])
	
def p_expr_list_1(p):
	'''expr_list : '''
	p[0] = []

def p_expr_list_2(p):
	'''expr_list : expr'''
	p[0] = [p[1]]

def p_expr_list_3(p):
	'''expr_list : expr_list COMMA expr'''
	p[0] = p[1] + [p[3]]

def p_rule_list_1(p):
	'''rule_list : expr'''
	p[0] = [p[1]]

def p_rule_list_2(p):
	'''rule_list : IDENT EQUALS expr'''
	p[0] = [ExpressionAST(ExpressionAST.EXP_TYPE_FCALL, 'assign', [ExpressionAST(ExpressionAST.EXP_TYPE_IDENT, p[1]), p[3]])]

def p_rule_list_3(p):
	'''rule_list : rule_list COMMA expr'''
	p[0] = p[1] + [p[3]]

def p_rule_list_4(p):
	'''rule_list : rule_list COMMA IDENT EQUALS expr'''
	p[0] = p[1] + [ExpressionAST(ExpressionAST.EXP_TYPE_FCALL, 'assign', [ExpressionAST(ExpressionAST.EXP_TYPE_IDENT, p[3]), p[5]])]

#### Arithmetic expressions

def p_expr_binary(p):
	'''expr : expr PLUS expr
            | expr MINUS expr
            | expr TIMES expr
            | expr DIVIDE expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_BINARY, p[2], p[1], p[3])

def p_expr_iconst(p):
	'''expr : ICONST'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_ICONST, int(p[1]))

def p_expr_fconst(p):
	'''expr : FCONST'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_FCONST, float(p[1]))

def p_expr_sconst(p):
	'''expr : SCONST'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_SCONST, p[1])

def p_expr_ident(p):
	'''expr : IDENT'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_IDENT, p[1])

def p_expr_ref(p):
	'''expr : expr POINT expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_POINTS, p[1], p[3])

def p_expr_fcall(p):
	'''expr : IDENT LPAREN expr_list RPAREN'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_FCALL, p[1], p[3])

def p_expr_freeze(p):
	'''expr : FREEZE LPAREN expr_list RPAREN'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_FCALL, 'freeze', p[3])


def p_expr_group(p):
	'''expr : LPAREN expr RPAREN'''
	p[0] = p[2]

def p_expr_sequence(p):
	'''expr : LBRACKET expr_list RBRACKET'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_SEQUENCE, p[2])

def p_expr_unary(p):
	'''expr : MINUS expr %prec UNISUB'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_UNARY, p[1], p[2])
	
#### Relational expressions

def p_expr_rel(p):
	'''expr : expr LT expr
               | expr LE expr
               | expr GT expr
               | expr GE expr
               | expr EQ expr
               | expr NE expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_BINARY, p[2], p[1], p[3])

#### Logical expressions

def p_expr_log(p):
	'''expr : expr OR expr
               | expr AND expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_BINARY, p[2], p[1], p[3])

def p_expr_not(p):
	'''expr : NOT expr'''
	p[0] = ExpressionAST(ExpressionAST.EXP_TYPE_UNARY, p[1], p[2])

def p_error(t):
	if t:
		raise Exception("Syntax error at '%s' %s:%d" % (t.value, parser.fname, t.lineno))
	else:
		raise Exception("Syntax error at %s EOF" % (parser.fname))

# optimize=1, load pscan_parsetab.py file
parser = yacc.yacc(debug=False, tabmodule="pscan_parsetab", optimize=True)
parser.fname = "<None>"

if __name__ == "__main__":
	inptext = r"""
PARAMETER XI=1.0, XJ=1;
CIRCUIT root(INP, OUT)
{
PARAMETER XJ1=1.0<2;
EXTERNAL J1=2.0, J2=3.0;
INTERNAL Q=J1<J2;
VALUE S=f(J1);
RULE R1(J1>4)
SET(M1.M3.2, 3),
INC(J1),
INC(J2);
}
"""
	res = parser.parse(inptext, lexer=pscan_lex.lexer)
	for elem in res:
		print(str(elem) + "\n")
