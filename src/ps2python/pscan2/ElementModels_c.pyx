# cython: boundscheck=False, wraparound=False, cdivision=True, language_level=3, linetrace=False, binding=False, profile=False

# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from cython.parallel import prange

import numpy as np
# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution).
cimport numpy as cnp

import cython
from cpython cimport PyObject
from libc.stdlib cimport malloc, free
#from libc.stdio cimport printf
from libc cimport math
#from cpython cimport array
#import array

# Maximal number of TJM model Dirichlet series
DEF MaxTJMCoeffArraySize = 20

cdef extern from "stdio.h":
    int printf(char *, ...) nogil

# Copy of libcpp.complex with correct operators declarations
cdef extern from "<complex>" namespace "std" nogil:
    cdef cppclass complex[T]:
        complex() except +
        complex(T, T) except +
        complex(complex[T]&) except +
        # How to make the converting constructor, i.e. convert complex[double]
        # to complex[float]?

        complex[T] operator+(complex[T]&)
        complex[T] operator+(T)
        complex[T] operator-(complex[T]&)
        complex[T] operator-(T)
        complex[T] operator*(complex[T]&)
        complex[T] operator*(T)
        complex[T] operator/(complex[T]&)
        complex[T] operator/(T)

        bint operator==(complex[T]&)
        bint operator==(T&)
        bint operator!=(complex[T]&)
        bint operator!=(T&)

        # Access real part
        T real()
        void real(T)

        # Access imaginary part
        T imag()
        void imag(T)

    # Return real part
    T real[T](complex[T]&)
    long double real(long double)
    double real(double)
    float real(float)

    # Return imaginary part
    T imag[T](complex[T]&)
    long double imag(long double)
    double imag(double)
    float imag(float)

    complex[double] exp(complex[double]&)
    complex[double] operator+(double, complex[double]&)
    complex[double] operator+(complex[double]&, double)
    complex[double] operator*(double, complex[double]&)
    complex[double] operator*(complex[double]&, double)
    complex[double] operator-(double, complex[double]&)
    complex[double] operator-(complex[double]&, double)
    
        
ctypedef complex[double] COMPLEX_t

from . import SimulationParameters as SP
from . import psglobals
from . import psconfig 

ctypedef cnp.float64_t FLOAT_t
ctypedef cnp.int32_t INT_t
ctypedef cnp.uint8_t UINT8_t
        
cdef inline void NQuanta(double phase, int* n, UINT8_t* inc, UINT8_t* dec, double histeresis) nogil:
    cdef double x
    cdef int nq
    cdef int oldn = n[0]
    
    x = math.fabs(phase) / (2.0 * math.M_PI)
    if x - 0.5 - math.floor(x) > histeresis:
        inc[0] = 0
        dec[0] = 0
    else:
        nq = int(math.floor(x + 0.5))
        if phase < 0:
            nq = -nq
        n[0] = nq
        
        if oldn < nq:
            inc[0] = 1
            dec[0] = 0
        elif oldn > nq:
            inc[0] = 0
            dec[0] = 1
        else:
            inc[0] = 0
            dec[0] = 0

            
ctypedef struct ModelContext:
    FLOAT_t coeff_dt 
    FLOAT_t coeff_ddt
    FLOAT_t smooth_factor
    FLOAT_t histeresis
    FLOAT_t time_step
    FLOAT_t* phases
    FLOAT_t* phases_prev
    FLOAT_t* voltages
    FLOAT_t* voltages_deriv
    FLOAT_t* voltages_prev

ctypedef ModelContext* pModelContext_t
            
cdef class ElementModel:
    def __init__(ElementModel self):
        pass
    cdef void Admitance0(self, FLOAT_t[:] matrix) nogil:
        pass
    cdef void Admitance1(self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
    cdef void Admitance2(self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
    cdef void RHSCurrent(self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        pass
    cdef void Values(self, ModelContext* ctx) nogil:
        pass


cdef class InductorModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef double admitance
    cdef int i, j, eindex, ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(InductorModel self, int i, int j, int eindex):
        self.admitance = 1.0
        self.i = i
        self.j = j
        self.eindex = eindex
    
    def InitModel(InductorModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data

    def SetParameters(InductorModel self, params):
        self.admitance = 1.0/params[0]
        
    cdef void Admitance0(InductorModel self, FLOAT_t[:] matrix) nogil:
        if self.i == 0:
            matrix[self.ind_jj] += self.admitance
        elif self.j == 0:
            matrix[self.ind_ii] += self.admitance
        else:
            matrix[self.ind_ii] += self.admitance
            matrix[self.ind_ij] -= self.admitance
            matrix[self.ind_ji] -= self.admitance
            matrix[self.ind_jj] += self.admitance
    
    cdef void Admitance1(InductorModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void Admitance2(InductorModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
    
    cdef void RHSCurrent(InductorModel self, FLOAT_t[:] vec, pModelContext_t ctx) nogil:
        if self.i == 0:
            vec[self.j-1] -= ctx.phases[self.j-1] * self.admitance
        elif self.j == 0:
            vec[self.i-1] -= ctx.phases[self.i-1] * self.admitance
        else:
            vec[self.i-1] -= (ctx.phases[self.i-1] - ctx.phases[self.j-1]) * self.admitance
            vec[self.j-1] += (ctx.phases[self.i-1] - ctx.phases[self.j-1]) * self.admitance

    cdef void Values(InductorModel self, ModelContext* ctx) nogil:
        if self.i == 0:
            self.elem_currents[self.eindex] = -self.node_phases[self.j-1] * self.admitance
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_currents[self.eindex] = self.node_phases[self.i-1] * self.admitance
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_currents[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1]) * self.admitance
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])
        #printf("Ind value: node [%d %d] index %d curr %lf\n", self.i, self.j, self.eindex, self.elem_currents[self.eindex])


cdef class InductorImpedanceModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef double impedance
    cdef int i, j, q, eindex
    cdef int ind_iq, ind_jq, ind_qi, ind_qj, ind_qq
    def __init__(InductorImpedanceModel self, int i, int j, int q, int eindex):
        self.impedance = 1.0
        self.i = i
        self.j = j
        self.q = q
        self.eindex = eindex
    
    def InitModel(InductorImpedanceModel self, ind_iq, ind_jq, ind_qi, ind_qj, ind_qq):
        self.ind_iq = ind_iq
        self.ind_jq = ind_jq
        self.ind_qi = ind_qi
        self.ind_qj = ind_qj
        self.ind_qq = ind_qq

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data

    def SetParameters(InductorImpedanceModel self, params):
        self.impedance = params[0]
        
    cdef void Admitance0(InductorImpedanceModel self, FLOAT_t[:] matrix) nogil:
        if self.i == 0:
            matrix[self.ind_jq] -= 1.0
            matrix[self.ind_qj] += 1.0
        elif self.j == 0:
            matrix[self.ind_iq] += 1.0
            matrix[self.ind_qi] -= 1.0
        else:
            matrix[self.ind_iq] += 1.0
            matrix[self.ind_jq] -= 1.0

            matrix[self.ind_qi] -= 1.0
            matrix[self.ind_qj] += 1.0

        matrix[self.ind_qq] += self.impedance
    
    cdef void Admitance1(InductorImpedanceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void Admitance2(InductorImpedanceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
    
    cdef void RHSCurrent(InductorImpedanceModel self, FLOAT_t[:] vec, pModelContext_t ctx) nogil:
        if self.i == 0:
            vec[self.j-1] += ctx.phases[self.q-1]
            vec[self.q-1] -= ctx.phases[self.j-1] + self.impedance * ctx.phases[self.q-1]
        elif self.j == 0:
            vec[self.i-1] -= ctx.phases[self.q-1]
            vec[self.q-1] -= -ctx.phases[self.i-1] + self.impedance * ctx.phases[self.q-1]
        else:
            vec[self.i-1] -= ctx.phases[self.q-1]
            vec[self.j-1] += ctx.phases[self.q-1]
            vec[self.q-1] -= -ctx.phases[self.i-1] + ctx.phases[self.j-1] + self.impedance * ctx.phases[self.q-1]

    cdef void Values(InductorImpedanceModel self, ModelContext* ctx) nogil:
        if self.i == 0:
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])
        self.elem_currents[self.eindex] = self.node_phases[self.q-1]
        #printf("Ind value: node [%d %d] index %d curr %lf\n", self.i, self.j, self.eindex, self.elem_currents[self.eindex])


            
cdef class ResistorModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef double resistance
    cdef int i, j, eindex
    cdef int ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(ResistorModel self, int i, int j, int eindex):
        self.resistance = 1.0
        self.i = i
        self.j = j
        self.eindex = eindex
    
    def InitModel(ResistorModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
    
    def SetParameters(ResistorModel self, params):
        self.resistance = params[0]
        
    cdef void Admitance0(ResistorModel self, FLOAT_t[:] matrix) nogil:
        pass
    
    cdef void Admitance1(ResistorModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm = ctx.coeff_dt/self.resistance
        if self.i == 0:
            matrix[self.ind_jj] += adm
        elif self.j == 0:
            matrix[self.ind_ii] += adm
        else:
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void Admitance2(ResistorModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void RHSCurrent(ResistorModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        if self.i == 0:
            vec[self.j-1] -= ctx.voltages[self.j-1]/self.resistance
        elif self.j == 0:
            vec[self.i-1] -= ctx.voltages[self.i-1]/self.resistance
        else:
            vec[self.i-1] -= (ctx.voltages[self.i-1] - ctx.voltages[self.j-1])/self.resistance
            vec[self.j-1] += (ctx.voltages[self.i-1] - ctx.voltages[self.j-1])/self.resistance

    cdef void Values(ResistorModel self, ModelContext* ctx) nogil:
        if self.i == 0:
            self.elem_currents[self.eindex] = -self.node_voltages[self.j-1]/self.resistance
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_currents[self.eindex] = self.node_voltages[self.i-1]/self.resistance
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_currents[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])/self.resistance
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        
cdef class CapacitorModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* node_dvoltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef FLOAT_t capacitance
    cdef int i, j, eindex
    cdef int ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(CapacitorModel self, int i, int j, int eindex):
        self.capacitance = 1.0
        self.i = i
        self.j = j
        self.eindex = eindex

    def InitModel(CapacitorModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_dvoltage = psglobals.NodeDVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.node_dvoltages = <double*>view_dvoltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
        
    def SetParameters(CapacitorModel self, params):
        self.capacitance = params[0]
        
    cdef void Admitance0(CapacitorModel self, FLOAT_t[:] matrix) nogil:
        pass
    
    cdef void Admitance1(CapacitorModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm = ctx.coeff_ddt * self.capacitance
        
        if self.i == 0:
            matrix[self.ind_jj] += adm
        elif self.j == 0:
            matrix[self.ind_ii] += adm
        else:
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void Admitance2(CapacitorModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void RHSCurrent(CapacitorModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        if self.i == 0:
            vec[self.j-1] -= ctx.voltages_deriv[self.j-1] * self.capacitance
        elif self.j == 0:
            vec[self.i-1] -= ctx.voltages_deriv[self.i-1] * self.capacitance
        else:
            vec[self.i-1] -= (ctx.voltages_deriv[self.i-1] - ctx.voltages_deriv[self.j-1]) * self.capacitance
            vec[self.j-1] += (ctx.voltages_deriv[self.i-1] - ctx.voltages_deriv[self.j-1]) * self.capacitance

    cdef void Values(CapacitorModel self, ModelContext* ctx) nogil:
        if self.i == 0:
            self.elem_currents[self.eindex] = -self.node_dvoltages[self.j-1] * self.capacitance
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_currents[self.eindex] = self.node_dvoltages[self.i-1] * self.capacitance
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_currents[self.eindex] = (self.node_dvoltages[self.i-1] - self.node_dvoltages[self.j-1]) * self.capacitance
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        
cdef class CurrentSourceModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef double current
    cdef int i, j, eindex
    cdef int ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(CurrentSourceModel self, int i, int j, int eindex):
        self.current = 0.0
        self.i = i
        self.j = j
        self.eindex = eindex

    def InitModel(CurrentSourceModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data

        
    def SetParameters(CurrentSourceModel self, params):
        self.current = params[0]

    cdef void Admitance0(CurrentSourceModel self, FLOAT_t[:] matrix) nogil:
        pass
    
    cdef void Admitance1(CurrentSourceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void Admitance2(CurrentSourceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void RHSCurrent(CurrentSourceModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        # Smooth current values in the beginning of simulation
        cdef double current = ctx.smooth_factor * self.current

        if self.i == 0:
            vec[self.j-1] += current
        elif self.j == 0:
            vec[self.i-1] += current
        else:
            vec[self.i-1] += current
            vec[self.j-1] -= current

    cdef void Values(CurrentSourceModel self, ModelContext* ctx)  nogil:
        # Smooth current values in the beginning of simulation
        cdef double current = ctx.smooth_factor*self.current

        if self.i == 0:
            self.elem_currents[self.eindex] = current
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_currents[self.eindex] = current
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_currents[self.eindex] = current
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])


cdef class PhaseSourceModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef int* elem_n
    cdef UINT8_t* elem_inc
    cdef UINT8_t* elem_dec
    cdef double Linternal
    cdef double phase
    cdef int i, j, q, eindex
    cdef int ind_iq, ind_jq, ind_qi, ind_qj
    def __init__(PhaseSourceModel self, int i, int j, int q, int eindex):
        self.phase = 0.0
        self.Linternal = 0.2
        self.i = i
        self.j = j
        self.q = q
        self.eindex = eindex

    def InitModel(PhaseSourceModel self, ind_iq, ind_jq, ind_qi, ind_qj):
        self.ind_iq = ind_iq
        self.ind_jq = ind_jq
        self.ind_qi = ind_qi
        self.ind_qj = ind_qj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        cdef cnp.ndarray[INT_t, ndim=1, mode="c"] view_en = psglobals.ElementN
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_einc = psglobals.ElementInc
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_edec = psglobals.ElementDec
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
        self.elem_n = <int*>view_en.data
        self.elem_inc = <UINT8_t*>view_einc.data
        self.elem_dec = <UINT8_t*>view_edec.data
        
    def SetParameters(PhaseSourceModel self, params):
        self.phase = params[0]
        
    cdef void Admitance0(PhaseSourceModel self, FLOAT_t[:] matrix) nogil:
        if self.i == 0:
            matrix[self.ind_jq] -= 1.0
            matrix[self.ind_qj] -= 1.0
        elif self.j == 0:
            matrix[self.ind_iq] += 1.0
            matrix[self.ind_qi] += 1.0
        else:
            matrix[self.ind_iq] += 1.0
            matrix[self.ind_qi] += 1.0
            matrix[self.ind_jq] -= 1.0
            matrix[self.ind_qj] -= 1.0
    
    cdef void Admitance1(PhaseSourceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void Admitance2(PhaseSourceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void RHSCurrent(PhaseSourceModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        # Smooth phase values in the beginning of simulation
        cdef double phase = ctx.smooth_factor*self.phase
        
        if self.i == 0:
            vec[self.j-1] += ctx.phases[self.q-1]
            vec[self.q-1] -= -ctx.phases[self.j-1] - phase
        elif self.j == 0:
            vec[self.i-1] -= ctx.phases[self.q-1]
            vec[self.q-1] -= ctx.phases[self.i-1] - phase
        else:
            vec[self.i-1] -= ctx.phases[self.q-1]
            vec[self.j-1] += ctx.phases[self.q-1]
            vec[self.q-1] -= ctx.phases[self.i-1] - ctx.phases[self.j-1] - phase

    cdef void Values(PhaseSourceModel self, ModelContext* ctx)  nogil:
        
        if self.i == 0:
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        self.elem_currents[self.eindex] = self.node_phases[self.q-1]

        NQuanta(self.elem_phases[self.eindex], 
            &self.elem_n[self.eindex], &self.elem_inc[self.eindex], 
            &self.elem_dec[self.eindex], ctx.histeresis)


cdef class VoltageSourceModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef int* elem_n
    cdef UINT8_t* elem_inc
    cdef UINT8_t* elem_dec
    cdef double Rinternal
    cdef double voltage
    cdef int i, j, q, eindex
    cdef int ind_iq, ind_jq, ind_qi, ind_qj
    def __init__(VoltageSourceModel self, int i, int j, int q, int eindex):
        self.voltage = 0.0
        self.i = i
        self.j = j
        self.q = q
        self.eindex = eindex

    def InitModel(VoltageSourceModel self, ind_iq, ind_jq, ind_qi, ind_qj):
        self.ind_iq = ind_iq
        self.ind_jq = ind_jq
        self.ind_qi = ind_qi
        self.ind_qj = ind_qj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        cdef cnp.ndarray[INT_t, ndim=1, mode="c"] view_en = psglobals.ElementN
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_einc = psglobals.ElementInc
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_edec = psglobals.ElementDec
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
        self.elem_n = <int*>view_en.data
        self.elem_inc = <UINT8_t*>view_einc.data
        self.elem_dec = <UINT8_t*>view_edec.data
        
    def SetParameters(VoltageSourceModel self, params):
        self.voltage = params[0]
        
    cdef void Admitance0(VoltageSourceModel self, FLOAT_t[:] matrix) nogil:
        pass
    
    cdef void Admitance1(VoltageSourceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        if self.i == 0:
            matrix[self.ind_jq] -= 1.0
            matrix[self.ind_qj] -= 1.0
        elif self.j == 0:
            matrix[self.ind_iq] += 1.0
            matrix[self.ind_qi] += 1.0
        else:
            matrix[self.ind_iq] += 1.0
            matrix[self.ind_qi] += 1.0
            matrix[self.ind_jq] -= 1.0
            matrix[self.ind_qj] -= 1.0
        
    cdef void Admitance2(VoltageSourceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void RHSCurrent(VoltageSourceModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        # Smooth values in the beginning of simulation
        cdef double voltage = ctx.smooth_factor*self.voltage

        if self.i == 0:
            vec[self.j-1] += ctx.phases[self.q-1]
            vec[self.q-1] -= (-ctx.voltages[self.j-1] - voltage) / ctx.coeff_dt
        elif self.j == 0:
            vec[self.i-1] -= ctx.phases[self.q-1]
            vec[self.q-1] -= (ctx.voltages[self.i-1] - voltage) / ctx.coeff_dt
        else:
            vec[self.i-1] -= ctx.phases[self.q-1]
            vec[self.j-1] += ctx.phases[self.q-1]
            vec[self.q-1] -= (ctx.voltages[self.i-1] - ctx.voltages[self.j-1] - voltage) / ctx.coeff_dt

    cdef void Values(VoltageSourceModel self, ModelContext* ctx) nogil:

        if self.i == 0:
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        self.elem_currents[self.eindex] = self.node_phases[self.q-1]

        NQuanta(self.elem_phases[self.eindex], 
            &self.elem_n[self.eindex], &self.elem_inc[self.eindex], 
            &self.elem_dec[self.eindex], ctx.histeresis)
        

cdef class JunctionModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* node_dvoltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef int* elem_n
    cdef UINT8_t* elem_inc
    cdef UINT8_t* elem_dec
    cdef double crit_current
    cdef double resistance
    cdef double capacitance
    cdef int i, j, eindex
    cdef int ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(JunctionModel self, int i, int j, int eindex):
        self.crit_current = 0.0
        self.resistance = 1.0
        self.capacitance = 1.0
        self.i = i
        self.j = j
        self.eindex = eindex
    
    def InitModel(JunctionModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_dvoltage = psglobals.NodeDVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        cdef cnp.ndarray[INT_t, ndim=1, mode="c"] view_en = psglobals.ElementN
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_einc = psglobals.ElementInc
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_edec = psglobals.ElementDec
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.node_dvoltages = <double*>view_dvoltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
        self.elem_n = <int*>view_en.data
        self.elem_inc = <UINT8_t*>view_einc.data
        self.elem_dec = <UINT8_t*>view_edec.data
    
    def SetParameters(JunctionModel self, params):
        self.crit_current = params[0]
        self.resistance = params[1]
        self.capacitance = params[2]
            
    cdef void Admitance0(JunctionModel self, FLOAT_t[:] matrix) nogil:
        pass
    
    cdef void Admitance1(JunctionModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm = ctx.coeff_dt/self.resistance + ctx.coeff_ddt*self.capacitance
        
        if self.i == 0:
            matrix[self.ind_jj] += adm
        elif self.j == 0:
            matrix[self.ind_ii] += adm
        else:
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void Admitance2(JunctionModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm
        
        if self.i == 0:
            matrix[self.ind_jj] += self.crit_current * math.cos(ctx.phases[self.j-1])
        elif self.j == 0:
            matrix[self.ind_ii] += self.crit_current * math.cos(ctx.phases[self.i-1])
        else:
            adm = self.crit_current * math.cos(ctx.phases[self.i-1]-ctx.phases[self.j-1])
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void RHSCurrent(JunctionModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        cdef double dphase
        cdef double dvoltage
        cdef double ddvoltage
        cdef double curr
        
        if self.i == 0:
            dphase = ctx.phases[self.j-1]
            dvoltage = ctx.voltages[self.j-1]
            ddvoltage = ctx.voltages_deriv[self.j-1]
            vec[self.j-1] -= self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
        elif self.j == 0:
            dphase = ctx.phases[self.i-1]
            dvoltage = ctx.voltages[self.i-1]
            ddvoltage = ctx.voltages_deriv[self.i-1]
            vec[self.i-1] -= self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
        else:
            dphase = ctx.phases[self.i-1] - ctx.phases[self.j-1]
            dvoltage = ctx.voltages[self.i-1] - ctx.voltages[self.j-1]
            ddvoltage = ctx.voltages_deriv[self.i-1] - ctx.voltages_deriv[self.j-1]
            curr = self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
            vec[self.i-1] -= curr
            vec[self.j-1] += curr

    cdef void Values(JunctionModel self, ModelContext* ctx) nogil:
        if self.i == 0:
            self.elem_currents[self.eindex] = -self.crit_current * math.sin(self.node_phases[self.j-1]) - \
                 self.node_voltages[self.j-1] / self.resistance - self.node_dvoltages[self.j-1] * self.capacitance
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_currents[self.eindex] = self.crit_current * math.sin(self.node_phases[self.i-1]) + \
                 self.node_voltages[self.i-1] / self.resistance + self.node_dvoltages[self.i-1] * self.capacitance
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_currents[self.eindex] = self.crit_current * math.sin(self.node_phases[self.i-1] - self.node_phases[self.j-1]) + \
                (self.node_voltages[self.i-1] - self.node_voltages[self.j-1]) / self.resistance + \
                (self.node_dvoltages[self.i-1] - self.node_dvoltages[self.j-1]) * self.capacitance
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        NQuanta(self.elem_phases[self.eindex], 
            &self.elem_n[self.eindex], &self.elem_inc[self.eindex], 
            &self.elem_dec[self.eindex], ctx.histeresis)

            
cdef class JunctionRSJNModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* node_dvoltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef int* elem_n
    cdef UINT8_t* elem_inc
    cdef UINT8_t* elem_dec
    
    cdef double crit_current
    cdef double resistance
    cdef double capacitance
    cdef double rsjn_vg
    cdef double rsjn_n
    
    cdef int i, j, eindex
    cdef int ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(JunctionRSJNModel self, int i, int j, int eindex):
        self.crit_current = 0.0
        self.resistance = 1.0
        self.capacitance = 1.0
        self.rsjn_vg = 4.3
        self.rsjn_n = 10.0
        self.i = i
        self.j = j
        self.eindex = eindex
    
    def InitModel(JunctionRSJNModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_dvoltage = psglobals.NodeDVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        cdef cnp.ndarray[INT_t, ndim=1, mode="c"] view_en = psglobals.ElementN
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_einc = psglobals.ElementInc
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_edec = psglobals.ElementDec
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.node_dvoltages = <double*>view_dvoltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
        self.elem_n = <int*>view_en.data
        self.elem_inc = <UINT8_t*>view_einc.data
        self.elem_dec = <UINT8_t*>view_edec.data
    
    def SetParameters(JunctionRSJNModel self, params):
        self.crit_current = params[0]
        self.resistance = params[1]
        self.rsjn_vg = params[2]
        self.rsjn_n = params[3]
        self.capacitance = params[4]
        
            
    cdef void Admitance0(JunctionRSJNModel self, FLOAT_t[:] matrix) nogil:
        pass
    
    cdef void Admitance1(JunctionRSJNModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm = ctx.coeff_ddt*self.capacitance
        
        if self.i == 0:
            matrix[self.ind_jj] += adm
        elif self.j == 0:
            matrix[self.ind_ii] += adm
        else:
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void Admitance2(JunctionRSJNModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm
        cdef double dphase
        cdef double dvoltage
        cdef double rel_voltage
        
        if self.i == 0:
            dphase = ctx.phases[self.j-1]
            dvoltage = ctx.voltages[self.j-1]   
        elif self.j == 0:
            dphase = ctx.phases[self.i-1]
            dvoltage = ctx.voltages[self.i-1]    
        else:
            dphase = ctx.phases[self.i-1] - ctx.phases[self.j-1]
            dvoltage = ctx.voltages[self.i-1] - ctx.voltages[self.j-1]    
            
        rel_voltage = math.pow(dvoltage/self.rsjn_vg, self.rsjn_n)
            
        adm = self.crit_current * math.cos(dphase) + ctx.coeff_dt * rel_voltage * (1.0+self.rsjn_n + rel_voltage) / \
            (self.resistance*(1+rel_voltage)*(1+rel_voltage))
        
        if self.i == 0:
            matrix[self.ind_jj] += adm
        elif self.j == 0:
            matrix[self.ind_ii] += adm
        else:
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void RHSCurrent(JunctionRSJNModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        cdef double dphase
        cdef double dvoltage
        cdef double rel_voltage
        cdef double ddvoltage
        cdef double curr
        
        if self.i == 0:
            dphase = ctx.phases[self.j-1]
            dvoltage = ctx.voltages[self.j-1]
            rel_voltage = math.pow(dvoltage/self.rsjn_vg, self.rsjn_n)
            ddvoltage = ctx.voltages_deriv[self.j-1]
            vec[self.j-1] -= self.crit_current * math.sin(dphase) + dvoltage * rel_voltage / \
                (self.resistance*(1.0 + rel_voltage)) + ddvoltage * self.capacitance
        elif self.j == 0:
            dphase = ctx.phases[self.i-1]
            dvoltage = ctx.voltages[self.i-1]
            rel_voltage = math.pow(dvoltage/self.rsjn_vg, self.rsjn_n)
            ddvoltage = ctx.voltages_deriv[self.i-1]
            vec[self.i-1] -= self.crit_current * math.sin(dphase) + dvoltage * rel_voltage / \
                (self.resistance*(1.0 + rel_voltage)) + ddvoltage * self.capacitance
        else:
            dphase = ctx.phases[self.i-1] - ctx.phases[self.j-1]
            dvoltage = ctx.voltages[self.i-1] - ctx.voltages[self.j-1]
            rel_voltage = math.pow(dvoltage/self.rsjn_vg, self.rsjn_n)
            ddvoltage = ctx.voltages_deriv[self.i-1] - ctx.voltages_deriv[self.j-1]
            curr = self.crit_current * math.sin(dphase) + dvoltage * rel_voltage / \
                (self.resistance*(1.0 + rel_voltage)) + ddvoltage * self.capacitance
            vec[self.i-1] -= curr
            vec[self.j-1] += curr

    cdef void Values(JunctionRSJNModel self, ModelContext* ctx) nogil:
        cdef double dvoltage
        cdef double rel_voltage
    
        if self.i == 0:
            dvoltage = self.node_voltages[self.j-1]
            rel_voltage = math.pow(dvoltage/self.rsjn_vg, self.rsjn_n)
        
            self.elem_currents[self.eindex] = -self.crit_current * math.sin(self.node_phases[self.j-1]) - \
                dvoltage * rel_voltage / (self.resistance*(1.0 + rel_voltage))  - self.node_dvoltages[self.j-1] * self.capacitance
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            dvoltage = self.node_voltages[self.i-1]
            rel_voltage = math.pow(dvoltage/self.rsjn_vg, self.rsjn_n)
        
            self.elem_currents[self.eindex] = self.crit_current * math.sin(self.node_phases[self.i-1]) + \
                dvoltage * rel_voltage / (self.resistance*(1.0 + rel_voltage)) + self.node_dvoltages[self.i-1] * self.capacitance
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            dvoltage = self.node_voltages[self.i-1] - self.node_voltages[self.j-1]
            rel_voltage = math.pow(dvoltage/self.rsjn_vg, self.rsjn_n)
            
            self.elem_currents[self.eindex] = self.crit_current * math.sin(self.node_phases[self.i-1] - self.node_phases[self.j-1]) + \
                dvoltage * rel_voltage / (self.resistance*(1.0 + rel_voltage)) + \
                (self.node_dvoltages[self.i-1] - self.node_dvoltages[self.j-1]) * self.capacitance
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        NQuanta(self.elem_phases[self.eindex], 
            &self.elem_n[self.eindex], &self.elem_inc[self.eindex], 
            &self.elem_dec[self.eindex], ctx.histeresis)


cdef class JunctionRSJPHModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* node_dvoltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef int* elem_n
    cdef UINT8_t* elem_inc
    cdef UINT8_t* elem_dec
    cdef double crit_current
    cdef double resistance
    cdef double capacitance
    cdef double phase_shift
    cdef int i, j, eindex
    cdef int ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(JunctionRSJPHModel self, int i, int j, int eindex):
        self.crit_current = 0.0
        self.resistance = 1.0
        self.capacitance = 1.0
        self.phase_shift = 0.0
        self.i = i
        self.j = j
        self.eindex = eindex
    
    def InitModel(JunctionRSJPHModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_dvoltage = psglobals.NodeDVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        cdef cnp.ndarray[INT_t, ndim=1, mode="c"] view_en = psglobals.ElementN
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_einc = psglobals.ElementInc
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_edec = psglobals.ElementDec
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.node_dvoltages = <double*>view_dvoltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
        self.elem_n = <int*>view_en.data
        self.elem_inc = <UINT8_t*>view_einc.data
        self.elem_dec = <UINT8_t*>view_edec.data
    
    def SetParameters(JunctionRSJPHModel self, params):
        self.crit_current = params[0]
        self.resistance = params[1]
        self.capacitance = params[2]
        self.phase_shift = params[3]
            
    cdef void Admitance0(JunctionRSJPHModel self, FLOAT_t[:] matrix) nogil:
        pass
    
    cdef void Admitance1(JunctionRSJPHModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm = ctx.coeff_dt/self.resistance + ctx.coeff_ddt*self.capacitance
        
        if self.i == 0:
            matrix[self.ind_jj] += adm
        elif self.j == 0:
            matrix[self.ind_ii] += adm
        else:
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void Admitance2(JunctionRSJPHModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        cdef double adm
        
        if self.i == 0:
            matrix[self.ind_jj] += self.crit_current * math.cos(ctx.phases[self.j-1] + self.phase_shift)
        elif self.j == 0:
            matrix[self.ind_ii] += self.crit_current * math.cos(ctx.phases[self.i-1] + self.phase_shift)
        else:
            adm = self.crit_current * math.cos(ctx.phases[self.i-1]-ctx.phases[self.j-1] + self.phase_shift)
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
    cdef void RHSCurrent(JunctionRSJPHModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        cdef double dphase
        cdef double dvoltage
        cdef double ddvoltage
        cdef double curr
        
        if self.i == 0:
            dphase = ctx.phases[self.j-1] + self.phase_shift
            dvoltage = ctx.voltages[self.j-1]
            ddvoltage = ctx.voltages_deriv[self.j-1]
            vec[self.j-1] -= self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
        elif self.j == 0:
            dphase = ctx.phases[self.i-1] + self.phase_shift
            dvoltage = ctx.voltages[self.i-1]
            ddvoltage = ctx.voltages_deriv[self.i-1]
            vec[self.i-1] -= self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
        else:
            dphase = ctx.phases[self.i-1] - ctx.phases[self.j-1] + self.phase_shift
            dvoltage = ctx.voltages[self.i-1] - ctx.voltages[self.j-1]
            ddvoltage = ctx.voltages_deriv[self.i-1] - ctx.voltages_deriv[self.j-1]
            curr = self.crit_current * math.sin(dphase) + dvoltage / self.resistance + ddvoltage * self.capacitance
            vec[self.i-1] -= curr
            vec[self.j-1] += curr

    cdef void Values(JunctionRSJPHModel self, ModelContext* ctx) nogil:
        if self.i == 0:
            self.elem_currents[self.eindex] = -self.crit_current * math.sin(self.node_phases[self.j-1] + self.phase_shift) - \
                 self.node_voltages[self.j-1] / self.resistance - self.node_dvoltages[self.j-1] * self.capacitance
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1] - self.phase_shift
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_currents[self.eindex] = self.crit_current * math.sin(self.node_phases[self.i-1] + self.phase_shift) + \
                 self.node_voltages[self.i-1] / self.resistance + self.node_dvoltages[self.i-1] * self.capacitance
            self.elem_phases[self.eindex] = self.node_phases[self.i-1] + self.phase_shift
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_currents[self.eindex] = self.crit_current * math.sin(self.node_phases[self.i-1] - self.node_phases[self.j-1] + self.phase_shift) + \
                (self.node_voltages[self.i-1] - self.node_voltages[self.j-1]) / self.resistance + \
                (self.node_dvoltages[self.i-1] - self.node_dvoltages[self.j-1]) * self.capacitance
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1]) + self.phase_shift
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        NQuanta(self.elem_phases[self.eindex], 
            &self.elem_n[self.eindex], &self.elem_inc[self.eindex], 
            &self.elem_dec[self.eindex], ctx.histeresis)

cdef class JunctionTJMModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* node_dvoltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef int* elem_n
    cdef UINT8_t* elem_inc
    cdef UINT8_t* elem_dec
    cdef int narray
    cdef double crit_current
    cdef double beta
    cdef double wvg
    cdef double wvrat
    cdef double wrrat
    cdef double sgw
    cdef complex[double] A[MaxTJMCoeffArraySize], B[MaxTJMCoeffArraySize], P[MaxTJMCoeffArraySize]
    cdef complex[double] Fc[MaxTJMCoeffArraySize], Fs[MaxTJMCoeffArraySize] 
    cdef complex[double] Fcdt[MaxTJMCoeffArraySize], Fsdt[MaxTJMCoeffArraySize]
    cdef complex[double] Fcprev[MaxTJMCoeffArraySize], Fsprev[MaxTJMCoeffArraySize]
    cdef complex[double] alpha0[MaxTJMCoeffArraySize], beta0[MaxTJMCoeffArraySize], alpha1[MaxTJMCoeffArraySize]
    cdef double sinphi2
    cdef double cosphi2
    cdef int i, j, eindex
    cdef int ind_ii, ind_ij, ind_ji, ind_jj
    def __init__(JunctionTJMModel self, int i, int j, int eindex):
        self.crit_current = 1.0
        self.beta = 1.0
        self.wvg = 2.6/0.63
        self.wvrat = 0.6
        self.wrrat = 0.1
        self.narray = 6
        self.i = i
        self.j = j
        self.eindex = eindex
    
    def InitModel(JunctionTJMModel self, ind_ii, ind_ij, ind_ji, ind_jj):
        self.ind_ii = ind_ii
        self.ind_ij = ind_ij
        self.ind_ji = ind_ji
        self.ind_jj = ind_jj

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_dvoltage = psglobals.NodeDVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        cdef cnp.ndarray[INT_t, ndim=1, mode="c"] view_en = psglobals.ElementN
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_einc = psglobals.ElementInc
        cdef cnp.ndarray[UINT8_t, ndim=1, mode="c"] view_edec = psglobals.ElementDec
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.node_dvoltages = <double*>view_dvoltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data
        self.elem_n = <int*>view_en.data
        self.elem_inc = <UINT8_t*>view_einc.data
        self.elem_dec = <UINT8_t*>view_edec.data
    
    def SetParameters(JunctionTJMModel self, params):
        self.crit_current = params[1]
        self.beta = params[2]
    
    def InitTJMModel(JunctionTJMModel self, params):
        cdef double x = 0.0

        self.crit_current = params[1]
        self.beta = params[2]
        self.wvg = params[3]
        self.wvrat = params[4]
        self.wrrat = params[5]

        if params[0] in psconfig.tjm_models:
            [A,B,P] = psconfig.tjm_models[params[0]]
            self.narray = len(A)
            for i in range(0, self.narray):
                self.A[i] = COMPLEX_t(A[i].real, A[i].imag)
                self.B[i] = COMPLEX_t(B[i].real, B[i].imag)
                self.P[i] = COMPLEX_t(P[i].real, P[i].imag)*self.wvg*0.5
                x = x - (self.A[i] / self.P[i]).real()

        else:
            raise Exception("Unknown TJM model: ".encode('UTF-8') + params[0].encode('UTF-8'))

        self.sgw = 1.0 / (self.wvg * self.wvrat)

        #printf("X=%lf SGW=%lf\n", x, self.sgw)

        for i in range(0, self.narray):
            self.A[i] = self.A[i] / (-1.0 * x)
            self.B[i] = self.B[i] * self.wvg * (self.wrrat - 1.0) / (2.0 * self.wvrat)

            
    cdef void Admitance0(JunctionTJMModel self, FLOAT_t[:] matrix) nogil:
        # This metnod called during initialization of simulation
        cdef complex[double] neg_one = COMPLEX_t(-1.0, 0.0)

        # Fc(0) and Fs(0)
        for i in range(0, self.narray):
            self.Fcprev[i] = neg_one/self.P[i]
            self.Fsprev[i] = COMPLEX_t(0.0, 0.0)
    
    cdef void Admitance1(JunctionTJMModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        # This methon is called on the first Newton iteration of the time step
        cdef double adm = self.crit_current*(self.sgw * (ctx.coeff_dt + self.sgw * self.beta * ctx.coeff_ddt))
        cdef double dphase_prev
        cdef double dvoltage_prev
        cdef double sinphi2
        cdef double cosphi2
        cdef complex[double] pq
        cdef complex[double] epq
        
        if self.i == 0:
            dphase_prev = -ctx.phases_prev[self.j-1]
            dvoltage_prev = -ctx.voltages_prev[self.j-1]
            matrix[self.ind_jj] += adm
        elif self.j == 0:
            dphase_prev = ctx.phases_prev[self.i-1]
            dvoltage_prev = ctx.voltages_prev[self.i-1]
            matrix[self.ind_ii] += adm
        else:
            dphase_prev = ctx.phases_prev[self.i-1] - ctx.phases_prev[self.j-1]
            dvoltage_prev = ctx.voltages_prev[self.i-1] - ctx.voltages_prev[self.j-1]
            matrix[self.ind_ii] += adm
            matrix[self.ind_ij] -= adm
            matrix[self.ind_ji] -= adm
            matrix[self.ind_jj] += adm
        
        sinphi2 = math.sin(dphase_prev/2.0)
        cosphi2 = math.cos(dphase_prev/2.0)

        for i in range(0, self.narray):
            pq = self.P[i]*ctx.time_step
            epq = exp(pq)
            self.alpha0[i] = (epq * (pq*pq - 2.0) + pq*2.0 + 2.0) / (pq*pq*self.P[i])
            self.beta0[i] = (epq*pq - epq*2.0 + pq + 2.0) /(pq*self.P[i]*self.P[i])
            self.alpha1[i] = (epq*2.0 - 2.0 - pq*2.0 - pq*pq) / (pq*pq*self.P[i])
            #self.alpha0[i] = (epq * (pq - 1.0) + 1.0)/(pq * self.P[i])
            #self.alpha1[i] = (epq - pq - 1.0)/(pq * self.P[i])

            # Part of Fc(t+dt) and Fs(t+dt) which do not depends on phase(t+dt)
            self.Fcdt[i] = epq*self.Fcprev[i]+self.alpha0[i]*cosphi2-self.beta0[i]*sinphi2*dvoltage_prev/ 2.0
            self.Fsdt[i] = epq*self.Fsprev[i]+self.alpha0[i]*sinphi2+self.beta0[i]*cosphi2*dvoltage_prev/ 2.0
            #self.Fcdt[i] = epq*self.Fcprev[i]+self.alpha0[i]*cosphi2
            #self.Fsdt[i] = epq*self.Fsprev[i]+self.alpha0[i]*sinphi2
        
    cdef void Admitance2(JunctionTJMModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        # This methon called on each Newton step
        cdef double dphase
        cdef double dcurr
        cdef double cosphi
        cdef complex[double] FcAnSum = COMPLEX_t(0.0, 0.0)
        cdef complex[double] FsAnSum = COMPLEX_t(0.0, 0.0)
        cdef complex[double] Sum1 = COMPLEX_t(0.0, 0.0)
        cdef complex[double] Sum2 = COMPLEX_t(0.0, 0.0)
        
        
        if self.i == 0:
            dphase = -ctx.phases[self.j-1]
        elif self.j == 0:
            dphase = ctx.phases[self.i-1]
        else:
            dphase = ctx.phases[self.i-1] - ctx.phases[self.j-1]
        
        self.sinphi2 = math.sin(dphase/2.0)
        self.cosphi2 = math.cos(dphase/2.0)
        cosphi = math.cos(dphase)

        for i in range(0, self.narray):
            self.Fc[i] = self.Fcdt[i]+self.alpha1[i]*self.cosphi2
            self.Fs[i] = self.Fsdt[i]+self.alpha1[i]*self.sinphi2
            FcAnSum = FcAnSum + self.A[i] * self.Fc[i]
            FsAnSum = FsAnSum + self.A[i] * self.Fs[i]

        dcurr = -self.crit_current*(FcAnSum.real() * self.cosphi2 - FsAnSum.real() * self.sinphi2)

        #printf("TJM dcurr=%lf\n", dcurr)

        if self.i == 0:
            matrix[self.ind_jj] += dcurr
        elif self.j == 0:
            matrix[self.ind_ii] += dcurr
        else:
            matrix[self.ind_ii] += dcurr
            matrix[self.ind_ij] -= dcurr
            matrix[self.ind_ji] -= dcurr
            matrix[self.ind_jj] += dcurr
        
    cdef void RHSCurrent(JunctionTJMModel self, FLOAT_t[:] vec, ModelContext* ctx) nogil:
        cdef double dphase
        cdef double dvoltage
        cdef double ddvoltage
        cdef double curr
        cdef double full_current
        cdef complex[double] FcSum = COMPLEX_t(0.0, 0.0)
        cdef complex[double] FsSum = COMPLEX_t(0.0, 0.0)
        
        if self.i == 0:
            dphase = -ctx.phases[self.j-1]
            dvoltage = -ctx.voltages[self.j-1]
            ddvoltage = -ctx.voltages_deriv[self.j-1]
        elif self.j == 0:
            dphase = ctx.phases[self.i-1]
            dvoltage = ctx.voltages[self.i-1]
            ddvoltage = ctx.voltages_deriv[self.i-1]
        else:
            dphase = ctx.phases[self.i-1] - ctx.phases[self.j-1]
            dvoltage = ctx.voltages[self.i-1] - ctx.voltages[self.j-1]
            ddvoltage = ctx.voltages_deriv[self.i-1] - ctx.voltages_deriv[self.j-1]

        self.sinphi2 = math.sin(dphase/2.0)
        self.cosphi2 = math.cos(dphase/2.0)    

        for i in range(0, self.narray):
            FcSum = FcSum + (self.A[i] + self.B[i])*self.Fc[i]
            FsSum = FsSum + (self.A[i] - self.B[i])*self.Fs[i]

        curr = FcSum.real() * self.sinphi2 + FsSum.real() * self.cosphi2

        if self.i == 0:
            vec[self.j-1] += self.crit_current * (-curr + self.sgw * (dvoltage + self.sgw * self.beta * ddvoltage))
        elif self.j == 0:
            vec[self.i-1] -= self.crit_current * (-curr + self.sgw * (dvoltage + self.sgw * self.beta * ddvoltage))
        else:
            full_current = self.crit_current * (-curr + self.sgw * (dvoltage + self.sgw * self.beta * ddvoltage))
            vec[self.i-1] -= full_current
            vec[self.j-1] += full_current

        #printf("TJM curr=%lf\n", self.jcurr)


    cdef void Values(JunctionTJMModel self, ModelContext* ctx) nogil:
        # This methon called when time step is accepted
        cdef double dphase
        cdef double dvoltage
        cdef double ddvoltage
        cdef double full_current
        cdef double sinphi2
        cdef double cosphi2
        cdef double curr
        cdef complex[double] FcSum = COMPLEX_t(0.0, 0.0)
        cdef complex[double] FsSum = COMPLEX_t(0.0, 0.0)

        if self.i == 0:
            dphase = -self.node_phases[self.j-1]
            dvoltage = -self.node_voltages[self.j-1]
            ddvoltage = -self.node_dvoltages[self.j-1]
        elif self.j == 0:
            dphase = self.node_phases[self.i-1]
            dvoltage = self.node_voltages[self.i-1]
            ddvoltage = self.node_dvoltages[self.i-1]
        else:
            dphase = self.node_phases[self.i-1] - self.node_phases[self.j-1]
            dvoltage = self.node_voltages[self.i-1] - self.node_voltages[self.j-1]
            ddvoltage = self.node_dvoltages[self.i-1] - self.node_dvoltages[self.j-1]

        if self.i == 0:
            dphase = -self.node_phases[self.j-1]
        elif self.j == 0:
            dphase = self.node_phases[self.i-1]
        else:
            dphase = self.node_phases[self.i-1] - self.node_phases[self.j-1]

        sinphi2 = math.sin(dphase/2.0)
        cosphi2 = math.cos(dphase/2.0)

        for i in range(0, self.narray):
            self.Fc[i] = self.Fcdt[i]+self.alpha1[i]*self.cosphi2
            self.Fs[i] = self.Fsdt[i]+self.alpha1[i]*self.sinphi2
            self.Fcprev[i] = self.Fc[i]
            self.Fsprev[i] = self.Fs[i]
            FcSum = FcSum + (self.A[i] + self.B[i])*self.Fc[i]
            FsSum = FsSum + (self.A[i] - self.B[i])*self.Fs[i]

        curr = FcSum.real() * sinphi2 + FsSum.real() * cosphi2

        if self.i == 0:
            self.elem_currents[self.eindex] = -self.crit_current * (-curr + self.sgw * (dvoltage + 
                self.sgw * self.beta * ddvoltage))
            self.elem_phases[self.eindex] = -self.node_phases[self.j-1]
            self.elem_voltages[self.eindex] = -self.node_voltages[self.j-1]
        elif self.j == 0:
            self.elem_currents[self.eindex] = self.crit_current * (-curr + self.sgw * (dvoltage + 
                self.sgw * self.beta * ddvoltage))
            self.elem_phases[self.eindex] = self.node_phases[self.i-1]
            self.elem_voltages[self.eindex] = self.node_voltages[self.i-1]
        else:
            self.elem_currents[self.eindex] = self.crit_current * (-curr + self.sgw * 
                (dvoltage + self.sgw * self.beta * ddvoltage))
            self.elem_phases[self.eindex] = (self.node_phases[self.i-1] - self.node_phases[self.j-1])
            self.elem_voltages[self.eindex] = (self.node_voltages[self.i-1] - self.node_voltages[self.j-1])

        NQuanta(self.elem_phases[self.eindex], 
            &self.elem_n[self.eindex], &self.elem_inc[self.eindex], 
            &self.elem_dec[self.eindex], ctx.histeresis)


cdef class MutualInductanceModel(ElementModel):
    cdef double* node_phases
    cdef double* node_voltages
    cdef double* elem_currents
    cdef double* elem_phases
    cdef double* elem_voltages
    cdef double minductance
    cdef int q, v, eindex
    cdef int ind_qq, ind_qv, ind_vq, ind_vv
    def __init__(MutualInductanceModel self, int q, int v, int eindex):
        self.minductance = 0.0
        self.q = q
        self.v = v
        self.eindex = eindex
    
    def InitModel(MutualInductanceModel self, ind_qq, ind_qv, ind_vq, ind_vv):
        self.ind_qq = ind_qq
        self.ind_qv = ind_qv
        self.ind_vq = ind_vq
        self.ind_vv = ind_vv

        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_phase = psglobals.NodePhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_voltage = psglobals.NodeVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ephase = psglobals.ElementPhase
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_evoltage = psglobals.ElementVoltage
        cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] view_ecurrent = psglobals.ElementCurrent
        self.node_phases = <double*>view_phase.data
        self.node_voltages = <double*>view_voltage.data
        self.elem_phases = <double*>view_ephase.data
        self.elem_voltages = <double*>view_evoltage.data
        self.elem_currents = <double*>view_ecurrent.data

    def SetParameters(MutualInductanceModel self, params):
        self.minductance = params[0]
        
    cdef void Admitance0(MutualInductanceModel self, FLOAT_t[:] matrix) nogil:
        matrix[self.ind_qv] += self.minductance
        matrix[self.ind_vq] += self.minductance
    
    cdef void Admitance1(MutualInductanceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
        
    cdef void Admitance2(MutualInductanceModel self, FLOAT_t[:] matrix, ModelContext* ctx) nogil:
        pass
    
    cdef void RHSCurrent(MutualInductanceModel self, FLOAT_t[:] vec, pModelContext_t ctx) nogil:
        vec[self.q-1] -= ctx.phases[self.v-1] * self.minductance
        vec[self.v-1] -= ctx.phases[self.q-1] * self.minductance


    cdef void Values(MutualInductanceModel self, ModelContext* ctx) nogil:
        # There are no current, phase or voltage across mutual inductance
        self.elem_currents[self.eindex] = 0.0
        self.elem_phases[self.eindex] = 0.0
        self.elem_voltages[self.eindex] = 0.0


cdef PyObject** source_emodels
cdef PyObject** all_emodels
RHSHelper = None
NTHREADS = 4

def InitElementModels():
    global source_emodels, all_emodels, RHSHelper, NTHREADS
    cdef int i
    cdef int nsource = psglobals.TotalSourceElements
    cdef int nelem = psglobals.TotalElements
    cdef int nth = NTHREADS
    cdef int first_buckets = int(nelem//nth)
    cdef int last_bucket = int(nelem - (nth-1)*first_buckets)

    source_emodels = <PyObject**>malloc(nsource * sizeof(PyObject*))
    all_emodels = <PyObject**>malloc(nelem * sizeof(PyObject*))
    
    RHSHelper = np.zeros((nth, psglobals.TotalNodes), dtype = np.float64)

    for i in range(0, nsource):
        source_emodels[i] = <PyObject*>(psglobals.SourceElementList[i].model)

    for i in range(0, nelem):
        all_emodels[i] = <PyObject*>(psglobals.ElementList[i].model)


def BuildStaticMatrix(cnp.ndarray[FLOAT_t, ndim=1, mode="c"] A0):
    global all_emodels, NTHREADS
    cdef int nelem = psglobals.TotalElements
    cdef double coeff_dt = SP.InterpCoeffDt[0]
    cdef double coeff_ddt = SP.InterpCoeffDDt[0]
    cdef int i
    cdef int nth = NTHREADS
    cdef int first_buckets = int(nelem//nth)
    cdef int bucketsize

    if psglobals.OptimizeSimulation and psglobals.CurrentTime > SP.INITIAL_RAMP:
        for elem in psglobals.SourceElementList:
            elem.UpdateParameters()
    else:
        for elem in psglobals.ElementList:
            elem.UpdateParameters()

    for i in range(0, nelem):
        (<ElementModel>all_emodels[i]).Admitance0(A0)


def BuildMatrix(cnp.ndarray[FLOAT_t, ndim=1, mode="c"] A1,
                cnp.ndarray[FLOAT_t, ndim=1, mode="c"] A2,
                cnp.ndarray[FLOAT_t, ndim=1, mode="c"] B):
    global all_emodels, NTHREADS
    
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] phases = SP.X[SP.Xind]
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] phases_prev = SP.X[SP.Xind-1]
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages = SP.DX
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages_deriv = SP.DDX
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages_prev = SP.DXprev
    cdef int nelem = psglobals.TotalElements
    cdef int i
    cdef int nth = NTHREADS
    cdef int first_buckets = int(nelem//nth)
    cdef int bucketsize
    
    cdef ModelContext ctx
    ctx.coeff_dt = SP.InterpCoeffDt[0]
    ctx.coeff_ddt = SP.InterpCoeffDDt[0]
    ctx.smooth_factor = SP.InitialSmoothFactor
    ctx.time_step = SP.TimeSteps[0]
    ctx.phases = <double *>phases.data
    ctx.phases_prev = <double *>phases_prev.data
    ctx.voltages = <double *>voltages.data
    ctx.voltages_deriv = <double *>voltages_deriv.data
    ctx.voltages_prev = <double *>voltages_prev.data
    
    for i in range(0, nelem):
        (<ElementModel>all_emodels[i]).Admitance1(A1, &ctx)
        (<ElementModel>all_emodels[i]).Admitance2(A2, &ctx)
        (<ElementModel>all_emodels[i]).RHSCurrent(B, &ctx)
        

def UpdateMatrix(cnp.ndarray[FLOAT_t, ndim=1, mode="c"] A2,
                 cnp.ndarray[FLOAT_t, ndim=1, mode="c"] B):
    global all_emodels, NTHREADS
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] phases = SP.X[SP.Xind]
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] phases_prev = SP.X[SP.Xind-1]
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages = SP.DX
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages_deriv = SP.DDX
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages_prev = SP.DXprev
    cdef int nelem = psglobals.TotalElements
    cdef int i
    cdef int nth = NTHREADS
    cdef int first_buckets = int(nelem//nth)
    cdef int bucketsize
    
    cdef ModelContext ctx
    ctx.coeff_dt = SP.InterpCoeffDt[0]
    ctx.coeff_ddt = SP.InterpCoeffDDt[0]
    ctx.smooth_factor = SP.InitialSmoothFactor
    ctx.time_step = SP.TimeSteps[0]
    ctx.phases = <double *>phases.data
    ctx.phases_prev = <double *>phases_prev.data
    ctx.voltages = <double *>voltages.data
    ctx.voltages_deriv = <double *>voltages_deriv.data
    ctx.voltages_prev = <double *>voltages_prev.data
            
    for i in range(0, nelem):
        (<ElementModel>all_emodels[i]).Admitance2(A2, &ctx)
        (<ElementModel>all_emodels[i]).RHSCurrent(B, &ctx)
            

def UpdateRHS(cnp.ndarray[FLOAT_t, ndim=1, mode="c"] B):
    global all_emodels, NTHREADS, RHSHelper
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] phases = SP.X[SP.Xind]
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] phases_prev = SP.X[SP.Xind-1]
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages = SP.DX
    cdef cnp.ndarray[FLOAT_t, ndim=1, mode="c"] voltages_deriv = SP.DDX
    cdef FLOAT_t[:,:] RHSView = RHSHelper
    cdef int nelem = psglobals.TotalElements
    cdef int i, j
    cdef int nth = NTHREADS
    cdef int first_buckets = int(nelem//nth)
    cdef int bucketsize
    
    cdef ModelContext ctx
    ctx.coeff_dt = SP.InterpCoeffDt[0]
    ctx.coeff_ddt = SP.InterpCoeffDDt[0]
    ctx.smooth_factor = SP.InitialSmoothFactor
    ctx.time_step = SP.TimeSteps[0]
    ctx.phases = <double *>phases.data
    ctx.phases_prev = <double *>phases_prev.data
    ctx.voltages = <double *>voltages.data
    ctx.voltages_deriv = <double *>voltages_deriv.data
            
    #RHSHelper.fill(0.0)

    for i in range(0, nelem):
        (<ElementModel>all_emodels[i]).RHSCurrent(B, &ctx)

    #for j in prange(nth, nogil=True, num_threads=nth):

    #B = RHSHelper.sum(axis = 0, keepdims = True)

def UpdateValues():
    global all_emodels, NTHREADS
    cdef int nelem = psglobals.TotalElements
    cdef int i
    cdef int nth = NTHREADS
    cdef int first_buckets = int(nelem//nth)
    cdef int bucketsize
            
    cdef ModelContext ctx
    ctx.smooth_factor = SP.InitialSmoothFactor
    ctx.time_step = SP.TimeSteps[0]
    ctx.histeresis = SP.PHASE_HISTERESIS

    for i in range(0, nelem):
        (<ElementModel>all_emodels[i]).Values(&ctx)
