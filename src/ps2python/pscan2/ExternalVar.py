# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals
from .Expression import smooth_step
from . import SimulationParameters

class ExternalVar:
    """
    External parameter in circuit. If the whole circuit contains several 
    circuits, external parameters in each circuit will be independent

    Methods
    ---------
    Name() : string
        Name of the external parameter
    Value() : float
        Value of external parameter
    InitialValue() : float
        Initial value of external parameter (as defined in HDL file)
    SetValue(new_value)
        Set value of external parameter
    """
    def __init__(self, path, vardecl, index, circuit):
        self.path = path
        self.name = vardecl.Name()
        # circuit cannot be None
        cir_fname = circuit.FullName()
        if cir_fname == ".":
            self.full_name = "." + self.name
        else:
            self.full_name = cir_fname + "." + self.name
        self.value = vardecl.Value().GetFloatConstValue()
        self.initial_value = self.value
        self.init_smooth_time = -SimulationParameters.INITIAL_RAMP
        self.expected_value = self.value
        self.init_smooth_value = self.value
        self.index = index
        psglobals.ExternalValues[self.index] = self.value
        
    def __repr__(self):
        return("External {}.{} = {} [{}] ({})".format(self.path, self.name, self.value, 
            self.initial_value, self.index))
            
    def Name(self):
        return(self.name)
        
    def Path(self):
        return(self.path)
        
    def FullName(self):
        return(self.full_name)
        
    def FindType(self):
        return('p')

    def Value(self):
        return(psglobals.ExternalValues[self.index])
        
    def InitialValue(self):
        return(self.initial_value)
        
    def Index(self):
        return(self.index)
        
    def SetValue(self, newvalue):
        self.value = newvalue
        psglobals.ExternalValues[self.index] = self.value
        
    def SetValueSmooth(self, newvalue):
        self.init_smooth_time = psglobals.CurrentTime
        self.expected_value = newvalue
        self.init_smooth_value = self.value
    
    def Update(self):
        if psglobals.CurrentTime >= self.init_smooth_time and \
            psglobals.CurrentTime <= self.init_smooth_time + SimulationParameters.INITIAL_RAMP:
            self.value = self.init_smooth_value + \
                smooth_step((psglobals.CurrentTime - self.init_smooth_time)/SimulationParameters.INITIAL_RAMP)*\
                (self.expected_value - self.init_smooth_value)
            psglobals.ExternalValues[self.index] = self.value   
        
            
