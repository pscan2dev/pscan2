# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

class ExpressionAST:
	EXP_TYPE_INVALID = -1
	EXP_TYPE_ICONST = 1
	EXP_TYPE_FCONST = 2
	EXP_TYPE_SCONST = 3
	EXP_TYPE_IDENT = 4
	EXP_TYPE_UNARY = 5
	EXP_TYPE_BINARY = 6
	EXP_TYPE_FCALL = 7
	EXP_TYPE_INDEXED_VAL = 8
	EXP_TYPE_SEQUENCE = 9
	EXP_TYPE_POINTS = 10
	
	def __init__(self, etype, *args):
		self.etype = etype
		if etype == ExpressionAST.EXP_TYPE_ICONST:
			self.iconst_val = args[0]
		elif etype == ExpressionAST.EXP_TYPE_FCONST:
			self.fconst_val = args[0]
		elif etype == ExpressionAST.EXP_TYPE_SCONST:
			self.sconst_val = args[0]
		elif etype == ExpressionAST.EXP_TYPE_IDENT:
			self.ident = args[0]
		elif etype == ExpressionAST.EXP_TYPE_UNARY:
			if args[0] == 'not':
				self.operator = '!'
			else:
				self.operator = args[0]
			self.operand = args[1]
		elif etype == ExpressionAST.EXP_TYPE_BINARY:
			if args[0] == 'gt':
				self.operator = '>'
			elif args[0] == 'lt':
				self.operator = '<'
			elif args[0] == 'ge':
				self.operator = '>='
			elif args[0] == 'le':
				self.operator = '<='
			elif args[0] == 'ne':
				self.operator = '!='
			elif args[0] == 'eq':
				self.operator = '=='
			elif args[0] == 'and':
				self.operator = '&&'
			elif args[0] == 'or':
				self.operator = '||'
			else:
				self.operator = args[0]
			self.operand1 = args[1]
			self.operand2 = args[2]
		elif etype == ExpressionAST.EXP_TYPE_FCALL:
			self.fname = args[0]
			self.fargs = args[1]
		elif etype == ExpressionAST.EXP_TYPE_SEQUENCE:
			self.explist = args[0]
		elif etype == ExpressionAST.EXP_TYPE_POINTS:
			self.operand1 = args[0]
			self.operand2 = args[1]
		else:
			pass
	
	def __repr__(self):
		res = ""
		if self.etype == ExpressionAST.EXP_TYPE_ICONST:
			res += str(self.iconst_val)
		elif self.etype == ExpressionAST.EXP_TYPE_FCONST:
			res += str(self.fconst_val)
		elif self.etype == ExpressionAST.EXP_TYPE_SCONST:
			res += "'" + str(self.sconst_val) + "'"
		elif self.etype == ExpressionAST.EXP_TYPE_IDENT:
			res += str(self.ident)
		elif self.etype == ExpressionAST.EXP_TYPE_UNARY:
			res += str(self.operator) + str(self.operand)
		elif self.etype == ExpressionAST.EXP_TYPE_BINARY:
			res += str(self.operand1) + str(self.operator) + str(self.operand2)
		elif self.etype == ExpressionAST.EXP_TYPE_FCALL:
			res += str(self.fname) + "(" + str(self.fargs) + ")"
		elif self.etype == ExpressionAST.EXP_TYPE_SEQUENCE:
			res += str(self.explist)
		elif self.etype == ExpressionAST.EXP_TYPE_POINTS:
			res += str(self.operand1) + "." + str(self.operand2)
		else:
			pass
		return(res)
		
	def GetFloatConstValue(self):
		if self.etype == ExpressionAST.EXP_TYPE_FCONST:
			return(self.fconst_val)
		elif self.etype == ExpressionAST.EXP_TYPE_ICONST:
			return(float(self.iconst_val))
		elif self.etype == ExpressionAST.EXP_TYPE_UNARY:
			if self.operator == '-':
				return(-self.operand.GetFloatConstValue())
			else:
				return(0.0)
		elif self.etype == ExpressionAST.EXP_TYPE_BINARY:
			if self.operator == '+':
				return(self.operand1.GetFloatConstValue() + self.operand2.GetFloatConstValue())
			elif self.operator == '-':
				return(self.operand1.GetFloatConstValue() - self.operand2.GetFloatConstValue())
			elif self.operator == '*':
				return(self.operand1.GetFloatConstValue() * self.operand2.GetFloatConstValue())	
			elif self.operator == '/':
				return(self.operand1.GetFloatConstValue() / self.operand2.GetFloatConstValue())	
			else:
				return(0.0)
		else:
			return(0.0)

	def GetIdentSet(self):
		if self.etype == ExpressionAST.EXP_TYPE_IDENT:
			return(set([self.ident]))
		elif self.etype == ExpressionAST.EXP_TYPE_UNARY:
			return(self.operand.GetIdentSet())
		elif self.etype == ExpressionAST.EXP_TYPE_BINARY:
			return(self.operand1.GetIdentSet() | self.operand2.GetIdentSet())
		elif self.etype == ExpressionAST.EXP_TYPE_FCALL:
			result = set()
			for arg in self.fargs:
				result |= arg.GetIdentSet()
			return(result)
		return(set())
			