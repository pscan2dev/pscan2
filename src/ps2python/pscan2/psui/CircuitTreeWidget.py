# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt,
        QTextStream, QObject, pyqtSignal, pyqtSlot)
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QGroupBox, QTreeWidget, QTreeWidgetItem,
        QVBoxLayout, QGridLayout, QSpacerItem, QSizePolicy, QLineEdit, QCheckBox, QPushButton)
        
from pscan2 import psglobals

from .ParametersFrame import ParametersFrame
from .ValuesFrame import ValuesFrame
from .ElementsFrame import ElementsFrame
from .RulesFrame import RulesFrame
        
class CircuitTreeWidget(QTreeWidget):
    PARAMETERS_SELECTED = 0
    EXTERNALS_SELECTED = 1
    VALUES_SELECTED = 2
    ELEMENT_FUNCTION_SELECTED = 3
    RULES_SELECTED = 4

    use_checked = pyqtSignal(int, list, name='use_checked')

    def __init__(self, parent, parameters_layout, externals_layout, 
            values_layout, elements_layout, rules_layout):
        super(CircuitTreeWidget, self).__init__(parent)
        
        self.itemClicked.connect(self.circuitActivatedCB)
        self.parameters_layout = parameters_layout
        self.externals_layout = externals_layout
        self.values_layout = values_layout
        self.elements_layout = elements_layout
        self.rules_layout = rules_layout
        self.setColumnCount(1)
        self.headerItem().setHidden(True)
        self.number_items = 0
        item = QTreeWidgetItem(self)
        item.number = self.number_items
        item.circuit = None
        item.setText(0, "Globals")
        self.insertTopLevelItem(0, item)
        
        parameters_frame = ParametersFrame(psglobals.Parameters)
        parameters_frame.use_checked.connect(lambda selection_list, selection_type =\
            CircuitTreeWidget.PARAMETERS_SELECTED: self.onUseChecked(selection_type, selection_list))
        parameters_layout.insertWidget(self.number_items, parameters_frame)
      
        externals_frame = ParametersFrame({})
        externals_layout.insertWidget(self.number_items, externals_frame)
      
        values_frame = ValuesFrame(psglobals.Internals, {})
        values_frame.use_checked.connect(lambda selection_list, selection_type =\
            CircuitTreeWidget.VALUES_SELECTED: self.onUseChecked(selection_type, selection_list))
        values_layout.insertWidget(self.number_items, values_frame)
        
        elements_frame = ElementsFrame({})
        elements_layout.insertWidget(self.number_items, elements_frame)
        
        rules_frame = RulesFrame([])
        rules_layout.insertWidget(self.number_items, rules_frame)
        
        self.number_items += 1
        
        root_circuit = psglobals.RootCircuit
        item = self.CreateCircuitTreeItem(self, root_circuit, parameters_layout, externals_layout, 
            values_layout, elements_layout, rules_layout)
        self.insertTopLevelItem(0, item)
        
            
    def CreateCircuitTreeItem(self, parent, circuit, parameters_layout, externals_layout, 
            values_layout, elements_layout, rules_layout):
        item = QTreeWidgetItem(parent)
        
        parameters_frame = ParametersFrame(circuit.Parameters())
        parameters_frame.use_checked.connect(lambda selection_list, selection_type =\
            CircuitTreeWidget.PARAMETERS_SELECTED: self.onUseChecked(selection_type, selection_list))
        parameters_layout.insertWidget(self.number_items, parameters_frame)
        
        externals_frame = ParametersFrame(circuit.Externals())
        externals_frame.use_checked.connect(lambda selection_list, selection_type =\
            CircuitTreeWidget.EXTERNALS_SELECTED: self.onUseChecked(selection_type, selection_list))
        externals_layout.insertWidget(self.number_items, externals_frame)
        
        values_frame = ValuesFrame(circuit.Internals(), circuit.Values())
        values_frame.use_checked.connect(lambda selection_list, selection_type =\
            CircuitTreeWidget.VALUES_SELECTED: self.onUseChecked(selection_type, selection_list))
        values_layout.insertWidget(self.number_items, values_frame)
        
        elements_frame = ElementsFrame(circuit.Elements())
        elements_frame.use_checked.connect(lambda selection_list, selection_type =\
            CircuitTreeWidget.ELEMENT_FUNCTION_SELECTED: self.onUseChecked(selection_type, selection_list))
        elements_layout.insertWidget(self.number_items, elements_frame)
        
        rules_frame = RulesFrame(circuit.RuleList())
        rules_frame.use_checked.connect(lambda selection_list, selection_type = \
            CircuitTreeWidget.RULES_SELECTED: self.onUseChecked(selection_type, selection_list))
        rules_layout.insertWidget(self.number_items, rules_frame)
        
        item.number = self.number_items
        self.number_items += 1
        item.circuit = circuit
        item.setText(0, circuit.Name() + ' :' + circuit.CircuitName())
        slist = list(circuit.subcircuits.values())
        slist.sort(key = lambda c: c.Name())
        for subcircuit in slist:
            self.CreateCircuitTreeItem(item, subcircuit, parameters_layout, externals_layout, 
                values_layout, elements_layout, rules_layout)
        return item
        
        
    @pyqtSlot(QTreeWidgetItem,int)
    def circuitActivatedCB(self, item, column):
        #print("Item activated: " + str(item.number))
        param_widget = self.parameters_layout.widget(item.number)
        param_widget.updateValues()
        self.parameters_layout.setCurrentIndex(item.number)

        param_widget = self.externals_layout.widget(item.number)
        param_widget.updateValues()
        self.externals_layout.setCurrentIndex(item.number)

        value_widget = self.values_layout.widget(item.number)
        value_widget.updateValues()
        self.values_layout.setCurrentIndex(item.number)
        
        self.elements_layout.setCurrentIndex(item.number)
        self.rules_layout.setCurrentIndex(item.number)
        
    @pyqtSlot(int, list)
    def onUseChecked(self, selection_type, selection_list):
        #print("CircuitTreeWidget::onUseChecked: ", selection_type, selection_list)
        self.use_checked.emit(selection_type, selection_list)
		
    def UpdateParameters(self):
        for i in range(0, self.parameters_layout.count()):
            self.parameters_layout.widget(i).updateValues()
