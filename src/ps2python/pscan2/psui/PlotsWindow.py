# -*- coding: utf-8 -*-
# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import os
os.environ['PYQTGRAPH_QT_LIB'] = 'PyQt5'

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
import numpy as np
import math, random

GRAY_LINE = QtGui.QPen(QtGui.QBrush(QtGui.QColor(127,127,127)), 0)

RULE_LABEL = QtGui.QColor(255, 127, 0)

PLOT_PENS=[QtGui.QColor(166,206,227), 
           QtGui.QColor(31,120,180), 
           QtGui.QColor(178,223,138), 
           QtGui.QColor(51,160,44), 
           QtGui.QColor(251,154,153), 
           QtGui.QColor(227,26,28), 
           QtGui.QColor(253,191,111), 
           QtGui.QColor(255,127,0), 
           QtGui.QColor(202,178,214), 
           QtGui.QColor(106,61,154)]
RULE_LINE_COLORS = [QtGui.QColor(255, 0 , 0),
               QtGui.QColor(255, 127, 0),
               QtGui.QColor(255, 255, 0),
               QtGui.QColor(0, 255, 0),
               QtGui.QColor(0, 0, 255),
               QtGui.QColor(75, 0, 130),
               QtGui.QColor(148, 0, 211)]
RULE_FILL_COLORS = [QtGui.QColor(255, 0 , 0, 100),
               QtGui.QColor(255, 127, 0, 100),
               QtGui.QColor(255, 255, 0, 100),
               QtGui.QColor(0, 255, 0, 100),
               QtGui.QColor(0, 0, 255, 100),
               QtGui.QColor(75, 0, 130, 100),
               QtGui.QColor(148, 0, 211, 100)]


class AnalogPlotWidget(pg.PlotWidget):
    def __init__(self, name, var_list, tmax):
        super(AnalogPlotWidget, self).__init__(name=name)
        self.tmax = tmax
        self.var_list = var_list
        self.plot_map = {}
        self.data_map = {}
        self.plot_explist = []
        self.highlighted_plot = None
        self.setXRange(0, self.tmax)
        legend = self.addLegend(offset=(-5, 5))
        for j in range(0, len(var_list)):
            (method_name, obj) = var_list[j]
            plot = pg.PlotCurveItem(name=method_name+'('+obj.FullName()+')', clickable=True)
            plot.sigClicked.connect(self.highlightPlot)
            plot.setPen(PLOT_PENS[j % len(PLOT_PENS)])
            self.addItem(plot)
            self.plot_map[j] = plot
            self.plot_explist.append(var_list[j])
            data_array = np.zeros(1000, dtype=np.float)
            self.data_map[j] = data_array

    def Update(self, t, time_array, index):
        for j in range(0, len(self.plot_explist)):
            if self.data_map[j].shape[0] <= index:
                self.data_map[j] = np.resize(self.data_map[j], self.data_map[j].shape[0] + 1000)
            arr = self.data_map[j]
            (method_name, obj) = self.plot_explist[j]
            arr[index] = getattr(obj, method_name)()
            plot = self.plot_map[j]
            plot.setData(x=time_array, y=arr[0:index+1])

    def Init(self):
        for j in range(0, len(self.plot_explist)):
            self.data_map[j].fill(0.0)
            self.plot_map[j].setData(x=[], y=[])


    def highlightPlot(self, sel_plot):
        for j, plot in self.plot_map.items():
            if plot is sel_plot:
                if self.highlighted_plot == plot:
                    plot.setPen(PLOT_PENS[j % len(PLOT_PENS)], width=1)
                    self.highlighted_plot = None
                else:
                    plot.setPen(PLOT_PENS[j % len(PLOT_PENS)], width=3)
                    self.highlighted_plot = plot
            else:
                plot.setPen(PLOT_PENS[j % len(PLOT_PENS)], width=1)

    def WritePlotNames(self, f):
        for j in range(0, len(self.plot_explist)):
            (method_name, obj) = self.plot_explist[j]
            f.write(','+method_name+'('+obj.FullName()+')')
    
    def WritePlotValues(self, f, i):
        for j in range(0, len(self.plot_explist)):
            arr = self.data_map[j]
            f.write(','+str(arr[i]))

    def WriteVeusz(self, f, index):
        f.write('descriptor')
        for j in range(0, len(self.plot_explist)):
            (method_name, obj) = self.plot_explist[j]
            f.write(' '+method_name+'('+obj.FullName()+')')
        f.write('\n')
        for i in range(index):
            for j in range(0, len(self.plot_explist)):
                arr = self.data_map[j]
                f.write(str(arr[i]) + ' ')
            f.write('\n')



class DigitalPlotWidget(pg.PlotWidget):
    def __init__(self, name, var_list, tmax):
        super(DigitalPlotWidget, self).__init__(name=name)
        self.tmax = tmax
        # Wanted to have rules numbering from top to bottom
        var_list.reverse()
        self.var_list = var_list
        self.plot_map = {}
        self.data_map = {}
        self.plot_explist = []
        self.rect_list = []
        self.line_list = []
        self.setXRange(0, self.tmax)
        self.setYRange(0, 1.5*len(var_list))
        for j in range(0, len(var_list)):
            self.plot_explist.append(var_list[j])
            data_array = np.zeros(1000, dtype=np.float)
            self.data_map[j] = data_array
            self.rect_list.append([])
    
    def Init(self):
        vbox = self.getViewBox()
        for j in range(0, len(self.plot_explist)):
            for rect in self.rect_list[j]:
                vbox.removeItem(rect)
            self.data_map[j].fill(0.0)
            self.rect_list[j] = []

        for line in self.line_list:
            vbox.removeItem(line)
        self.line_list = []

    def Update(self, t, time_array, index):
        #print("Update ", t, index)
        vbox = self.getViewBox()
        for j in range(0, len(self.plot_explist)):
            if self.data_map[j].shape[0] <= index:
                self.data_map[j] = np.resize(self.data_map[j], self.data_map[j].shape[0] + 1000)
            arr = self.data_map[j]
            (method_name, obj) = self.plot_explist[j]
            arr[index] = getattr(obj, method_name)()
            #print("Update Plot ", j, arr[index])
            rects = self.rect_list[j]
            arr_val = int(arr[index])
            if index == 1:
                # Create labels
                rlabel = pg.TextItem(obj.FullName(), anchor=(1, 1), color=RULE_LABEL)
                rlabel.setPos(self.tmax, j*1.5)
                rlabel.setParentItem(vbox)
                vbox.addItem(rlabel)
                # Create line
                line = QtGui.QGraphicsLineItem(QtCore.QLineF(0.0, j*1.5, time_array[index], j*1.5))
                line.setPen(GRAY_LINE)
                vbox.addItem(line)
                self.line_list.append(line)
                # First index. Create rectangles
                if arr_val != 0:
                    rect = QtGui.QGraphicsRectItem(QtCore.QRectF(0, j*1.5, time_array[index], 1.0))
                    rect.setPen(QtGui.QPen(RULE_LINE_COLORS[(arr_val-1) % len(RULE_LINE_COLORS)], 0))
                    rect.setBrush(QtGui.QBrush(RULE_FILL_COLORS[(arr_val-1) % len(RULE_FILL_COLORS)]))
                    vbox.addItem(rect)
                    rects.append(rect)

            elif index > 1:
                arr_val1 = int(arr[index-1])
                line = self.line_list[j]
                line.setLine(0.0, j*1.5, time_array[index], j*1.5)
                if arr_val != 0:
                    if arr_val1 != 0 and len(rects) > 0:
                        rect = rects[-1]
                        rectf = rect.rect()
                        rectf.setRight(time_array[index])
                        rect.setRect(rectf)
                    if arr_val != arr_val1:
                        rect2 = QtGui.QGraphicsRectItem(QtCore.QRectF(time_array[index], j*1.5, 0.00001, 1.0))
                        rect2.setPen(QtGui.QPen(RULE_LINE_COLORS[(arr_val-1) % len(RULE_LINE_COLORS)], 0))
                        rect2.setBrush(QtGui.QBrush(RULE_FILL_COLORS[(arr_val-1) % len(RULE_FILL_COLORS)]))
                        vbox.addItem(rect2)
                        rects.append(rect2)
        

class PlotsWindow(QtGui.QWidget):
    run_simulation = QtCore.pyqtSignal(name='run_simulation')

    def __init__(self, variable_list, tmax):
        super(PlotsWindow, self).__init__()
        self.create(0,True,True)
        self.resize(1000, 800)
        l = QtGui.QVBoxLayout()
        l.setContentsMargins(0,0,0,0)
        l.setSpacing(2)
        self.setLayout(l)

        button_frame = QtGui.QFrame()
        button_frame.setFrameStyle(QtGui.QFrame.Raised | QtGui.QFrame.Panel)
        button_frame.setContentsMargins(2,2,2,2)
        l.addWidget(button_frame)

        l2 = QtGui.QHBoxLayout()
        l2.setContentsMargins(2,2,2,2)
        button_frame.setLayout(l2)
        run_button = QtGui.QPushButton("Run")
        run_button.clicked.connect(self.onRun)
        l2.addWidget(run_button)
        savecsv_button = QtGui.QPushButton("Save CSV")
        savecsv_button.clicked.connect(self.onSaveCSV)
        l2.addWidget(savecsv_button)
        savev_button = QtGui.QPushButton("Save Veusz")
        savev_button.clicked.connect(self.onSaveVeusz)
        l2.addWidget(savev_button)
        l2.addStretch()

        self.nplots = len(variable_list)
        self.variables = variable_list
        self.tmax = tmax
        self.expressions = []
        self.pw_list = []
        self.plot_map = {}
        self.time_array = np.zeros(1000, dtype=np.float)
        self.index = 0
        self.plot_widgets = []
        for i in range(0, self.nplots):
            (plot_type, variables) = self.variables[i]
            if plot_type == 1:
                plot_widget = DigitalPlotWidget('plot' + str(i), variables, tmax)
            else:
                plot_widget = AnalogPlotWidget('plot' + str(i), variables, tmax)
            l.addWidget(plot_widget)
            self.plot_widgets.append(plot_widget)

        self.show()

    def TimeStepUpdate(self, t):
        if self.time_array.shape[0] <= self.index:
            self.time_array = np.resize(self.time_array, self.time_array.shape[0] + 1000)
        self.time_array[self.index] = t
        tarr = self.time_array[0:self.index+1]
        for pw in self.plot_widgets:
            pw.Update(t, tarr, self.index)
        self.index += 1

    def onRun(self):
        self.time_array.fill(0.0)
        self.index = 0
        for pw in self.plot_widgets:
            pw.Init()
        self.run_simulation.emit()

    def onSaveCSV(self):
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save CSV", "plots.csv")
        if not fileName:
            return
        try:
            f = open(fileName, 'w')
        except:
            print("Cannot open file to write CSV plots ", fileName)
            return
        f.write("Time")
        for pw in self.plot_widgets:
            if isinstance(pw, AnalogPlotWidget):
                pw.WritePlotNames(f)
        f.write('\n')
        for i in range(self.index):
            f.write(str(self.time_array[i]))
            for pw in self.plot_widgets:
                if isinstance(pw, AnalogPlotWidget):
                    pw.WritePlotValues(f, i)
            f.write('\n')
        f.close()

    def onSaveVeusz(self):
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save Veusz", "plots.dat")
        if not fileName:
            return
        try:
            f = open(fileName, 'w')
        except:
            print("Cannot open file to write Veusz plots ", fileName)
            return
        f.write('descriptor Time\n')
        for i in range(self.index):
            f.write(str(self.time_array[i]) + '\n')
        for pw in self.plot_widgets:
            if isinstance(pw, AnalogPlotWidget):
                pw.WriteVeusz(f, self.index)
        f.close()


class AnalogTest:
    def __init__(self, name):
        self.time = 0.0
        self.name = name

    def FullName(self):
        return self.name

    def UpdateTime(self, t):
        self.time = t

    def V(self):
        return math.sin(self.time/100) + 0.1*random.random()

class DigitalTest:
    def __init__(self, name):
        self.time = 0.0
        self.name = name

    def FullName(self):
        return self.name

    def UpdateTime(self, t):
        self.time = t
        
    def Value(self):
        if self.time > 150 and self.time < 850:
            return math.floor(self.time/100.0)
        else:
            return 0


def update():
    global ctime, plotw, timer
    global a1, d1, d2

    if ctime >= 1000:
        timer.stop()
        return
    else:
        a1.UpdateTime(ctime)
        d1.UpdateTime(ctime)
        d2.UpdateTime(ctime)
        plotw.TimeStepUpdate(ctime)

    ctime += 1.0
            
if __name__ == '__main__':
    a1 = AnalogTest("data")
    d1 = DigitalTest("rule1")
    d2 = DigitalTest("rule2")
    
    app = QtGui.QApplication([])
    plotw = PlotsWindow([(1, [('Value', d1), ('Value', d2)]), (0, [('V', a1)])], 1000.0)

    ctime = 0.0
    timer = QtCore.QTimer()
    timer.timeout.connect(update)
    timer.start(1)

    QtGui.QApplication.instance().exec_()
    
    