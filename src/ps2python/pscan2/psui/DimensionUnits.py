# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt, QTextStream, 
        pyqtSignal, pyqtSlot)
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QWidget, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QVBoxLayout, QGroupBox, QGridLayout, 
        QStackedLayout, QLineEdit, QComboBox, QListWidget, QPushButton, QRadioButton, 
        QAbstractItemView, QButtonGroup)
from pscan2 import psglobals


class DimensionUnits(QWidget):
    setup_done = pyqtSignal(float, float, name='setup_done')

    def __init__(self):
        super(DimensionUnits, self).__init__()
        self.create(0, True, True)
        #self.resize(600, 600)

        self.param_set = set()
        self.param_map = {}

        l = QVBoxLayout()
        self.setLayout(l)

        l2 = QHBoxLayout()
        l.addLayout(l2)

        l2.addWidget(QLabel("Current units:"))
        self.current = QLineEdit(self)
        q=psglobals.Par2Units["I"]
        self.current.insert(str(psglobals.DimUnits[q]))
        l2.addWidget(self.current)
        l2.addWidget(QLabel(q))
        
        separator = QFrame()
        separator.setFrameShape(QFrame.VLine)
        separator.setFrameShadow(QFrame.Sunken)
        l2.addWidget(separator)

        l2.addWidget(QLabel("Voltage units:"))
        self.voltage = QLineEdit(self)
        q=psglobals.Par2Units["V"]
        self.voltage.insert(str(psglobals.DimUnits[q]))
        l2.addWidget(self.voltage)
        l2.addWidget(QLabel(q))

        separator = QFrame()
        separator.setFrameShape(QFrame.VLine)
        separator.setFrameShadow(QFrame.Sunken)
        l2.addWidget(separator)

        cancel_button = QPushButton("Cancel")
        l2.addWidget(cancel_button)
        cancel_button.clicked.connect(self.onCancel)
        done_button = QPushButton("Done")
        l2.addWidget(done_button)
        done_button.clicked.connect(self.onDone)

        separator = QFrame()
        separator.setFrameShape(QFrame.HLine)
        separator.setFrameShadow(QFrame.Sunken)
        l.addWidget(separator)

        l.addWidget(QLabel("Units:"))
        
        separator = QFrame()
        separator.setFrameShape(QFrame.HLine)
        separator.setFrameShadow(QFrame.Sunken)

        self.units_window = QLabel()
        l.addWidget(separator)
        l.addWidget(self.units_window)
        self.updateUnitsList()

        l.setAlignment(Qt.AlignTop)
        
        min_size = self.minimumSizeHint()
        if not min_size.isEmpty():
            self.resize(min_size.width(), 600)

    def bringUpFront(self):
        self.show()
        self.raise_()
        self.activateWindow()
        self.showNormal()

    def updateUnitsList(self):
        units_list = ""
        for param in psglobals.Par2Units:
            q=psglobals.Par2Units[param]
            units_list += "%s units:\t%e %s\n" % (param, psglobals.DimUnits[q], q)
        self.units_window.setText(units_list)

    @pyqtSlot()
    def onCancel(self):
        self.hide()

    @pyqtSlot()
    def onDone(self):
#        self.hide()
        try:
            Iunit = float(self.current.text())
        except:
            print("ERROR: Cannot get units of current")
            return
        try:
            Vunit = float(self.voltage.text())
        except:
            print("ERROR: Cannot get units of voltage")
            return
        q=psglobals.Par2Units["F"] # flux
        Funit = psglobals.DimUnits[q]
        q=psglobals.Par2Units["I"] # current
        psglobals.DimUnits[q] = Iunit
        q=psglobals.Par2Units["V"] # voltage
        psglobals.DimUnits[q] = Vunit
        q=psglobals.Par2Units["R"] # resistance
        psglobals.DimUnits[q] = Vunit/Iunit
        q=psglobals.Par2Units["L"] # inductance
        psglobals.DimUnits[q] = Funit/Iunit
        q=psglobals.Par2Units["T"] # time
        psglobals.DimUnits[q] = Funit/Vunit
        q=psglobals.Par2Units["C"] # capacitance
        psglobals.DimUnits[q] = Funit*Iunit/Vunit/Vunit
        self.updateUnitsList()
        

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    #app.setStyleSheet('QPushButton {min-width: 30px; }')
    mainWin = CalculateMarginsSetup(100)
    mainWin.addParameters(['p1', 'p2'])
    #mainWin.addValues(['v1', 'v2'])
    mainWin.show()
    sys.exit(app.exec_())
