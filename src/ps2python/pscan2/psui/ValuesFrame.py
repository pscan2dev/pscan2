# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import os

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt,
        QTextStream, QObject, pyqtSlot, pyqtSignal)
from PyQt5.QtGui import QIcon, QKeySequence, QPixmap
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QGroupBox, QTreeWidget, QTreeWidgetItem,
        QVBoxLayout, QGridLayout, QSpacerItem, QSizePolicy, QLineEdit, QCheckBox, QPushButton,
        QScrollArea)
 
from .SmallPushButton import * 
        
class ValuesFrame(QFrame):
    use_checked = pyqtSignal(list, name='use_checked')

    def __init__(self, internals_map, values_map, parent = None):
        super(ValuesFrame, self).__init__(parent)
        frame_layout = QVBoxLayout()
        self.setLayout(frame_layout)
        control_frame = QFrame()
        control_layout = QHBoxLayout()
        control_frame.setLayout(control_layout)
        
        path = os.path.dirname(os.path.abspath(__file__))

        pixmap = QPixmap(os.path.join(path,"images/checked_box.png"))
        icon = QIcon(pixmap)
        check_all_button = SmallIconPushButton(icon, None)
        control_layout.addWidget(check_all_button, Qt.AlignLeft)
        check_all_button.clicked.connect(self.onCheckAll)
        
        pixmap = QPixmap(os.path.join(path,"images/unchecked_box.png"))
        icon = QIcon(pixmap)
        uncheck_all_button = SmallIconPushButton(icon, None)
        control_layout.addWidget(uncheck_all_button, Qt.AlignLeft)
        uncheck_all_button.clicked.connect(self.onUncheckAll)
        
        pixmap = QPixmap(os.path.join(path,"images/use_checked.png"))
        icon = QIcon(pixmap)
        use_selection_button = QPushButton(icon, None)
        use_selection_button.setIconSize(pixmap.size())
        control_layout.addWidget(use_selection_button, Qt.AlignLeft)
        use_selection_button.clicked.connect(self.onUseChecked)
        
        frame_layout.addWidget(control_frame)

        pscroll = QScrollArea()
        frame_layout.addWidget(pscroll)

        parameters_frame = QFrame(pscroll)
        params_layout = QGridLayout()
        parameters_frame.setLayout(params_layout)

        pscroll.setWidget(parameters_frame)
        pscroll.setWidgetResizable(True)
        pscroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.value_widgets = {}
        nrow = 0
        self.param_selection = []
        internals_list = list(internals_map.values())
        internals_list.sort(key=lambda elem: elem.Name())
        for param in internals_list:
            param_label = QLabel(param.Name())
            params_layout.addWidget(param_label, nrow, 0, Qt.AlignLeft | Qt.AlignTop)
            param_value = QLabel(str(param.Value()))
            param_value.setFrameShape(QFrame.Box)
            params_layout.addWidget(param_value, nrow, 1, Qt.AlignLeft | Qt.AlignTop)
            self.value_widgets[param.Name()] = param_value
            param_sel = QCheckBox()
            params_layout.addWidget(param_sel, nrow, 2)
            self.param_selection.append(param_sel)
            nrow += 1
            
        values_list = list(values_map.values())
        values_list.sort(key=lambda elem: elem.Name())    
        for param in values_list:
            param_label = QLabel(param.Name())
            params_layout.addWidget(param_label, nrow, 0, Qt.AlignLeft | Qt.AlignTop)
            param_value = QLabel(str(param.Value()))
            param_value.setFrameShape(QFrame.Box)
            params_layout.addWidget(param_value, nrow, 1, Qt.AlignLeft | Qt.AlignTop)
            self.value_widgets[param.Name()] = param_value
            param_sel = QCheckBox()
            params_layout.addWidget(param_sel, nrow, 2)
            self.param_selection.append(param_sel)
            nrow += 1
            
        self.values_list = internals_list + values_list
            
        spacer = QSpacerItem(1,1, QSizePolicy.Expanding, QSizePolicy.Expanding)
        params_layout.addItem(spacer, nrow, 0)
        
        
    def onCheckAll(self):
        for n in range(len(self.param_selection)):
            sel = self.param_selection[n]
            sel.setChecked(True)
    
    def onUncheckAll(self):
        for n in range(len(self.param_selection)):
            sel = self.param_selection[n]
            sel.setChecked(False)
    
    def updateValues(self):
        for value in self.values_list:
            self.value_widgets[value.Name()].setText(str(value.Value()))
    
    def onUseChecked(self):
        selected_params = []
        for n in range(len(self.param_selection)):
            sel = self.param_selection[n]
            if sel.isChecked():
                selected_params.append(self.values_list[n])
        if len(selected_params) > 0:
            self.use_checked.emit(selected_params)
            