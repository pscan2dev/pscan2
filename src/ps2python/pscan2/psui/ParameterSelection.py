# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from PyQt5.QtCore import (QFileInfo, QPoint, QSettings, QDir)
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMessageBox,
	QTextEdit, QFrame, QHBoxLayout, QVBoxLayout, QGroupBox, QGridLayout,
	QDialog, QLabel, QPushButton, QLineEdit, QCheckBox)


class CircuitsSelection(QDialog):
    def __init__(self, circuit_list, parent=None):
        super(CircuitsSelection, self).__init__(parent)

        mainLayout = QVBoxLayout()

        dirLineFrame = QFrame()
        dirLine = QHBoxLayout()
        fileLabel = QLabel("Directory:")
        browseButton = self.createButton("&Browse...", self.browse)
        self.directoryName = QLineEdit()
        dirLine.addWidget(fileLabel)
        dirLine.addWidget(self.directoryName)
        dirLine.addWidget(browseButton)
        dirLineFrame.setLayout(dirLine)
        mainLayout.addWidget(dirLineFrame)

        circuitFrame = QGroupBox("Subcircuits")
        circuitGrid = QGridLayout()
        circuitGrid.setColumnStretch(0, 1)
        circuitGrid.setColumnStretch(1, 1)
        nline = 0
        circuitGrid.addWidget(QLabel("Subircuit"), nline, 0)
        circuitGrid.addWidget(QLabel("Filename"), nline, 1)
        circuitGrid.addWidget(QLabel("Process"), nline, 2)
        nline += 1
        self.fname_cbox_map = {}
        for c in circuit_list:
            cname = QLabel(c)
            fname = QLineEdit(c)
            check_box = QCheckBox()
            self.fname_cbox_map[c] = (fname, check_box)
            circuitGrid.addWidget(cname, nline, 0)
            circuitGrid.addWidget(fname, nline, 1)
            circuitGrid.addWidget(check_box, nline, 2)
            nline += 1

        circuitFrame.setLayout(circuitGrid)
        mainLayout.addWidget(circuitFrame)

        buttonsFrame = QFrame()
        buttonsLayout = QHBoxLayout()
        OKbutton = QPushButton("OK")
        Cancelbutton = QPushButton("Cancel")
        OKbutton.clicked.connect(self.accept)
        Cancelbutton.clicked.connect(self.reject)
        buttonsLayout.addStretch()
        buttonsLayout.addWidget(Cancelbutton)
        buttonsLayout.addWidget(OKbutton)
        buttonsFrame.setLayout(buttonsLayout)
        mainLayout.addWidget(buttonsFrame)

        self.setLayout(mainLayout)

        self.setWindowTitle("Save Parameters")
        #self.resize(700, 300)

    def browse(self):
        directory = QFileDialog.getExistingDirectory(self, "Select directory", QDir.currentPath())

        if directory:
            self.directoryName.setText(directory)

    def createButton(self, text, member):
        button = QPushButton(text)
        button.clicked.connect(member)
        return button

    def getResult(self):
        dir = self.directoryName.text()
        resmap = {}
        for (cname, cwidgets) in self.fname_cbox_map.items():
            if cwidgets[1].isChecked():
                resmap[cname] = cwidgets[0].text()
        return(dir, resmap)

    # static method to create the dialog and return (dir, selection, accepted)
    @staticmethod
    def getSelection(circuit_list, parent = None):
        dialog = CircuitsSelection(circuit_list, parent)
        result = dialog.exec_()
        (dir, fmap) = dialog.getResult()
        return (dir, fmap, result == QDialog.Accepted)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    res = CircuitsSelection.getSelection(['s1', 's2'], None)
    print(res)
    sys.exit(app.exec_())
