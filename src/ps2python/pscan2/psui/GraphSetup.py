# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import json

from pscan2 import find

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt, QTextStream, 
        pyqtSignal, pyqtSlot)
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QWidget, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QVBoxLayout, QGroupBox, QGridLayout, 
        QStackedLayout, QLineEdit, QComboBox, QListWidget, QPushButton, QRadioButton, 
        QAbstractItemView, QButtonGroup, QScrollArea, QCheckBox)

class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return json.JSONEncoder.default(self, obj)        
        
class ClickableFrame(QFrame):
    frame_selected = pyqtSignal(object, name='frame_selected')

    def __init__(self, parent):
        super(ClickableFrame, self).__init__(parent)
        
    def mouseReleaseEvent(self, ev):
        self.frame_selected.emit(self)
        
        
class PlotsSetup(QFrame):
    return_items = pyqtSignal(int, list, name='return_items')

    def __init__(self, parent = None, plots_setup = [[0, []], [1, []]]):
        super(PlotsSetup, self).__init__(parent)
        self.frame_counter = 0
        l = QVBoxLayout()
        self.setLayout(l)

        self.plot_frames = []
        self.nplots = 0
        
        self.restorePlotsSetup(plots_setup)

    def createPlotFrame(self, graph_type):
        plot_frame = ClickableFrame(self)
        plot_frame.setFrameStyle(QFrame.Panel | QFrame.Plain)
        frame_name = "plots_frame_" + str(self.frame_counter)
        self.frame_counter += 1
        plot_frame.setObjectName(frame_name)
        plot_frame.setStyleSheet("#{} {{ border: 1px solid black; }}".format(frame_name))
        self.plot_frames.append(plot_frame)
        plot_frame.frame_selected.connect(self.activateFrame)

        l2 = QVBoxLayout()
        plot_frame.setLayout(l2)
        bg = QButtonGroup(plot_frame)
        plot_frame.type_button_group = bg
        l3 = QHBoxLayout()
        l2.addLayout(l3)
        analog_rb = QRadioButton("Values")
        l3.addWidget(analog_rb)
        digital_rb = QRadioButton("Rules")
        if graph_type == 0:
            analog_rb.setChecked(True)
            plot_frame.graph_type = graph_type
        else:
            digital_rb.setChecked(True)
            plot_frame.graph_type = graph_type
        analog_rb.setEnabled(False)
        digital_rb.setEnabled(False)
        l3.addWidget(digital_rb)
        bg.addButton(analog_rb)
        bg.addButton(digital_rb)
        bg.setId(analog_rb, 0)
        bg.setId(digital_rb, 1)
        bg.buttonClicked['int'].connect(lambda btnid, frame=plot_frame: self.typeChangedCB(btnid, frame))
        plot_frame.analog_rb = analog_rb
        plot_frame.digital_rb = digital_rb

        plot_delete_button = QPushButton("Delete")
        l3.addWidget(plot_delete_button)
        plot_delete_button.clicked.connect(lambda checked, frame=plot_frame: self.onDeletePlot(checked, frame))
        plot_delete_button.setEnabled(False)
        plot_frame.delete_button = plot_delete_button

        plot_list = QListWidget(plot_frame)
        plot_list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        plot_list.setDragDropMode(QAbstractItemView.InternalMove)
        l2.addWidget(plot_list)
        plot_frame.list_widget = plot_list

        self.layout().addWidget(plot_frame)
        self.nplots += 1

        return plot_frame

    @pyqtSlot(bool, object)
    def onDeletePlot(self, checked, frame):
        #print("Delete plot frame ", frame.objectName())

        if self.nplots == 1:
            # Cannot delete last plot frame
            return

        if frame != self.active_frame:
            # Something strange. Delete button should be enabled ONLY on active plot frame
            return

        btnid = frame.type_button_group.checkedId()

        removed_items_list = []
        for i in range(0, frame.list_widget.count()):
            item = frame.list_widget.item(i)
            removed_items_list.append(item.text())

        if btnid == 0:
            btnid = 1
        else:
            btnid = 0

        self.plot_frames.remove(frame)
        self.active_frame = self.plot_frames[0]
        self.activateFrame(self.active_frame)
        self.nplots -= 1

        self.layout().removeWidget(frame)
        frame.deleteLater()

        if len(removed_items_list) > 0:
            # Emit a signal
            self.return_items.emit(btnid, removed_items_list)


    @pyqtSlot(int, object)
    def typeChangedCB(self, btnid, frame):
        #print("typeChangedCB", btnid, frame.objectName())
        if frame != self.active_frame:
            return

        # Nothing changes
        if frame.graph_type == btnid:
            return

        frame.graph_type = btnid

        removed_items_list = []
        for i in range(0, frame.list_widget.count()):
            item = frame.list_widget.item(i)
            removed_items_list.append(item.text())

        frame.list_widget.clear()

        if len(removed_items_list) > 0:
            # Emit a signal
            self.return_items.emit(btnid, removed_items_list)

    @pyqtSlot(object)
    def activateFrame(self, frame):
        frame_name = self.active_frame.objectName()
        self.active_frame.setStyleSheet("#{} {{ border: 1px solid black; }}".format(frame_name))
        self.active_frame.analog_rb.setEnabled(False)
        self.active_frame.digital_rb.setEnabled(False)
        self.active_frame.delete_button.setEnabled(False)

        self.active_frame = frame
        frame_name = self.active_frame.objectName()
        self.active_frame.setStyleSheet("#{} {{ border: 2px solid blue; }}".format(frame_name))
        self.active_frame.analog_rb.setEnabled(True)
        self.active_frame.digital_rb.setEnabled(True)
        self.active_frame.delete_button.setEnabled(True)


    #@pyqtSlot()
    def getActiveFrameType(self):
        type_bg = self.active_frame.type_button_group
        return type_bg.checkedId()

    def addToActiveFrame(self, item_list):
        active_list = self.active_frame.list_widget
        active_list.addItems(item_list)

    def removeFromActiveFrame(self):
        selected_items = self.active_frame.list_widget.selectedItems()
        removed_list = []
        if len(selected_items) > 0:
            for item in selected_items:
                index = self.active_frame.list_widget.row(item)
                self.active_frame.list_widget.takeItem(index)
                removed_list.append(item.text())
        return((self.getActiveFrameType(), removed_list))


    def addPlot(self):
        self.createPlotFrame(self.nplots % 2)

    def getPlotsSetup(self):
        result = []
        for frame in self.plot_frames:
            btnid = frame.type_button_group.checkedId()
            items_list = []
            for i in range(0, frame.list_widget.count()):
                item = frame.list_widget.item(i)
                items_list.append(item.text())
            if len(items_list) > 0:
                result.append([btnid, items_list])

        return result

    def restorePlotsSetup(self, config_list):
        if len(self.plot_frames) > 0:
            for frame in self.plot_frames:
                self.layout().removeWidget(frame)
                frame.deleteLater()

            self.plot_frames.clear()

        for [btnid, items_list] in config_list:
            frame = self.createPlotFrame(btnid)
            frame.list_widget.addItems(items_list)
            
        self.nplots = len(config_list)
        self.active_frame = self.plot_frames[0]
        self.activateFrame(self.active_frame)
        

class GraphSetup(QWidget):
    setup_done = pyqtSignal(float, bool, list, name='setup_done')

    def __init__(self, simulation_time = 100.0):
        super(GraphSetup, self).__init__()
        self.create(0, True, True)
        self.resize(600, 800)
        self.num_windows = 2

        self.rules_map = {}
        self.values_map = {}
        self.unasigned_rules = set()
        self.unasigned_values = set()

        l = QVBoxLayout()
        self.setLayout(l)

        l2 = QHBoxLayout()
        l.addLayout(l2)
        l2.addWidget(QLabel("Simulation Time:"))
        self.sim_time = QLineEdit(self)
        self.sim_time.insert(str(simulation_time))
        l2.addWidget(self.sim_time)
        
        self.stop_on_error_checkbox = QCheckBox("Stop On Error")
        self.stop_on_error_checkbox.setCheckState(0)
        l2.addWidget(self.stop_on_error_checkbox)
        
        add_plot_button = QPushButton("Add plot")
        l2.addWidget(add_plot_button)
        add_plot_button.clicked.connect(self.onAddPlot)

        separator = QFrame()
        separator.setFrameShape(QFrame.VLine)
        separator.setFrameShadow(QFrame.Sunken)
        l2.addWidget(separator)

        save_button = QPushButton("Save")
        l2.addWidget(save_button)
        save_button.clicked.connect(self.onSave)
        load_button = QPushButton("Load")
        l2.addWidget(load_button)
        load_button.clicked.connect(self.onLoad)

        separator = QFrame()
        separator.setFrameShape(QFrame.VLine)
        separator.setFrameShadow(QFrame.Sunken)
        l2.addWidget(separator)

        cancel_button = QPushButton("Cancel")
        l2.addWidget(cancel_button)
        cancel_button.clicked.connect(self.onCancel)
        done_button = QPushButton("Done")
        l2.addWidget(done_button)
        done_button.clicked.connect(self.onDone)

        l4 = QHBoxLayout()
        l.addLayout(l4)

        l5 = QVBoxLayout()
        l4.addLayout(l5)

        rules_box = QGroupBox()
        rules_box_layout = QVBoxLayout()
        rules_box.setLayout(rules_box_layout)
        rules_box.setTitle("Rules")
        self.plot_rules_list = QListWidget(rules_box)
        self.plot_rules_list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        #self.plot_rules_list.insertItems(0, ["r1", "r2", "r3"])
        rules_box_layout.addWidget(self.plot_rules_list)
        l5.addWidget(rules_box)

        values_box = QGroupBox()
        values_box_layout = QVBoxLayout()
        values_box.setLayout(values_box_layout)
        values_box.setTitle("Values")
        self.plot_values_list = QListWidget(values_box)
        self.plot_values_list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        #self.plot_values_list.insertItems(0, ["v1", "v2", "v3"])
        values_box_layout.addWidget(self.plot_values_list)
        l5.addWidget(values_box)

        move_box_layout = QVBoxLayout()
        l4.addLayout(move_box_layout)

        move_box_layout.addStretch(1)

        move_items_button = QPushButton("->")
        move_box_layout.addWidget(move_items_button)
        move_items_button.clicked.connect(self.onMoveItems)

        move_back_items_button = QPushButton("<-")
        move_box_layout.addWidget(move_back_items_button)
        move_back_items_button.clicked.connect(self.onMoveBackItems)

        move_box_layout.addStretch(1)

        pscroll = QScrollArea()
        l4.addWidget(pscroll)

        self.plots_setup = PlotsSetup(self)
        self.plots_setup.return_items.connect(self.onReturnItems)

        pscroll.setWidget(self.plots_setup)
        pscroll.setWidgetResizable(True)
        pscroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)


    @pyqtSlot()
    def onAddPlot(self):
        self.plots_setup.addPlot()

    @pyqtSlot()
    def onSave(self):
        config_str = self.serialize()
        with open('gconfig.json', mode='w') as fp:
            fp.write(config_str)

    @pyqtSlot()
    def onLoad(self):
        try:
            with open('gconfig.json', mode='r') as fp:
                json_str = fp.read()
                self.deserialize(json_str)
        except:
            print("ERROR: Cannot load graph setup from file gconfig.json")

    @pyqtSlot()
    def onCancel(self):
        self.hide()

    @pyqtSlot()
    def onDone(self):
        self.hide()
        try:
            simulation_time = float(self.sim_time.text())
        except:
            print("ERROR: Cannot get simulation time")
            return
            
        result = []
        for (btnid, item_list) in self.plots_setup.getPlotsSetup():
            if btnid == 1:
                # Rules
                rlist = []
                for rname in item_list:
                    rlist.append(self.rules_map[rname])
                result.append([btnid, rlist])
            elif btnid == 0:
                # Values
                vlist = []
                for vname in item_list:
                    vlist.append(self.values_map[vname])
                result.append([btnid, vlist])

        self.setup_done.emit(simulation_time, 
            self.stop_on_error_checkbox.checkState() != 0, 
            result)

    @pyqtSlot()
    def onMoveItems(self):
        active_plots_type = self.plots_setup.getActiveFrameType()
        if active_plots_type == 0:
            # values (analog)
            moved_items = self.removeSelectedValues()
        elif active_plots_type == 1:
            # rules (digital)
            moved_items = self.removeSelectedRules()
        self.plots_setup.addToActiveFrame(moved_items)

    @pyqtSlot()
    def onMoveBackItems(self):
        (btnid, item_list) = self.plots_setup.removeFromActiveFrame()
        if btnid == 0:
            self.returnValues(item_list)
        else:
            self.returnRules(item_list)

    @pyqtSlot(int, list)
    def onReturnItems(self, btnid, item_list):
        if btnid == 0:
            self.returnRules(item_list)
        else:
            self.returnValues(item_list)

    def stopOnError(self):
        return self.stop_on_error

    def returnRules(self, rname_list):
        for rname in rname_list:
            self.unasigned_rules.add(rname)

        self.showUnassignedRules()

    def addRules(self, new_rule_list):
        for (objname, fname, obj) in new_rule_list:
            self.rules_map[objname] = (fname, obj)
            self.unasigned_rules.add(objname)
        
        self.showUnassignedRules()
        self.bringUpFront()

    def showUnassignedRules(self):
        rule_list = list(self.unasigned_rules)
        rule_list.sort()
        self.plot_rules_list.clear()
        self.plot_rules_list.insertItems(0, rule_list)

    def removeRules(self, rule_names_list):
        for rname in rule_names_list:
            del self.rules_map[rname]
            self.unasigned_rules.discard(rname)

        self.showUnassignedRules()

    def removeSelectedRules(self):
        selected_items = self.plot_rules_list.selectedItems()
        removed_rule_list = []
        if len(selected_items) > 0:
            for item in selected_items:
                (fname, obj) = self.rules_map[item.text()]
                self.unasigned_rules.remove(item.text())
                removed_rule_list.append(item.text())
        
            self.showUnassignedRules()
        return removed_rule_list
        
    def returnValues(self, vname_list):
        for vname in vname_list:
            self.unasigned_values.add(vname)

        self.showUnassignedValues()

    def showUnassignedValues(self):
        values_list = list(self.unasigned_values)
        values_list.sort()
        self.plot_values_list.clear()
        self.plot_values_list.insertItems(0, values_list)

    def addValues(self, new_value_list):
        for (objname, fname, obj) in new_value_list:
            self.values_map[objname] = (fname, obj)
            self.unasigned_values.add(objname)
            
        self.showUnassignedValues()
        self.bringUpFront()
        
    def removeValues(self, value_names_list):
        for vname in value_names_list:
            del self.values_map[vname]
            self.unasigned_values.discard(vname)
        
        self.showUnassignedValues()
        
    def removeSelectedValues(self):
        removed_value_list = []
        selected_items = self.plot_values_list.selectedItems()
        if len(selected_items) > 0:
            for item in selected_items:
                (fname, obj) = self.values_map[item.text()]
                self.unasigned_values.remove(item.text())
                removed_value_list.append(item.text())

            self.showUnassignedValues()
        return removed_value_list
        
    def addParameters(self, plist):
        pass

    def addExternals(self, elist):
        pass
        
    def addInternals(self, elist):
        pass
        
    def bringUpFront(self):
        self.show()
        self.raise_()
        self.activateWindow()
        self.showNormal()

    def serialize(self):
        plots_config = self.plots_setup.getPlotsSetup()
        rules_list = []
        for [func, obj] in self.rules_map.values():
            rules_list.append([obj.FullName(), func])

        values_list = []
        for [func, obj] in self.values_map.values():
            values_list.append([obj.FullName(), obj.FindType(), func])


        cfg = [rules_list, values_list, self.unasigned_rules, self.unasigned_values, plots_config]
        return json.dumps(cfg, cls=SetEncoder)

    def deserialize(self, json_str):
        lst = json.loads(json_str)
        if len(lst) != 5:
            raise("Wrong JSON string: " + json_str)

        self.values_map = {}
        self.rules_map = {}
        self.unasigned_rules = set()
        self.unasigned_values = set()

        for rname,func in lst[0]:
            rule = find(rname, 'r')
            self.rules_map[rname] = (func, rule)
            
        for vname,ftype,func in lst[1]:
            try:
                obj = find(vname, ftype)
                name = func + '(' + vname + ')'
                if ftype == 'e' or ftype == 'n':
                    self.values_map[name] = (func, obj)
                else:
                    self.values_map[vname] = (func, obj)
            except:
                pass

        for rname in lst[2]:
            if rname in self.rules_map:
                self.unasigned_rules.add(rname)
            else:
                pass

        for vname in lst[3]:
            if vname in self.values_map:
                self.unasigned_values.add(vname)
            else:
                pass

        self.showUnassignedValues()
        self.showUnassignedRules()

        self.plots_setup.restorePlotsSetup(lst[4])

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    #app.setStyleSheet('QPushButton {min-width: 30px; }')
    mainWin = GraphSetup()
    mainWin.addRules(['r1', 'r2'])
    mainWin.addValues(['v1', 'v2'])
    mainWin.show()
    sys.exit(app.exec_())        
    