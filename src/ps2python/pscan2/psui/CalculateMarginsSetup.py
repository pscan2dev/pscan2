# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)
# ak 20190910

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt, QTextStream, 
        pyqtSignal, pyqtSlot)
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QWidget, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QVBoxLayout, QGroupBox, QGridLayout, 
        QStackedLayout, QLineEdit, QComboBox, QListWidget, QPushButton, QRadioButton, 
        QAbstractItemView, QButtonGroup)
from pscan2.util import find


class CalculateMarginsSetup(QWidget):
    setup_done = pyqtSignal(float, list, float, name='setup_done')
    FileConfigName = "margins_menue.dat"
    
    def __init__(self, simulation_time = 100.0, max_margin=0.4):
        super(CalculateMarginsSetup, self).__init__()
        self.create(0, True, True)
        #self.resize(600, 600)

        self.param_set = set()
        self.param_map = {}

        l = QVBoxLayout()
        self.setLayout(l)

        l2 = QHBoxLayout()
        l.addLayout(l2)
        l2.addWidget(QLabel("Simulation Time:"))
        self.sim_time = QLineEdit(self)
        self.sim_time.insert(str(simulation_time))
        l2.addWidget(self.sim_time)

        separator = QFrame()
        separator.setFrameShape(QFrame.VLine)
        separator.setFrameShadow(QFrame.Sunken)
        l2.addWidget(separator)

        l2.addWidget(QLabel("Maximum margins:"))
        self.max_marg = QLineEdit(self)
        self.max_marg.insert(str(max_margin))
        l2.addWidget(self.max_marg)

        separator = QFrame()
        separator.setFrameShape(QFrame.VLine)
        separator.setFrameShadow(QFrame.Sunken)
        l2.addWidget(separator)

        save_button = QPushButton("Save")
        l2.addWidget(save_button)
        save_button.clicked.connect(self.onSave)
        load_button = QPushButton("Load")
        l2.addWidget(load_button)
        load_button.clicked.connect(self.onLoad)
        done_button = QPushButton("Done")
        l2.addWidget(done_button)
        done_button.clicked.connect(self.onDone)

        param_box = QGroupBox()
        param_box_layout = QVBoxLayout()
        param_box.setLayout(param_box_layout)
        param_box.setTitle("Parameters")
        self.param_list = QListWidget(param_box)
        self.param_list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.param_list.setDragDropMode(QAbstractItemView.InternalMove)
        self.fname=CalculateMarginsSetup.FileConfigName

        #self.plot_rules_list.insertItems(0, ["r1", "r2", "r3"])
        param_box_layout.addWidget(self.param_list)

        l3 = QHBoxLayout()
        param_box_layout.addLayout(l3)

        l3.addStretch()
        remove_button = QPushButton("Remove")
        l3.addWidget(remove_button)
        remove_button.clicked.connect(self.onRemoveParameters)

        l.addWidget(param_box)

        min_size = self.minimumSizeHint()
        if not min_size.isEmpty():
            self.resize(min_size.width(), 600)

    def bringUpFront(self):
        self.show()
        self.raise_()
        self.activateWindow()
        self.showNormal()

    def addParameters(self, param_list):
        self.param_set |= set(param_list)
        self.updateParamList()
        self.bringUpFront()

    def addExternals(self, elist):
        self.addParameters(elist)
        
    def addInternals(self, elist):
        pass

    def addValues(self, elist):
        pass

    def addRules(self, elist):
        pass        

    def updateParamList(self):
        param_list = []
        for param in self.param_set:
            self.param_map[param.FullName()] = param
            param_list.append(param.FullName())
        param_list.sort()
        self.param_list.clear()
        self.param_list.insertItems(0, param_list)

    @pyqtSlot()
    def onRemoveParameters(self):
        selected_items = self.param_list.selectedItems()
        if len(selected_items) > 0:
            selected_params = []
            for item in selected_items:
                selected_params.append(self.param_map[item.text()])
            self.param_set -= set(selected_params)
            self.updateParamList()

    @pyqtSlot()
    def onSave(self):
        fp=open(self.fname,"w")
        for i in range(self.param_list.count()):
            item = self.param_list.item(i)
            fp.write(item.text()+"\n")
        fp.close()

    @pyqtSlot()
    def onLoad(self):
        try:
            fp=open(self.fname,"r")
        except:
            return
        param_list = [line.strip() for line in fp.readlines()]
        fp.close()
        for param_name in param_list.copy():
            try:
                par = find(param_name)
            except:
                print("ERROR: Cannot find parameter " + param_name)
                param_list.remove(param_name)
            else:
                self.param_map[param_name] = par
        self.param_list.clear()
        self.param_list.insertItems(0, param_list)

    @pyqtSlot()
    def onDone(self):
        self.hide()
        try:
            simulation_time = float(self.sim_time.text())
        except:
            print("ERROR: Cannot get simulation time")
            return
        try:
            max_margin = float(self.max_marg.text())
        except:
            print("ERROR: Cannot get maximum margins parameter")
            return
        param_list = []
        for i in range(self.param_list.count()):
            item = self.param_list.item(i)
            param_list.append(self.param_map[item.text()])
        self.setup_done.emit(simulation_time, param_list, max_margin)

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    #app.setStyleSheet('QPushButton {min-width: 30px; }')
    mainWin = CalculateMarginsSetup(100)
    mainWin.addParameters(['p1', 'p2'])
    #mainWin.addValues(['v1', 'v2'])
    mainWin.show()
    sys.exit(app.exec_())
