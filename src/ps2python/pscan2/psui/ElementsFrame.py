# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import os

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt,
        QTextStream, QObject, pyqtSlot, pyqtSignal)
from PyQt5.QtGui import QIcon, QKeySequence, QPixmap
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QGroupBox, QTreeWidget, QTreeWidgetItem,
        QVBoxLayout, QGridLayout, QSpacerItem, QSizePolicy, QLineEdit, QCheckBox, QPushButton,
        QScrollArea)
        
from .SmallPushButton import *
        
class ElementsFrame(QFrame):
    use_checked = pyqtSignal(list, name='use_checked')

    def __init__(self, elements_map, parent=None):
        super(ElementsFrame, self).__init__(parent)
        frame_layout = QVBoxLayout()
        self.setLayout(frame_layout)
        control_frame = QFrame()
        control_layout = QHBoxLayout()
        control_frame.setLayout(control_layout)
        path = os.path.dirname(os.path.abspath(__file__))
        pixmap = QPixmap(os.path.join(path,"images/use_checked.png"))
        icon = QIcon(pixmap)
        use_selection_button = SmallIconPushButton(icon)
        use_selection_button.setIconSize(pixmap.size())
        use_selection_button.clicked.connect(self.onUseChecked)

        control_layout.addWidget(use_selection_button)
        control_layout.setAlignment(use_selection_button, Qt.AlignHCenter)
        frame_layout.addWidget(control_frame)

        pscroll = QScrollArea()
        frame_layout.addWidget(pscroll)

        parameters_frame = QFrame(pscroll)
        params_layout = QGridLayout()
        parameters_frame.setLayout(params_layout)

        pscroll.setWidget(parameters_frame)
        pscroll.setWidgetResizable(True)
        pscroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        nrow = 0
        self.param_selection = {}
        self.param_selection['V'] = []
        self.param_selection['I'] = []
        self.param_selection['P'] = []
        self.element_list = list(elements_map.values())
        self.element_list.sort(key=lambda elem: elem.Name())
        if len(self.element_list) > 0:
            v_button = SmallTextPushButton("V")
            params_layout.addWidget(v_button, nrow, 1, Qt.AlignLeft)
            i_button = SmallTextPushButton("I")
            params_layout.addWidget(i_button, nrow, 2, Qt.AlignLeft)
            p_button = SmallTextPushButton("P")
            params_layout.addWidget(p_button, nrow, 3, Qt.AlignLeft)
            nrow += 1
            for param in self.element_list:
                param_label = QLabel(param.Name())
                params_layout.addWidget(param_label, nrow, 0, Qt.AlignLeft | Qt.AlignVCenter)
                param_sel = QCheckBox()
                params_layout.addWidget(param_sel, nrow, 1, Qt.AlignLeft)
                self.param_selection['V'].append(param_sel)
                param_sel = QCheckBox()
                params_layout.addWidget(param_sel, nrow, 2, Qt.AlignLeft)
                self.param_selection['I'].append(param_sel)
                param_sel = QCheckBox()
                params_layout.addWidget(param_sel, nrow, 3, Qt.AlignLeft)
                self.param_selection['P'].append(param_sel)
                nrow += 1
        spacer = QSpacerItem(1, 1, QSizePolicy.Minimum, QSizePolicy.Expanding)
        params_layout.addItem(spacer, nrow, 0)

    def onUseChecked(self):
        selected_params = []
        for ftype in ['V', 'I', 'P']:
            param_list = self.param_selection[ftype]
            for n in range(len(param_list)):
                sel = param_list[n]
                if sel.isChecked():
                    selected_params.append((ftype, self.element_list[n]))
        if len(selected_params) > 0:
            self.use_checked.emit(selected_params)

            