# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from PyQt5.QtCore import (QFileInfo, QPoint, QSettings, QDir)
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMessageBox,
	QTextEdit, QFrame, QHBoxLayout, QVBoxLayout, QGroupBox, QGridLayout,
	QDialog, QLabel, QPushButton, QLineEdit, QCheckBox)


class CircuitsSelection(QDialog):
    def __init__(self, circuit_list, parent=None):
        super(CircuitsSelection, self).__init__(parent)

        mainLayout = QVBoxLayout()

        # dirLineFrame = QFrame()
        # dirLine = QHBoxLayout()
        # fileLabel = QLabel("Directory:")
        # browseButton = self.createButton("&Browse...", self.browse)
        # self.directoryName = QLineEdit()
        # dirLine.addWidget(fileLabel)
        # dirLine.addWidget(self.directoryName)
        # dirLine.addWidget(browseButton)
        # dirLineFrame.setLayout(dirLine)
        # mainLayout.addWidget(dirLineFrame)

        circuitFrame = QGroupBox("Subcircuits")
        circuitGrid = QGridLayout()
        circuitGrid.setColumnStretch(0, 1)
        nline = 0
        circuitGrid.addWidget(QLabel("<b>Subircuit</b>"), nline, 0)
        circuitGrid.addWidget(QLabel("<b>Process</b>"), nline, 1)
        nline += 1
        self.cbox_map = {}
        for c in circuit_list:
            cname = QLabel(c)
            check_box = QCheckBox()
            self.cbox_map[c] = check_box
            circuitGrid.addWidget(cname, nline, 0)
            circuitGrid.addWidget(check_box, nline, 1)
            nline += 1

        circuitFrame.setLayout(circuitGrid)
        mainLayout.addWidget(circuitFrame)

        buttonsFrame = QFrame()
        buttonsLayout = QHBoxLayout()
        OKbutton = QPushButton("OK")
        Cancelbutton = QPushButton("Cancel")
        OKbutton.clicked.connect(self.accept)
        Cancelbutton.clicked.connect(self.reject)
        buttonsLayout.addStretch()
        buttonsLayout.addWidget(Cancelbutton)
        buttonsLayout.addWidget(OKbutton)
        buttonsFrame.setLayout(buttonsLayout)
        mainLayout.addWidget(buttonsFrame)

        self.setLayout(mainLayout)

        self.setWindowTitle("Select Circuits")
        #self.resize(700, 300)

    # def browse(self):
    #     directory = QFileDialog.getExistingDirectory(self, "Select directory", QDir.currentPath())

    #     if directory:
    #         self.directoryName.setText(directory)

    def createButton(self, text, member):
        button = QPushButton(text)
        button.clicked.connect(member)
        return button

    def getResult(self):
        reslist = []
        for (cname, cbox) in self.cbox_map.items():
            if cbox.isChecked():
                reslist.append(cname)
        return reslist

    # static method to create the dialog and return (selection, accepted)
    @staticmethod
    def getSelection(circuit_list, parent = None):
        dialog = CircuitsSelection(circuit_list, parent)
        result = dialog.exec_()
        clist = dialog.getResult()
        return (clist, result == QDialog.Accepted)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    res = CircuitsSelection.getSelection(['s1', 's2'], None)
    print(res)
    sys.exit(app.exec_())
