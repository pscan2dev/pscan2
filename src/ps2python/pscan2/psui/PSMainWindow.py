# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from pscan2 import *
from pscan2 import psglobals
from pscan2.Simulation import TimeStep, InitSimulation
import os, sys, numpy, time


from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt,
        QTextStream, pyqtSignal, pyqtSlot, QTimer)
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QWidget,
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QGroupBox, QGridLayout, QStackedLayout)

from .CircuitTreeWidget import *
from .GraphSetup import GraphSetup
from .PlotsWindow import PlotsWindow
from .CircuitsSelection import CircuitsSelection
from .DimensionUnits import DimensionUnits
from .CalculateMarginsSetup import CalculateMarginsSetup
from .OptimizeSetup import OptimizeSetup

from . import psui_rc

def eventUpdateCallback():
    QApplication.instance().processEvents()

class PSMainWindow(QMainWindow):
    sequenceNumber = 1
    windowList = []

    def __init__(self, fileName=None):
        global psglobals

        super(PSMainWindow, self).__init__()
        
        psglobals.GUICallback = eventUpdateCallback

        self.graph_setup_window = None
        self.margins_setup_window = None
        self.MarginsReportFname = "MarginsReport.txt"
        self.set_units_window = None
        self.optimize_setup_window = None
        self.active_setup_window = None

        self.stop_execution_flag = False
        
        self.simulation_time = self.getSimulationTime()
        
        self.simulation_stop_on_error = True

        self.init()
        if fileName:
            self.loadFile(fileName)
        else:
            self.setCurrentFile('')

    def closeEvent(self, event):
        pass

    def newFile(self):
        other = MainWindow()
        MainWindow.windowList.append(other)
        other.move(self.x() + 40, self.y() + 40)
        other.show()

    def open(self):
        fileName, _ = QFileDialog.getOpenFileName(self)
        if fileName:
            load_all_parameters(fileName)
            self.circuit_tree.UpdateParameters()

    def save(self):
        fileName, _ = QFileDialog.getSaveFileName(self, "Save As", self.curFile)
        if not fileName:
            return False

        return self.saveFile(fileName)

    def saveParameters(self):
        circuit_list = list(psglobals.CircuitScriptMap.keys())
        (circuit_selection, result) = CircuitsSelection.getSelection(circuit_list, self)
        if result:
            save_parameters(circuit_selection)
        

    def about(self):
        QMessageBox.about(self, "About PSCAN2",
                "The <b>PSCAN2</b> is a superconductor circuit analyzer")

    def documentWasModified(self):
        self.setWindowModified(True)

    def init(self):
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.isUntitled = True
        self.mainFrame = QFrame()
        self.setCentralWidget(self.mainFrame)
        layout = QHBoxLayout()
        self.mainFrame.setLayout(layout)
    
        self.circuitTreeFrame = QGroupBox(self.mainFrame)
        self.circuitTreeFrame.setTitle("Circuit")
        layout.addWidget(self.circuitTreeFrame)
        
        self.parametersFrame = QGroupBox(self.mainFrame)
        self.parametersFrame.setTitle("Parameters")
        layout.addWidget(self.parametersFrame)
        self.parametersLayout = QStackedLayout()
        self.parametersFrame.setLayout(self.parametersLayout)
        
        self.externalsFrame = QGroupBox(self.mainFrame)
        self.externalsFrame.setTitle("Externals")
        layout.addWidget(self.externalsFrame)
        self.externalsLayout = QStackedLayout()
        self.externalsFrame.setLayout(self.externalsLayout)
        
        self.intvalueFrame = QGroupBox(self.mainFrame)
        self.intvalueFrame.setTitle("Internals && Values")
        layout.addWidget(self.intvalueFrame)
        self.valuesLayout = QStackedLayout()
        self.intvalueFrame.setLayout(self.valuesLayout)
        
        self.elementsFrame = QGroupBox(self.mainFrame)
        self.elementsFrame.setTitle("Elements")
        layout.addWidget(self.elementsFrame)
        self.elementsLayout = QStackedLayout()
        self.elementsFrame.setLayout(self.elementsLayout)

        self.rulesFrame = QGroupBox(self.mainFrame)
        self.rulesFrame.setTitle("Rules")
        layout.addWidget(self.rulesFrame)
        self.rulesLayout = QStackedLayout()
        self.rulesFrame.setLayout(self.rulesLayout)
        
        self.circuit_tree = CircuitTreeWidget(self.circuitTreeFrame, self.parametersLayout,
            self.externalsLayout, self.valuesLayout, self.elementsLayout, self.rulesLayout)
        self.circuit_tree.use_checked.connect(self.onUseSelected)

        ct_layout = QGridLayout()
        self.circuitTreeFrame.setLayout(ct_layout)
        ct_layout.addWidget(self.circuit_tree, 0, 0)
        
        self.createActions()
        self.createMenus()
        self.createToolBars()
        self.createStatusBar()

        self.readSettings()
        

    def createActions(self):
        path = os.path.dirname(os.path.abspath(__file__))

        self.openAct = QAction(QIcon(os.path.join(path,'images/open.png')), "&Load...", self,
                shortcut=QKeySequence.Open, statusTip="Load parameters",
                triggered=self.open)

        self.saveAct = QAction(QIcon(os.path.join(path,'images/save.png')), "&Save", self,
                shortcut=QKeySequence.Save,
                statusTip="Save all parameters", triggered=self.save)

        self.saveParametersAct = QAction(QIcon(os.path.join(path,'images/save_params.png')), 
                "&Save Parameters", self,
                statusTip="Save circut parameters", triggered=self.saveParameters)

        self.setUnitsAct = QAction(QIcon(os.path.join(path,'images/units.png')), 
                "&Units", self,
                statusTip="Set units", triggered=self.setUnits)

        self.graphAct = QAction(QIcon(os.path.join(path,'images/graph.png')), "&Graph", self,
                statusTip="Setup Graph",
                triggered=self.setupGraph)

        self.marginsAct = QAction(QIcon(os.path.join(path,'images/margins.png')), "&Margins", self,
                statusTip="Calculate Margins",
                triggered=self.setupCalculateMargins)

        self.optimizeAct = QAction(QIcon(os.path.join(path,'images/optimize.png')), "&Optimize", self,
                statusTip="Optimize Margins",
                triggered=self.setupOptimize)        
                
        self.exitAct = QAction("E&xit", self, shortcut="Ctrl+Q",
                statusTip="Exit the application",
                triggered=QApplication.instance().closeAllWindows)

        self.aboutAct = QAction("&About", self,
                statusTip="Show the application's About box",
                triggered=self.about)

        self.stopAct = QAction(QIcon(os.path.join(path,'images/stop.png')), "Stop", self,
                statusTip="Stop",
                triggered=self.stopExecution)

    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addAction(self.saveAct)
        self.fileMenu.addAction(self.saveParametersAct)
        self.fileMenu.addAction(self.setUnitsAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)

        self.menuBar().addSeparator()

        self.helpMenu = self.menuBar().addMenu("&Help")
        self.helpMenu.addAction(self.aboutAct)

    def createToolBars(self):
        self.fileToolBar = self.addToolBar("File")
        self.fileToolBar.addAction(self.openAct)
        self.fileToolBar.addAction(self.saveAct)
        self.fileToolBar.addAction(self.saveParametersAct)
        self.fileToolBar.addAction(self.setUnitsAct)
        self.fileToolBar.addAction(self.graphAct)
        self.fileToolBar.addAction(self.marginsAct)
        self.fileToolBar.addAction(self.optimizeAct)
        # Spacer
        spacer = QWidget()
        spacer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.fileToolBar.addWidget(spacer)
        # Right aligned widget
        self.fileToolBar.addAction(self.stopAct)

        # self.fileToolBar.addSeparator()
        # tframe = QFrame()
        # tflayout = QHBoxLayout()
        # tframe.setLayout(tflayout)
        # time_label = QLabel("Simulation Time: ")
        # tflayout.addWidget(time_label)
        # time_entry = QLineEdit()
        # tflayout.addWidget(time_entry)
        # tflayout.addStretch(100)
        # self.fileToolBar.addWidget(tframe)

    def createStatusBar(self):
        self.statusBar().showMessage("Ready")

    def readSettings(self):
        settings = QSettings('Trolltech', 'SDI Example')
        pos = settings.value('pos', QPoint(200, 200))
        size = settings.value('size', QSize(400, 800))
        self.move(pos)
        self.resize(size)

    def writeSettings(self):
        settings = QSettings('Trolltech', 'SDI Example')
        settings.setValue('pos', self.pos())
        settings.setValue('size', self.size())

    def loadFile(self, fileName):
        file = QFile(fileName)
        if not file.open(QFile.ReadOnly | QFile.Text):
            QMessageBox.warning(self, "SDI",
                    "Cannot read file %s:\n%s." % (fileName, file.errorString()))
            return

        instr = QTextStream(file)

        self.setCurrentFile(fileName)
        self.statusBar().showMessage("File loaded", 2000)

    def saveFile(self, fileName):
        save_all_parameters(fileName)

        # try:
        #     save_all_parameters()
        # except:
        #     QMessageBox.warning(self, "SDI",
        #             "Cannot write file %s" % (fileName))
        #     return False

        self.setCurrentFile(fileName)
        self.statusBar().showMessage("File saved", 2000)
        return True

    def setCurrentFile(self, fileName):
        self.isUntitled = not fileName
        if self.isUntitled:
            self.curFile = "{}_param.txt".format(psglobals.RootCircuit.Name())
        else:
            self.curFile = QFileInfo(fileName).canonicalFilePath()

        self.setWindowModified(False)

        self.setWindowTitle("PSCAN2 - {}".format(psglobals.RootCircuit.Name()))
        
    def getSimulationTime(self):
        if 'tseq' in psglobals.Parameters:
            return psglobals.Parameters['tseq'].Value()
        else:
            return 100.0


    def setupGraph(self):
        if self.graph_setup_window == None:
            self.graph_setup_window = GraphSetup(self.getSimulationTime())
            self.graph_setup_window.setup_done.connect(self.createGraph)
        self.active_setup_window = self.graph_setup_window
        self.active_setup_window.show()

        
    def OnSimulationStep(self):
        if psglobals.CurrentTime <= self.simulation_time:
            #print('Time step ', psglobals.CurrentTime)
            TimeStep()
            (status, messages) = psglobals.RootCircuit.ProcessRules()
            if len(messages) > 0:
                print("------------------------------")
                for msg in messages:
                    print(util.format_rule_message(msg))
                    
            self.pwin.TimeStepUpdate(psglobals.CurrentTime)
                    
            if not status:
                if self.simulation_stop_on_error:
                    # Stop the simulation
                    self.stimer.stop()           
        else:
            self.stimer.stop()
       
        
    @pyqtSlot(float, bool, list)
    def createGraph(self, simulation_time, stop_on_error, plots_list):
        self.simulation_time = simulation_time
        self.simulation_stop_on_error = stop_on_error
        self.pwin = PlotsWindow(plots_list, simulation_time)
        self.pwin.run_simulation.connect(self.runSimulation)
        self.runSimulation()
            
    @pyqtSlot()
    def runSimulation(self):
        InitSimulation()
        psglobals.RootCircuit.InitilizeRules()
        self.stimer = QTimer()
        self.stimer.timeout.connect(self.OnSimulationStep)
        self.stimer.start(1)

    @pyqtSlot(int, list)
    def onUseSelected(self, selection_type, selection_list):
        if self.active_setup_window is None:
            return
        #print("onUseSelected", selection_type, selection_list)
        if selection_type == CircuitTreeWidget.RULES_SELECTED:
            send_list = []
            for rule in selection_list:
                send_list.append((rule.FullName(), "Value", rule))
            self.active_setup_window.addRules(send_list)
        elif selection_type == CircuitTreeWidget.VALUES_SELECTED:
            send_list = []
            for val in selection_list:
                send_list.append((val.FullName(), "Value", val))
            self.active_setup_window.addValues(send_list)
        elif selection_type == CircuitTreeWidget.ELEMENT_FUNCTION_SELECTED:
            send_list = []
            for (ftype, elem) in selection_list:
                send_list.append(('{}({})'.format(ftype, elem.FullName()), ftype, elem))
            self.active_setup_window.addValues(send_list)
        elif selection_type == CircuitTreeWidget.PARAMETERS_SELECTED:
            self.active_setup_window.addParameters(selection_list)
        elif selection_type == CircuitTreeWidget.EXTERNALS_SELECTED:
            self.active_setup_window.addExternals(selection_list)

    def setUnits(self):
        if self.set_units_window == None:
            self.set_units_window = DimensionUnits()
#            self.set_units_window.setup_done.connect(self.setDimensionUnits)
        self.active_setup_window = self.set_units_window
        self.active_setup_window.show()


    def setupCalculateMargins(self):
        if self.margins_setup_window == None:
            self.margins_setup_window = CalculateMarginsSetup(self.getSimulationTime())
            self.margins_setup_window.setup_done.connect(self.calculateMargins)
        self.active_setup_window = self.margins_setup_window
        self.active_setup_window.show()

    def calculateMargins(self, simulation_time, plist, max_margin):
        #print('calculateMargins: ', simulation_time, plist)
        fp=open(self.MarginsReportFname,"a")
        s = util.print_header()
        print(s)
        fp.write(s)
        for param in plist:
            [pmin, pmax] = margin(param, simulation_time, messages_level=0, max_margin=max_margin)
            s = "Margins for {} [{:.2f}%,{:.2f}%]".format(param.FullName(), 100*pmin, 100*pmax)
            print(s)
            fp.write(s+"\n")
        fp.close()
        print ("\n--------------------\n")

    def setupOptimize(self):
        if self.optimize_setup_window == None:
            self.optimize_setup_window = OptimizeSetup(self.getSimulationTime())
            self.optimize_setup_window.setup_done.connect(self.optimizeMargins)
        self.active_setup_window = self.optimize_setup_window
        self.active_setup_window.show()

    def optimizeMargins(self, simulation_time, opt_list, change_list):
        #print('optimizeMargins: ', simulation_time, opt_list, change_list)
        opt = Optimizer(opt_list, change_list, simulation_time, callback=self.optimizeCB)
        self.stop_execution_flag = False
        opt.optimize()
        self.stop_execution_flag = False

    def optimizeCB(self):
        return not self.stop_execution_flag

    def stopExecution(self):
        self.stop_execution_flag = True


if __name__ == '__main__':

    initialize(sys.argv)

    app = QApplication(sys.argv)
    #app.setStyleSheet('QPushButton {min-width: 30px; }')
    mainWin = PSMainWindow()
    mainWin.show()
    sys.exit(app.exec_())
