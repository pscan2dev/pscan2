# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt, QTextStream, 
        pyqtSignal, pyqtSlot, QVariant, QAbstractTableModel, QAbstractListModel,
        QModelIndex)
from PyQt5.QtGui import QIcon, QKeySequence, QPen, QBrush, QDropEvent
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QWidget, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QVBoxLayout, QGroupBox, QGridLayout, 
        QStackedLayout, QLineEdit, QComboBox, QListWidget, QPushButton, QRadioButton, 
        QAbstractItemView, QListWidgetItem, QButtonGroup, QItemDelegate, QListView, QStyle,
        QTableWidget, QTableWidgetItem)
from pscan2.util import find

class OptimizeSetup(QWidget):
    setup_done = pyqtSignal(float, list, list, name='setup_done')
    DefaultLimits = {'j':(1.0, 3.0), 'v':(0.5, 2.0), 'other':(0.1, 10.0)}
    DefaultMargins = {'xj':0.25, 'xi':0.3, 'other':0.3}
    DefaultMargins2 = {'xj':0.3, 'xi':0.3, 'xl':0.4}
    OptMenueSaveFile = "opt_menue.dat"

    def __init__(self, simulation_time = 100.0):
        super(OptimizeSetup, self).__init__()
        self.create(0, True, True)
        #self.resize(600, 600)

        self.param_map = {}

        self.param_set = set()
        self.opt_param_list = []
        self.change_param_list = []

        l = QVBoxLayout()
        self.setLayout(l)

        l2 = QHBoxLayout()
        l.addLayout(l2)
        l2.addWidget(QLabel("Simulation Time:"))
        self.sim_time = QLineEdit(self)
        self.sim_time.insert(str(simulation_time))
        l2.addWidget(self.sim_time)

        separator = QFrame()
        separator.setFrameShape(QFrame.VLine)
        separator.setFrameShadow(QFrame.Sunken)
        l2.addWidget(separator)

        save_button = QPushButton("Save")
        l2.addWidget(save_button)
        save_button.clicked.connect(self.onSave)
        load_button = QPushButton("Load")
        l2.addWidget(load_button)
        load_button.clicked.connect(self.onLoad)
        cancel_button = QPushButton("Cancel")
        l2.addWidget(cancel_button)
        cancel_button.clicked.connect(self.onCancel)
        done_button = QPushButton("Done")
        l2.addWidget(done_button)
        done_button.clicked.connect(self.onDone)

        l3 = QHBoxLayout()
        l.addLayout(l3)

        param_box = QGroupBox()
        param_box_layout = QVBoxLayout()
        param_box.setLayout(param_box_layout)
        param_box.setTitle("Parameters")
        self.param_list = QListWidget(param_box)
        self.param_list.setSelectionMode(QAbstractItemView.ExtendedSelection)
        #self.plot_rules_list.insertItems(0, ["r1", "r2", "r3"])
        param_box_layout.addWidget(self.param_list)
        l3.addWidget(param_box)

        l4 = QHBoxLayout()
        param_box_layout.addLayout(l4)
        l4.addStretch()
        toopt_button = QPushButton("To Optimize")
        toopt_button.clicked.connect(self.onMoveToOptimize)
        l4.addWidget(toopt_button)
        tochange_button = QPushButton("To Change")
        tochange_button.clicked.connect(self.onMoveToChange)
        l4.addWidget(tochange_button)
        remove_button = QPushButton("Remove")
        l4.addWidget(remove_button)
        remove_button.clicked.connect(self.onRemoveParameters)

        # self.opt_list = QListView()
        # self.list_data = ['param1']
        # self.lm = MyListModel(self.list_data, self)
        # self.de = MyDelegate(self)
        # self.opt_list.setModel(self.lm)
        # self.opt_list.setItemDelegate(self.de)

        opt_box = QGroupBox()
        opt_box_layout = QVBoxLayout()
        opt_box.setLayout(opt_box_layout)
        opt_box.setTitle("Parameters to Optimize")
        self.opt_list = QTableWidget(0, 2)
        self.opt_list.setDragDropMode(QAbstractItemView.InternalMove)
        #self.opt_list.setSelectionMode(QAbstractItemView.SingleSelection)
        #self.opt_list.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.opt_list.setHorizontalHeaderLabels(['Parameter', 'Required margin'])
        horis_header = self.opt_list.horizontalHeader()
        horis_header.setSectionsClickable(False)
        vert_header = self.opt_list.verticalHeader()
        vert_header.setSectionsMovable(True)
        #vert_header.setVisible(False)
        
        opt_box_layout.addWidget(self.opt_list)

        l5 = QHBoxLayout()
        opt_box_layout.addLayout(l5)
        l5.addStretch()
        remove_button = QPushButton("Remove")
        l5.addWidget(remove_button)
        remove_button.clicked.connect(self.removeOptimizedParameters)

        l3.addWidget(opt_box)
        l3.setStretchFactor(opt_box, 10)

        change_box = QGroupBox()
        change_box_layout = QVBoxLayout()
        change_box.setLayout(change_box_layout)
        change_box.setTitle("Parameters to Change")
        self.change_list = QTableWidget(0, 3)
        self.change_list.setDragDropMode(QAbstractItemView.InternalMove)
        self.change_list.setHorizontalHeaderLabels(['Parameter', 'Min value', 'Max value'])
        horis_header = self.change_list.horizontalHeader()
        horis_header.setSectionsClickable(False)
        vert_header = self.change_list.verticalHeader()
        vert_header.setSectionsMovable(True)
        
        change_box_layout.addWidget(self.change_list)

        l6 = QHBoxLayout()
        change_box_layout.addLayout(l6)
        l6.addStretch()
        remove_button = QPushButton("Remove")
        l6.addWidget(remove_button)
        remove_button.clicked.connect(self.removeChangedParameters)

        l3.addWidget(change_box)
        l3.setStretchFactor(change_box, 10)

        #min_size = self.minimumSizeHint()
        #if not min_size.isEmpty():
        #    self.resize(min_size.width(), 600)
        self.resize(1100, 600)

    def bringUpFront(self):
        self.show()
        self.raise_()
        self.activateWindow()
        self.showNormal()

    def addParameters(self, param_list):
        new_params = []
        for param in param_list:
            self.param_map[param.FullName()] = param
            new_params.append(param.FullName())
        self.param_set |= set(new_params)
        self.updateParamList()
        self.bringUpFront()

    def addExternals(self, elist):
        self.addParameters(elist)
        
    def addInternals(self, elist):
        pass

    def addValues(self, elist):
        pass

    def addRules(self, elist):
        pass        

    def paramMinValue(self, param):
        if isinstance(param, str):
            name = param.split(".")[-1].lower()
        else:
            name = param.Name().lower()
        ptype = name[0]
        if not ptype in OptimizeSetup.DefaultLimits:
            ptype = 'other'
        return OptimizeSetup.DefaultLimits[ptype][0]

    def paramMaxValue(self, param):
        if isinstance(param, str):
            name = param.split(".")[-1].lower()
        else:
            name = param.Name().lower()
        ptype = name[0]
        if not ptype in OptimizeSetup.DefaultLimits:
            ptype = 'other'
        return OptimizeSetup.DefaultLimits[ptype][1]

    def paramRequiredMargin(self, param):
        if isinstance(param, str):
            ptype = param
        else:
            ptype = param.Name().lower()
        if ptype in OptimizeSetup.DefaultMargins:		
            return OptimizeSetup.DefaultMargins[ptype]
        elif ptype[0:2] in OptimizeSetup.DefaultMargins2:
            return OptimizeSetup.DefaultMargins2[ptype[0:2]]
        else:
            return OptimizeSetup.DefaultMargins['other']

    @pyqtSlot()
    def removeOptimizedParameters(self):
        remove_rows = []
        selected_ranges = self.opt_list.selectedRanges()
        #print('removeOptimizedParameters: ', selected_ranges)
        for sel_range in selected_ranges:
            for i in range(sel_range.topRow(), sel_range.bottomRow()+1):
                remove_rows.append(i)
        for row in remove_rows:
            self.opt_list.removeRow(row)


    @pyqtSlot()
    def appendOptimizeParameters(self, pname_list):
        i = self.opt_list.rowCount()
        #print('appendOptimizeParameters ', i, pname_list)
        self.opt_list.setRowCount(i + len(pname_list))
        for pname in pname_list:
            if not isinstance(pname, list):
                l1 = QTableWidgetItem(pname)
                l1.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                self.opt_list.setItem(i, 0, l1)
                rmarg = str(self.paramRequiredMargin(pname))
            elif len(pname) == 2:
                l1 = QTableWidgetItem(pname[0])
                l1.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                self.opt_list.setItem(i, 0, l1)
                rmarg = pname[1]
            required_margin = QTableWidgetItem(rmarg)
            required_margin.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt.ItemIsEnabled)
            self.opt_list.setItem(i, 1, required_margin)
            i += 1

    @pyqtSlot()
    def removeChangedParameters(self):
        remove_rows = []
        selected_ranges = self.change_list.selectedRanges()
        #print('removeChangedParameters: ', selected_ranges)
        for sel_range in selected_ranges:
            for i in range(sel_range.topRow(), sel_range.bottomRow()+1):
                remove_rows.append(i)
        for row in remove_rows:
            self.change_list.removeRow(row)

    @pyqtSlot()
    def appendChangeParameters(self, pname_list):
        i = self.change_list.rowCount()
        #print('appendChangeParameters ', i, pname_list)
        self.change_list.setRowCount(i + len(pname_list))
        for pname in pname_list:
            if not isinstance(pname, list):
                l1 = QTableWidgetItem(pname)
                minv = str(self.paramMinValue(pname))
                maxv = str(self.paramMaxValue(pname))
            elif len(pname) == 3:
                l1 = QTableWidgetItem(pname[0])
                minv = pname[1]
                maxv = pname[2]
            l1.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
            self.change_list.setItem(i, 0, l1)
            min_val = QTableWidgetItem(minv)
            min_val.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt.ItemIsEnabled)
            self.change_list.setItem(i, 1, min_val)
            max_val = QTableWidgetItem(maxv)
            max_val.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEditable | Qt.ItemIsEnabled)
            self.change_list.setItem(i, 2, max_val)
            i += 1


    def updateParamList(self):
        param_list = list(self.param_set)
        param_list.sort()
        self.param_list.clear()
        self.param_list.insertItems(0, param_list)

    @pyqtSlot()
    def onRemoveParameters(self):
        selected_items = self.param_list.selectedItems()
        #print('onRemoveParameters ', selected_items)
        if len(selected_items) > 0:
            selected_params = []
            for item in selected_items:
                selected_params.append(item.text())
            #print('onRemoveParameters ', selected_params)
            self.param_set -= set(selected_params)
            self.updateParamList()

    @pyqtSlot()
    def onMoveToOptimize(self):
        selected_items = self.param_list.selectedItems()
        if len(selected_items) > 0:
            selected_params = []
            for item in selected_items:
                selected_params.append(item.text())
            #print('onMoveToOptimize ', selected_params)
            self.param_set -= set(selected_params)
            self.updateParamList()
            self.appendOptimizeParameters(selected_params)

    @pyqtSlot()
    def onMoveToChange(self):
        selected_items = self.param_list.selectedItems()
        if len(selected_items) > 0:
            selected_params = []
            for item in selected_items:
                selected_params.append(item.text())
            #print('onMoveToChange ', selected_params)
            self.param_set -= set(selected_params)
            self.updateParamList()
            self.appendChangeParameters(selected_params)

    @pyqtSlot()
    def onCancel(self):
        self.hide()

    @pyqtSlot()
    def onSave(self):
        filename = OptimizeSetup.OptMenueSaveFile
        try:
            f=open(filename, 'w')
        except:
            print("ERROR: Cannot open for writing file " + filename)
            return
        f.write("#opt_list\n")
        for row in range(self.opt_list.rowCount()):
            f.write(self.opt_list.item(row,0).text() + " " + self.opt_list.item(row, 1).text() + "\n")
        f.write("#var_list\n")
        for row in range(self.change_list.rowCount()):
            f.write(self.change_list.item(row,0).text() + " " + self.change_list.item(row, 1).text() + " " + self.change_list.item(row, 2).text() + "\n")
        f.close()
        print("Optimization parameters are saved to file " + filename)
            
    @pyqtSlot()
    def onLoad(self):
        filename = OptimizeSetup.OptMenueSaveFile
        try:
            f = open(filename, 'r')
        except:
            print("ERROR: Cannot open file " + filename)
            return
        opt_lst = []
        var_lst = []
        for s in f.readlines():
            if s[0] == "#":
                continue
            sl = s.split()
            pname = sl[0]
            try:
                param = find(pname)
            except:
                print("ERROR: Cannot find parameter " + pname)
                continue
            self.param_map[pname] = param
            if len(sl) == 2:
                opt_lst += [sl]
            elif len(sl) == 3:
                var_lst += [sl]
            else:
                print("WARNING: wrong record in file " + filename + ":\n" + s)
        f.close()
        if len(opt_lst) == 0 and len(var_lst) == 0:
            return
        for tbl in [self.opt_list, self.change_list]:
            tbl.clearContents()
            tbl.setRowCount(0)
        self.appendOptimizeParameters(opt_lst)
        self.appendChangeParameters(var_lst)


    @pyqtSlot()
    def onDone(self):
        self.hide()
        try:
            simulation_time = float(self.sim_time.text())
        except:
            print("ERROR: Cannot get simulation time")
            return
        opt_list = [None]*self.opt_list.rowCount()
        for row in range(self.opt_list.rowCount()):
            param = self.opt_list.item(row, 0).text()
            try:
                req_margin = float(self.opt_list.item(row, 1).text())
                opt_list[self.opt_list.visualRow(row)] = [self.param_map[param], req_margin]
            except:
                print('Cannot get optimize parameter with required marging from row ', row)
        
        change_list = [None]*self.change_list.rowCount()
        for row in range(self.change_list.rowCount()):
            param = self.change_list.item(row, 0).text()
            try:
                min_value = float(self.change_list.item(row, 1).text())
                max_value = float(self.change_list.item(row, 2).text())
                change_list[self.change_list.visualRow(row)] = [self.param_map[param], min_value, max_value]
            except:
                print('Cannot get change parameter with values range from row ', row)

        #print('OptimizeSetup:onDone ', opt_list, change_list)
        if len(opt_list) > 0 and len(change_list) > 0:
            self.setup_done.emit(simulation_time, opt_list, change_list)

if __name__ == '__main__':
    import sys

    class MyParam:
        def __init__(self, name):
            self.name = name
        def FullName(self):
            return self.name
        def Name(self):
            return self.name
        def Value(self):
            return 1.4


    app = QApplication(sys.argv)
    #app.setStyleSheet('QPushButton {min-width: 30px; }')
    mainWin = OptimizeSetup(100.0)
    mainWin.addParameters([MyParam('p1'), MyParam('p2'), MyParam('p3')])
    #mainWin.addValues(['v1', 'v2'])
    mainWin.show()
    sys.exit(app.exec_())
