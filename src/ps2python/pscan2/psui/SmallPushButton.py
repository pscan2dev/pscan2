# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from PyQt5.QtCore import (QSize)
from PyQt5.QtWidgets import (QPushButton)
        
class SmallTextPushButton(QPushButton):
    def __init__(self, text, parent = None):
        super(SmallTextPushButton, self).__init__(text, parent)
        self.text = text
        
    def sizeHint(self):
        width = max(self.fontMetrics().boundingRect(self.text).width() + 9, 17)
        height = super(SmallTextPushButton, self).sizeHint().height()
        #print("Size hint ", self.text, " ", str(width))
        return QSize(width, height)
        
    def minimumSizeHint(self):
        width = max(self.fontMetrics().boundingRect(self.text).width() + 9, 17)
        height = super(SmallTextPushButton, self).minimumSizeHint().height()
        return QSize(width, height)
        
        
class SmallIconPushButton(QPushButton):
    def __init__(self, icon, parent = None):
        super(SmallIconPushButton, self).__init__(icon, None, parent)
        self.icon = icon
        
    def sizeHint1(self):
        qsize = self.icon.actualSize(QSize())
        width = max(qsize.width() + 9, 20)
        height = super(SmallIconPushButton, self).sizeHint().height()
        #print("Size hint ", self.text, " ", str(width))
        return QSize(width, height)
        
    def minimumSizeHint1(self):
        qsize = self.icon.actualSize(QSize())
        width = max(qsize.width() + 9, 20)
        height = super(SmallIconPushButton, self).minimumSizeHint().height()
        return QSize(width, height)