# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import os

from PyQt5.QtCore import (QFile, QFileInfo, QPoint, QSettings, QSize, Qt,
        QTextStream, QObject, pyqtSlot, pyqtSignal)
from PyQt5.QtGui import QIcon, QKeySequence, QPixmap
from PyQt5.QtWidgets import (QAction, QApplication, QFileDialog, QMainWindow, QLabel, 
        QMessageBox, QTextEdit, QFrame, QHBoxLayout, QGroupBox, QTreeWidget, QTreeWidgetItem,
        QVBoxLayout, QGridLayout, QSpacerItem, QSizePolicy, QLineEdit, QCheckBox, QPushButton,
        QScrollArea)
        
from .SmallPushButton import *
        
class ParametersFrame(QFrame):
    use_checked = pyqtSignal(list, name='use_checked')

    def __init__(self, parameters_map, parent = None):
        super(ParametersFrame, self).__init__(parent)
        
        self.parameters_map = parameters_map

        frame_layout = QVBoxLayout()
        self.setLayout(frame_layout)
        control_frame = QFrame()
        control_layout = QHBoxLayout()
        control_frame.setLayout(control_layout)

        path = os.path.dirname(os.path.abspath(__file__))

        pixmap = QPixmap(os.path.join(path,"images/checked_box.png"))
        icon = QIcon(pixmap)
        check_all_button = SmallIconPushButton(icon, None)
        control_layout.addWidget(check_all_button, Qt.AlignLeft)
        check_all_button.clicked.connect(self.onCheckAll)
        
        pixmap = QPixmap(os.path.join(path,"images/unchecked_box.png"))
        icon = QIcon(pixmap)
        uncheck_all_button = SmallIconPushButton(icon, None)
        control_layout.addWidget(uncheck_all_button, Qt.AlignLeft)
        uncheck_all_button.clicked.connect(self.onUncheckAll)
        
        pixmap = QPixmap(os.path.join(path,"images/use_checked.png"))
        icon = QIcon(pixmap)
        use_selection_button = QPushButton(icon, None)
        use_selection_button.setIconSize(pixmap.size())
        control_layout.addWidget(use_selection_button, Qt.AlignLeft)
        use_selection_button.clicked.connect(self.onUseChecked)
        
        frame_layout.addWidget(control_frame)

        pscroll = QScrollArea()
        frame_layout.addWidget(pscroll)

        parameters_frame = QFrame(pscroll)
        params_layout = QGridLayout()
        parameters_frame.setLayout(params_layout)

        pscroll.setWidget(parameters_frame)
        pscroll.setWidgetResizable(True)
        pscroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        nrow = 0
        self.param_selection = []
        self.value_widgets = {}
        param_list = list(parameters_map.values())
        param_list.sort(key=lambda elem: elem.Name())
        for param in param_list:
            param_label = QLabel(param.Name())
            params_layout.addWidget(param_label, nrow, 0, Qt.AlignLeft | Qt.AlignTop)
            param_value = QLineEdit(str(param.Value()))
            self.value_widgets[param.Name()] = param_value
            params_layout.addWidget(param_value, nrow, 1, Qt.AlignLeft | Qt.AlignTop)
            param_value.returnPressed.connect(lambda obj=self, p=param, w=param_value: obj.paramChangeCB(p,w))
            param_sel = QCheckBox()
            param_sel.parameter_object = param
            self.param_selection.append(param_sel)
            params_layout.addWidget(param_sel, nrow, 2)
            nrow += 1
            
        spacer = QSpacerItem(1,1, QSizePolicy.Expanding, QSizePolicy.Expanding)
        params_layout.addItem(spacer, nrow, 0)
        
    @pyqtSlot()
    def paramChangeCB(self, param, le_widget):
        print("Set param ", param.FullName(), " to ", le_widget.text())
        param.SetValue(float(le_widget.text()))
        
    def onCheckAll(self):
        for n in range(len(self.param_selection)):
            sel = self.param_selection[n]
            sel.setChecked(True)
    
    def onUncheckAll(self):
        for n in range(len(self.param_selection)):
            sel = self.param_selection[n]
            sel.setChecked(False)

    def updateValues(self):
        for param in self.parameters_map.values():
            self.value_widgets[param.Name()].setText(str(param.Value()))

    def onUseChecked(self):
        selected_params = []
        for sel in self.param_selection:
            if sel.isChecked():
                selected_params.append(sel.parameter_object)
        if len(selected_params) > 0:
            self.use_checked.emit(selected_params)
        
