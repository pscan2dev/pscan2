# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals
from .Expression import smooth_step
from . import SimulationParameters 

class ParameterVar:
    """
    Parameter in circuit. If the whole circuit contains several 
    circuits, parameters in those circuits will be the same

    Methods
    ---------
    Name() : string
        Name of the parameter
    Value() : float
        Value of the parameter
    InitialValue() : float
        Initial value of the parameter (as defined in HDL file)
    SetValue(new_value)
        Set value of the parameter
    """
    def __init__(self, path, vardecl, index):
        self.path = path
        self.name = vardecl.Name()
        if path == "":
            self.full_name = self.name
        else:
            self.full_name = str(path) + "." + self.name
        self.value = vardecl.Value().GetFloatConstValue()
        self.initial_value = self.value
        self.dimension = ""
        self.index = index
        self.init_smooth_time = -SimulationParameters.INITIAL_RAMP
        self.expected_value = self.value
        self.init_smooth_value = self.value
        psglobals.ParameterValues[index] = self.value
        
    def __repr__(self):
        return("Parameter {}.{} = {} [{}] ({})".format(self.path, self.name, self.value, 
            self.initial_value, self.index))

    def __lt__(self, other):
        return self.Name() < other.Name()
            
    def Name(self):
        return(self.name)
        
    def Path(self):
        return(self.path)
        
    def FullName(self):
        return(self.full_name)
        
    def Value(self):
        return(psglobals.ParameterValues[self.index])
        
    def DimValue(self):
        val = self.Value()
        if self.dimension == "":
            self.Setdimension()
        if self.dimension in psglobals.DimUnits:
            dim = psglobals.DimUnits[self.dimension]
            return("%e" % (val*dim) + ' ' + self.dimension)
        else:
            return("%f" % val)

    def Setdimension(self, dim = None):
        if dim == None:
            q = self.Name()[0].upper()
            if q in psglobals.Par2Units:
                self.dimension = psglobals.Par2Units[q]
        else:
            self.dimension = str(dim)
        
    def InitialValue(self):
        return(self.initial_value)
        
    def Index(self):
        return(self.index)
        
    def SetValue(self, newvalue):
        self.value = newvalue
        psglobals.ParameterValues[self.index] = self.value
        
    def SetValueSmooth(self, newvalue):
        self.init_smooth_time = psglobals.CurrentTime
        self.expected_value = newvalue
        self.init_smooth_value = self.value
    
    def Update(self):
        if psglobals.CurrentTime >= self.init_smooth_time and \
            psglobals.CurrentTime <= self.init_smooth_time + SimulationParameters.INITIAL_RAMP:
            self.value = self.init_smooth_value + \
                smooth_step((psglobals.CurrentTime - self.init_smooth_time)/SimulationParameters.INITIAL_RAMP)*\
                (self.expected_value - self.init_smooth_value)
            psglobals.ParameterValues[self.index] = self.value  
        
        
