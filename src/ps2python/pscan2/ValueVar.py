# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals
from .Expression import Expression

class ValueVar:
    """
    Value variable in circuit. If the whole circuit contains several 
    circuits, internal variables in each circuit will be independent.
    Internal variables can depend on parameters, external parameters, 
    internal parameters, simulation time, and node's or element's 
    phases, currents, voltages, etc.

    Methods
    ---------
    Name() : string
        Name of the external parameter
    Value() : float
        Value of external parameter
    """
    def __init__(self, path, vardecl, index, circuit):
        self.path = path
        self.name = vardecl.Name()
        # circuit cannot be None
        cir_fname = circuit.FullName()
        if cir_fname == ".":
            self.full_name = "." + self.name
        else:
            self.full_name = cir_fname + "." + self.name
        self.value_exp = Expression(vardecl.Value(), circuit, True)
        self.value = 0.0
        self.index = index
        psglobals.ValueValues[self.index] = self.value
        
    def __repr__(self):
        return("Value {} = {} ({})".format(self.full_name, self.value, self.index))
            
    def Name(self):
        return(self.name)
        
    def Path(self):
        return(self.path)
        
    def FullName(self):
        return(self.full_name)
        
    def FindType(self):
        return('v')

    def Value(self):
        return(self.value)
        
    def Index(self):
        return(self.index)

    def Update(self):
        self.value = self.value_exp.Value()
        psglobals.ValueValues[self.index] = self.value
