# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

from . import psglobals

class Node:
    """
    Node in the circuit

    Methods
    ---------
    Name() : string
        Name of the element
    P() : float
        Phase across the element
    V() : float
        Voltage across the element
    State() : float
        Value of state variable associated with the node
    """
    def __init__(self, name, circuit, index):
        self.name = name
        self.circuit = circuit
        self.index = index
        cir_fname = circuit.FullName()
        if cir_fname == ".":
            self.full_name = self.name
        else:
            self.full_name = cir_fname[1:] + "." + self.name

    def __repr__(self):
        return("Node: {} [{}]".format(self.full_name, self.index))

    def Name(self):
        return(self.name)

    def FullName(self):
        return(self.full_name)

    def FindType(self):
        return('n')

    def Index(self):
        return(self.index)

    def ValueIndex(self):
        return(self.index - 1)

    def P(self):
        return(psglobals.NodePhase[self.index-1])

    def V(self):
        return(psglobals.NodeVoltage[self.index-1])

    def State(self):
        return(psglobals.NodeState[self.index-1])
