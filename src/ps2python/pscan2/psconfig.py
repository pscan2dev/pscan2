﻿# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

# VB(IC, VC, B) = RSJ(IC, VC/IC, (B*IC)/(VC*VC)
# VBPH(IC, VC, B, PH) = RSJPH(IC, VC/IC, (B*IC)/(VC*VC), PH)
# J(X)=VB(X*XJ,XJ,XJ) -> RSJ(X*XJ, 1/X, X)
# JJ(X,Y)=VB(X*XJ*Y,XJ*Y,XJ*Y*Y) -> RSJ(X*XJ*Y, 1/X, X*Y)
# JJJ(X,Y,Z)=VB(X*Y*XJ,Z*Y*XJ,Z*Z*Y*Y*XJ) -> RSJ(X*Y*XJ, Z/X, X*Y)
# JN(X) -> RSJN(X*XJ, 1/X, RSJN_VG, RSJN_N, X)
# JJN(X,Y) -> RSJN(X*XJ*Y, 1/X, RSJN_VG, RSJN_N, X*Y)
# JPH(X) -> RSJPH(X*XJ, 1/X, RSJN_VG, RSJN_N, X, PH)
# JJPH(X,Y) -> RSJPH(X*XJ*Y, 1/X, RSJN_VG, RSJN_N, X*Y, PH)

jj_replace_list = [
	("^jjj\s*\(([^,)]+),([^,)]+),([^,)]+)\)", "rsj((\\1)*xj*(\\2), (\\3)/(\\1), (\\1)*(\\2))"),
	("^jn\s*\(([^,)]+)\)", "rsjn((\\1)*xj, 1.0/(\\1), rsjn_vg, rsjn_n, (\\1))"),
	("^jph\s*\(([^,)]+),([^,)]+)\)", "rsjph((\\1)*xj, 1.0/(\\1), (\\1), (\\2))"),
	("^jjn\s*\(([^,)]+),([^,)]+)\)", "rsjn((\\1)*xj*(\\2), 1.0/(\\1), rsjn_vg, rsjn_n, (\\1)*(\\2))"),
	("^jjph\s*\(([^,)]+),([^,)]+),([^,)]+)\)", "rsjph((\\1)*xj*(\\2), 1.0/(\\1), (\\1)*(\\2), (\\3))"),
	("^jj\s*\(([^,)]+),([^,)]+)\)", "rsj((\\1)*xj*(\\2), 1.0/(\\1), (\\1)*(\\2))"),
	("^jt\s*\(([^,)]+)\)", "tjm(\"tjm1\",(\\1)*xj,wbc,wvg,wvrat,wrrat)"),
	("^jjt\s*$", "tjm(\"tjm1\",{name}*x{name}*xj,wbc,wvg,wvrat,wrrat)"),
	("^j\s*\(([^,)]+)\)", "rsj((\\1)*xj, 1.0/(\\1), (\\1))"),
	("^jjj\s*$", "rsj({name}*x{name}*xj, v{name}/{name}, {name}*x{name})"),
	("^jj\s*$", "rsj({name}*x{name}*xj, 1.0/{name}, {name}*x{name})"),
	("^j\s*$", "rsj({name}*x{name}*xj, 1.0/{name}, {name})"),
	("^ja\s*\(([^,)]+),([^,)]+)\)", "rsj((\\1)*xj*(\\2), XR/(\\1), (\\1)/XJ)"),
	("^vb\s*\(([^,)]+),([^,)]+),([^,)]+)\)", "rsj((\\1), (\\2)/(\\1), (\\1)*(\\3)/(\\2)/(\\2))"),
	("^vbph\s*\(([^,)]+),([^,)]+),([^,)]+),([^,)]+)\)", "rsjph((\\1), (\\2)/(\\1), (\\1)*(\\3)/(\\2)/(\\2), (\\4))")
]

ind_replace_list = [
	("^\?\s*$", "{name}*x{name}*xl"),
	("^l\?\s*$", "{name}*x{name}*xl")
]

cur_replace_list = [
	("^\?\s*$", "{name}*x{name}*xi"),
	("^i\?\s*$", "{name}*x{name}*xi")
]

res_replace_list = [
	("^\?\s*$", "{name}*x{name}*xr"),
	("^r\?\s*$", "{name}*x{name}*xr")
]


# llm(l1, l2, m) -> trans(l1/(l1*l2-m*m), l2/(l1*l2-m*m), m/(l1*l2-m*m))
#trans_replace_list = [
#	("^llm\s*\(([^,)]+),([^,)]+),([^,)]+)\)", "trans((\\1)/((\\1)*(\\2) - (\\3)*(\\3)),"
#	"(\\2)/((\\1)*(\\2) - (\\3)*(\\3)),(\\3)/((\\1)*(\\2) - (\\3)*(\\3)))")
#]

model_replace_map = {
	'j': jj_replace_list,
	'l': ind_replace_list,
	'i': cur_replace_list,
	'r': res_replace_list
}

# TJM models: map of <model_name>:[[A],[B],[P]]
# Where A, B, and P - Dirichlet coefficients
# Size of Dirichlet series should be even and less or equal to 20
tjm_models = {
	# Provided by Prof. Vasili Semenov
	'tjm_vs':[[complex(8.19693E-02, 5.25478E-03),
        	 complex(3.45815E+00, 1.53604E+01),
        	 complex(-1.60452E+00, -2.98688E+00),
        	 complex(6.04354E-01, 6.86880E-02),
        	 complex(1.60407E+00, -8.20150E-01),
        	 complex(-2.47073E+00, -1.72903E+00)],
			[complex(7.87121E-02, 2.14381E-02),
        	 complex(7.71542E-01, 1.56719E+01),
        	 complex(1.57609E-01, 1.97024E+01),
        	 complex(3.70259E-01, 7.09614E-01),
        	 complex(1.94557E+00, 3.89893E-01),
        	 complex(-3.29227E+00, -8.01016E+00)],
			[complex(-1.01670E-01, 9.95300E-01),
        	 complex(-1.38170E+00, 1.69046E-02),
        	 complex(-1.66041E+00, -8.67054E-02),
        	 complex(-5.86729E-01, 8.82753E-01),
        	 complex(-2.89884E+00, 2.03727E+00),
        	 complex(-1.85051E+00, -4.71272E-02)]],
	# T = 0.4 Tc,  Riedel peak widening factor < 0.0001
	# Taken from MiTMoJCo (https://github.com/drgulevich/mitmojco) repository. Model BCS42_001
	'tjm1':	[[complex(-0.000935, -0.344952),
			complex(0.002376, -0.000079),
			complex(0.701978, -3.433012),
			complex(0.141990, 0.034241),
			complex(0.007165, -0.000087),
			complex(0.000650, -0.000029),
			complex(0.020416, 0.000395),
			complex(0.056332, 0.006446),
			complex(1.266591, 0.000000),
			complex(0.187313, 0.279494)],
			[complex(0.001392, -0.100648),
			complex(0.002382, -0.000055),
			complex(-0.258742, 0.553749),
			complex(0.095523, 0.119127),
			complex(0.007150, 0.000084),
			complex(0.000649, -0.000026),
			complex(0.020445, 0.002159),
			complex(0.053536, 0.018137),
			complex(-0.017427, 0.000001),
			complex(-0.161605, 0.336628)],
			[complex(-0.090721, -0.000036),
			complex(-0.004370, 1.000126),
			complex(-0.813405, -0.043201),
			complex(-0.299741, 0.941648),
			complex(-0.013468, 1.000303),
			complex(-0.001497, 1.000022),
			complex(-0.039953, 0.999920),
			complex(-0.113673, 0.993161),
			complex(-6.766647, -0.000001),
			complex(-0.646220, 0.637507)]],
	# T = 0.4 Tc,  Riedel peak widening factor = 0.001
	'tjm2':[[complex(.375815E-01, -.187841E-03),
			complex(.414105E+01, -.184232E+01),
			complex(-.825236E+00, .847076E+00),
			complex(.376722E+00, -.371434E+00),
			complex(.852763E-01, .101578E+01),
			complex(-.212480E+01, .102156E+02)],
			[complex(.376599E-01, .166928E-02),
			complex(.190043E+01, -.136854E+02),
			complex(.827915E+00, .569333E+01),
			complex(.546668E+00, -.103799E+00),
			complex(-.500884E+00, .107951E+01),
			complex(-.305103E+01, .177709E+01)],
			[complex(-.283505E-01, .999864E+00),
			complex(-.138528E+01, -.215948E+00),
			complex(-.130428E+01, -.504918E+00),
			complex(-.335856E+00, .105974E+01),
			complex(-.690308E+00, .109227E+01),
			complex(-.156693E+01, .992425E-02)]],
	# T = 0.4 Tc,  Riedel peak widening factor = 0.005
	'tjm3':[[complex(.166161E-01, .196399E+00),
			complex(.371215E+01, -.399262E+01),
			complex(.519499E-01, .271276E+01),
			complex(.330896E-01, -.565722E-03),
			complex(.186616E+00, -.220034E+00),
			complex(-.216119E+01, .444670E+02)],
			[complex(-.591848E-01, -.154174E+00),
			complex(.214091E+01, -.183403E+02),
			complex(.117112E+01, .374828E+01),
			complex(.330388E-01, .318017E-02),
			complex(.209726E+00, .289121E+00),
			complex(-.364211E+01, .413603E+02)],
			[complex(-.203794E+00, .989229E+00),
			complex(-.177278E+01, -.106162E+00),
			complex(-.165291E+01, -.665748E+00),
			complex(-.431224E-01, .100037E+01),
			complex(-.221928E+00, .975625E+00),
			complex(-.172133E+01, .398833E-01)]],
	# T = 0.4 Tc,  Riedel peak widening factor = 0.01
	'tjm4':[[complex(-.917875E-01, .252494E+00),
			complex(.411212E+01, -.312838E+01),
			complex(-.511463E+00, .372217E+01),
			complex(.343679E-01, .127995E-01),
			complex(.249672E+00, -.222495E+00),
			complex(-.189753E+01, .269941E+02)],
			[complex(-.180579E-01, .129600E+00),
			complex(.184287E+01, -.146921E+02),
			complex(.609927E+00, .772952E+00),
			complex(.314229E-01, .131221E-01),
			complex(.160004E+00, -.177492E-01),
			complex(-.266354E+01, -.208152E+01)],
			[complex(-.237227E+00, .973942E+00),
			complex(-.174126E+01, -.167763E+00),
			complex(-.134826E+01, -.853586E+00),
			complex(-.654586E-01, .991820E+00),
			complex(-.217492E+00, .960524E+00),
			complex(-.163154E+01, .123462E+00)]]
}

