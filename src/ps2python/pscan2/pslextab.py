# pslextab.py. This file automatically created by PLY (version 3.10). Don't edit!
_tabversion   = '3.10'
_lextokens    = set(('AND', 'POINT', 'CIRCUIT', 'DIVIDE', 'NE', 'INTERNAL', 'LBRACKET', 'LBRACE', 'MINUS', 'RULE', 'GT', 'IDENT', 'COMMA', 'NOT', 'PARAMETER', 'ICONST', 'TIMES', 'RBRACE', 'VALUE', 'SCONST', 'RBRACKET', 'FREEZE', 'LE', 'SEMI', 'EXTERNAL', 'EQ', 'OR', 'PLUS', 'LT', 'EQUALS', 'GE', 'FCONST', 'LPAREN', 'RPAREN'))
_lexreflags   = 64
_lexliterals  = ''
_lexstateinfo = {'INITIAL': 'inclusive'}
_lexstatere   = {'INITIAL': [('(?P<t_NEWLINE>\\n+)|(?P<t_IDENT>[A-Za-z_][\\w_]*)|(?P<t_SCONST>\\"([^\\\\\\n]|(\\\\.))*?\\")|(?P<t_comment>\\#.*)|(?P<t_FCONST>((\\d+)(\\.\\d+)(e(\\+|-)?(\\d+))? | (\\d+)e(\\+|-)?(\\d+)))|(?P<t_OR>\\|\\|)|(?P<t_ICONST>\\d+)|(?P<t_PLUS>\\+)|(?P<t_TIMES>\\*)|(?P<t_AND>&&)|(?P<t_LE><=)|(?P<t_GE>>=)|(?P<t_EQ>==)|(?P<t_NE>!=)|(?P<t_LPAREN>\\()|(?P<t_RPAREN>\\))|(?P<t_LBRACKET>\\[)|(?P<t_RBRACKET>\\])|(?P<t_LBRACE>\\{)|(?P<t_RBRACE>\\})|(?P<t_POINT>\\.)|(?P<t_MINUS>-)|(?P<t_DIVIDE>/)|(?P<t_NOT>!)|(?P<t_LT><)|(?P<t_GT>>)|(?P<t_EQUALS>=)|(?P<t_COMMA>,)|(?P<t_SEMI>;)', [None, ('t_NEWLINE', 'NEWLINE'), ('t_IDENT', 'IDENT'), ('t_SCONST', 'SCONST'), None, None, ('t_comment', 'comment'), (None, 'FCONST'), None, None, None, None, None, None, None, None, None, (None, 'OR'), (None, 'ICONST'), (None, 'PLUS'), (None, 'TIMES'), (None, 'AND'), (None, 'LE'), (None, 'GE'), (None, 'EQ'), (None, 'NE'), (None, 'LPAREN'), (None, 'RPAREN'), (None, 'LBRACKET'), (None, 'RBRACKET'), (None, 'LBRACE'), (None, 'RBRACE'), (None, 'POINT'), (None, 'MINUS'), (None, 'DIVIDE'), (None, 'NOT'), (None, 'LT'), (None, 'GT'), (None, 'EQUALS'), (None, 'COMMA'), (None, 'SEMI')])]}
_lexstateignore = {'INITIAL': ' \t\x0c'}
_lexstateerrorf = {'INITIAL': 't_error'}
_lexstateeoff = {}
