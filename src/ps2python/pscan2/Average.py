# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import math, logging, time
from . import psglobals

class Average:
    def __init__(self, t, N = 5, a = 1.0 / (pow(2.0, 1.0/5) - 1.0)):
        self.N = N
        self.a = a
        self.yaver = [0.0]*(N+1)
        self.told = t
        self.tstart = t

    def step(self, t, x):
        dt = t - self.told
        self.yaver[0] = x
        for i in range(1, self.N + 1):
                self.yaver[i] = (self.a * dt * self.yaver[i-1] + \
                    (t - self.tstart) * self.yaver[i])/(t - self.tstart + self.a * dt)
        self.told = t

        return self.yaver[self.N]


class AverageT:
    def __init__(self, t, Taver, N = 5, a = 1.0 / (pow(2.0, 1.0/5) - 1.0)):
        self.N = N
        self.a = a
        self.Taver = Taver
        self.yaver = [0.0]*(N+1)
        self.told = t

    def step(self, t, x):
        dt = t - self.told
        if dt < 0.00001:
            return x

        f = self.a * dt / self.Taver
        self.yaver[0] = x
        for i in range(1, self.N + 1):
                self.yaver[i] = (self.yaver[i-1] * f + self.yaver[i])/(1.0 + f)
        self.told = t

        return self.yaver[self.N]

        
