# cython: profile=False
# cython: boundscheck=False
# cython: wraparound=False

# PSCAN2: Superconductor Circuit Simulator 
# Copyright (C) 2015,2016 by Pavel Shevchenko (pscan2sim@gmail.com)

import numpy as np
# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution).
cimport numpy as np

import cython
from cpython cimport array
import array

from libc cimport math

cimport klu

cdef array.array dbl_array_template = array.array('d', [])

cdef class KLUSolver:
	cdef klu.klu_symbolic *symbolic
	cdef klu.klu_common common
	cdef int n
	cdef int nnz
	cdef array.array Ai
	cdef array.array Ap
	cdef array.array Asparse
	
	
	def __cinit__(self):
		self.symbolic = NULL
		self.n = 0
		self.nnz = 0
		klu.klu_defaults(&self.common)
		
	def __del__(self):
		if self.symbolic != NULL:
			klu.klu_free_symbolic(&self.symbolic, &self.common)
		
	def analyze(self, int n, array.array Ap, array.array Ai):
		self.n = n
		self.Ai = Ai
		self.Ap = Ap
		self.nnz = Ap[n]
		self.Asparse = array.clone(dbl_array_template, self.nnz, zero=False)
		self.symbolic = klu.klu_analyze(n, Ap.data.as_ints, Ai.data.as_ints, &self.common)
		
	def solve(self, array.array Ax, array.array B):
		cdef klu.klu_numeric* numeric
	
		if self.n > 0 and self.symbolic != NULL:
			numeric = klu.klu_factor(self.Ap.data.as_ints, self.Ai.data.as_ints, 
				Ax.data.as_doubles, self.symbolic, &self.common)
			klu.klu_solve(self.symbolic, numeric, self.n, 1, B.data.as_doubles, &self.common)
			klu.klu_free_numeric(&numeric, &self.common)
			
	def solveMatrix(self, np.ndarray[np.float64_t, ndim=2, mode="c"] Ax not None, 
					np.ndarray[np.float64_t, ndim=1, mode="c"] B not None):
		cdef klu.klu_numeric* numeric
		cdef int i, j, col, row, ind1, ind2
		cdef int[:] cAp = self.Ap
		cdef int[:] cAi = self.Ai
	
		if self.n > 0 and self.symbolic != NULL:
			ind1 = cAp[0]
			for i in range(1, self.n+1):
				ind2 = cAp[i]
				col = i - 1
				for j in range(ind1, ind2):
					row = cAi[j]
					self.Asparse[j] = Ax[row, col]
				ind1 = ind2
			
			numeric = klu.klu_factor(self.Ap.data.as_ints, self.Ai.data.as_ints, 
				self.Asparse.data.as_doubles, self.symbolic, &self.common)
			klu.klu_solve(self.symbolic, numeric, self.n, 1, <double*>B.data, &self.common)
			klu.klu_free_numeric(&numeric, &self.common)
	
	def solveSparseMatrix(self, np.ndarray[np.float64_t, ndim=1, mode="c"] Ax not None, 
						  np.ndarray[np.float64_t, ndim=1, mode="c"] B not None):
		cdef klu.klu_numeric* numeric
		
		if self.n > 0 and self.symbolic != NULL:
			numeric = klu.klu_factor(self.Ap.data.as_ints, self.Ai.data.as_ints, 
				<double *>Ax.data, self.symbolic, &self.common)
			klu.klu_solve(self.symbolic, numeric, self.n, 1, <double*>B.data, &self.common)
			klu.klu_free_numeric(&numeric, &self.common)
		

