import matplotlib.pyplot as plt

from pscan2 import TestVector

tv1 = TestVector.TestVector("", "11001", 20.0, 0, 3, 0.0)
print(tv1)

tv2 = TestVector.TestVector("", [0,1,0,0,1], 20.0, 10, 3, 0.0)
print(tv2.vector, tv2.vectorn, tv2.period)

tv3 = tv1+tv2
print(tv3.vector, tv3.vectorn, tv3.period)

tv4 = tv1*2
print(tv4.vector, tv4.vectorn, tv4.period)

xarr = []
yarr = []

for i in range(0, 1000):
	x = i * 0.5
	xarr.append(x)
	yarr.append(tv1.value(x))


plt.plot(xarr, yarr, linestyle='-', marker='*', color = 'blue')

plt.grid(True)
plt.show()
